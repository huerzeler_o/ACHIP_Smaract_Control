import numpy as np
import matplotlib.pyplot as plt
import h5py

plotSensors = ['SINDI02-DBLM085:B1_LOSS']
def plotData(outputFile):
    file = h5py.File(fileName, 'r')
    unit = 'mm'
    
    #plot loss profile
    fig1 = plt.figure(figsize = (9,6), dpi = 200)
    ax1 = fig1.add_subplot(111)
    
    posData = np.array(file['PositionRead'].value)
    for s in plotSensors:
        data = np.array(file['SensorValue/'+s].value)
        data-= np.average(data[:10])
        mean = np.sum(posData*data)/np.sum(data)
        rms = np.sqrt(np.sum((posData-mean)**2*data))/np.sqrt(np.sum(data))
        print(mean, rms)
        ax1.set_title('rms: '+str(rms))
        ax1.plot(posData, data, label = s, ls = '', marker = '+', ms = 10, color = 'r')

    ax1.set_xlabel('Position ('+unit+')')
    ax1.set_ylabel('BLM Loss')
    ax1.ticklabel_format(style='sci', scilimits = (-2,2), axis='both')

    ax1.legend()

    fig1.tight_layout()

directory = '/afs/psi.ch/intranet/SF/Diagnostics/ACHIP/Data/InjectorWS/'

fileName = directory + 'PSI_1um_S4_20181204_162016' + '.h5'

plotData(fileName)
plt.show()
