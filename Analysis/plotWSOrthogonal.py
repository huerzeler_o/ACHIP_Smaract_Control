# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
import fitFunctions as ff


files = ['D:/Measurement Data/ATHOS/20191214/ACHIP/WSMLeftWire_20191214_143736/LiveData.h5',
         'D:/Measurement Data/ATHOS/20191214/ACHIP/WSMTopWire_20191214_142549/LiveData.h5']

directions = ['x', 'y']
for f, direction in zip(files, directions):
    popt, perr = ff.plotFit(f, ff.gauss, direction, 'SATSY03-DLAC080-DHXP:BLM1_comp', poptDict = None, center = True)
    sig, sigErr = popt[2], np.sqrt(np.diag(perr)[2])
    plt.title(r'$\sigma_'+direction+' = $'+str(np.round(sig, 2))+r'$\pm$'+str(np.round(sigErr, 2))+' μm')
    plt.tight_layout()
    plt.grid()

