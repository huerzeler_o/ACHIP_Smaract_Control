# -*- coding: utf-8 -*-
import numpy as np
from scipy.special import erf
import scipy.constants as sc
from skimage.transform import radon
from scipy.interpolate import interp1d
from scipy import signal
import matplotlib.pyplot as plt
import h5py as h5
from scipy.optimize import curve_fit 

def gauss(x, a, x0, sigma, offset):
    return a*np.exp(-(x-x0)**2/(2*sigma**2))+offset

def dualGauss(x, a, x0, sigma, dist, offset):
    #two gaussian peaks, for alignment    
    return a*(np.exp(-(x-x0-dist/2)**2/(2*sigma**2)) + np.exp(-(x-x0+dist/2)**2/(2*sigma**2)))+offset

def linear(x, a, b):
    return a*x + b

def quadratic(x, a, b, c):
    return a*x**2 + b*x + c

def knifeEdge(x, a, x0, sigma, offset):
    return offset + a*0.5*(1+erf((x-x0)/sigma/np.sqrt(2)))

def twoD_Gaussian(xdata_tuple, amplitude, xo, yo, sigma_x, sigma_y, theta, offset):
    """
    Definition of 2D gausian, similar to octave code on wiki but with offset,
    supports arrays and fitting

    Parameters
    ----------
    xdata_tuple :   array_like
                    x and y positions as a tuple, e.g. from a meshgrid
    amplitude   :   float
                    amplitude of gaussian
    xo          :   float
                    x zero pos of gaussian
    yo          :   float
                    y zero position of gaussian
    sigma_x     :   float
                    sigma in x direction
    sigma_y     :   float
                    sigma in y direction
    theta       :   float
                    rotation of gaussian in rad
    offset      :   float
                    shift all values up or down
    Returns
    -------
    out :   float
            value of gaussian at position
    """

    (x,y) = xdata_tuple
    xo = float(xo)
    yo = float(yo)    
    a = (np.cos(theta)**2)/(2*sigma_x**2) + (np.sin(theta)**2)/(2*sigma_y**2)
    b = -(np.sin(2*theta))/(4*sigma_x**2) + (np.sin(2*theta))/(4*sigma_y**2)
    c = (np.sin(theta)**2)/(2*sigma_x**2) + (np.cos(theta)**2)/(2*sigma_y**2)
    g = offset + amplitude*np.exp( - (a*((x-xo)**2) + 2*b*(x-xo)*(y-yo) 
                            + c*((y-yo)**2)))
    return g.ravel()

def create_circular_mask(h, w):

    center = [int(w/2), int(h/2)]
    radius = min(center[0], center[1], w-center[0], h-center[1])

    Y, X = np.ogrid[:h, :w]
    dist_from_center = np.sqrt((X - center[0])**2 + (Y-center[1])**2)

    mask = dist_from_center <= radius
    return mask


class wireScannerModel:
    """
    Class for wire scan model funtion. For faster fitting,
    create an isntance with the corresponding meshgrid sizes
    which should then stay in memory for fater evaluation
    """
    def __init__(self, maxD, nPoints, wireWidth = 1):
        #set parameters for meshgrid here
        self.nPoints = nPoints
        self.maxD = maxD
        self.stepsizeScale = None
        self.evalTupels = None
        self.radonScale = None
        self.radonScaleDP = None
        self.wireModelConv = None
        self.wireWidth = wireWidth
        self.circMask = None
        self.prepEval()

    def prepEval(self):
        #generate meshgrid for computation
        self.x = np.linspace(-self.maxD, self.maxD, self.nPoints)
        self.y = np.linspace(-self.maxD, self.maxD, self.nPoints)
        X,Y = np.meshgrid(self.x,self.y)
        self.evalTupels = (X,Y)
        #self.radonScale = np.linspace(-np.sqrt(2*self.maxD**2),np.sqrt(2*self.maxD**2), np.ceil(self.nPoints*np.sqrt(2))) #this line is relevant when the circle flag is set to false in radon
        self.radonScale = np.linspace(-self.maxD,self.maxD, np.ceil(self.nPoints))
        self.wireModelConv = np.squeeze(np.zeros_like(self.radonScale))
        self.wireModelConv[(self.radonScale > -self.wireWidth/2)|(self.radonScale < self.wireWidth/2)] = 1
        self.wireModelConv[(self.radonScale < -self.wireWidth/2)|(self.radonScale > self.wireWidth/2)] = 0
        self.wireModelConv/=np.sum(self.wireModelConv)
        self.circMask = create_circular_mask(self.nPoints,self.nPoints)
        self.stepsizeScale = self.maxD*2/self.nPoints
        print("Stepsize of Bins = " + str(self.stepsizeScale) + "mu m")
        if 5*self.stepsizeScale > self.wireWidth:
            print("Warining: Precission of Grid is too low for reliable usage of convolution with wire thickness")

    def wireScannerBasic(self,pos_tuple, amplitude, xo, yo, sigma_x, sigma_y, theta, offset, conv = True, profile = None):
        """
        Wire scanner implementation, calculates value of scan under certain angle,
        by summation,The underlying spot is assumed to be of gaussian shape.

        Parameters
        ----------
        pos_tuple   :   array_like
                        position consisting out of displacedment d and angle theta_transform
        amplitude   :   float
                        amplitude of gaussian
        xo          :   float
                        x zero pos of gaussian
        yo          :   float
                        y zero position of gaussian
        sigma_x     :   float
                        sigma in x direction
        sigma_y     :   float
                        sigma in y direction
        theta       :   float
                        rotation of gaussian in rad
        offset      :   float
                        shift all values up or down
        Returns
        -------
        out :   float
                value of wirescan at displacement and angle
        """
        #unpack
        d = pos_tuple[:,0]
        theta_transform = pos_tuple[:,1]

        #check if eval grid is meaningfull
        if np.max(np.abs(d)) > self.maxD:
                print("Grid was not prepared large enought to be feasable, regenerating...")
                self.maxD = np.max(np.abs(d))
                self.prepEval()
                print("Continuing")

        #find the unique rotations
        angles = np.unique(theta_transform)
        #prepare array for results
        wireOutput = np.zeros_like(d)
        if profile is None:     
            #calculate gaussians values (offset=0, added after convolution)
            profile = twoD_Gaussian(self.evalTupels,amplitude, xo, yo, sigma_x, sigma_y, theta, 0)
            profile = np.reshape(profile, (self.nPoints, -1))
        #set values outside of circle to zero
        profile[~self.circMask] = 0
        #apply radon transform
        scanLine = radon(profile, theta = (angles+90)%360, circle=True)*(self.radonScale[1]-self.radonScale[0])
        #apply convolution
        if conv is True and np.sum(self.wireModelConv) != 0 and np.any(self.wireModelConv == np.nan) is False:
            for i in range(scanLine.shape[1]):
                scanLine[:,i] = signal.fftconvolve(scanLine[:,i], self.wireModelConv, mode = "same")
        #add global offset 
        scanLine += offset
        #iterate over all angles
        for i, elm in enumerate(angles):
                #interpolate points to be able to serve any requested d values
                interpolator = interp1d(self.radonScale,scanLine[:,i])
                wireOutput[np.where(theta_transform == elm)] = interpolator(d[np.where(theta_transform == elm)])
        return wireOutput
        

def removeNan(dataList):
    isNan = np.logical_or.reduce([np.isnan(d) for d in dataList])
    idx = np.argwhere(np.logical_not(isNan)).flatten()
    return [d[idx] for d in dataList]   
    
def plotFit(fileName, fitF, scanDirection, sensorName, poptDict = None, center = False):
    '''
    Plots fit arbitrary 1d fit function (fitF) and plot
    
    poptDict 
    is used to create title with relevant fit parameters
    e.g. poptDict = {r'$\sigma$':{'idx': 2, 'unit': 'μm'}}
    '''
    f = h5.File(fileName, 'r')
    d = np.array(f['PositionHEX'][scanDirection])
    s = np.array(f[sensorName])
    d, s = removeNan([d,s])
    p0 = []
    if fitF==gauss:
        a = np.max(s)-np.min(s)
        x0 = np.average(d)
        sig = (np.max(d)-np.min(d))/10
        offset = np.min(s)
        p0 = [a, x0, sig, offset]
    if fitF==dualGauss:
        a = np.max(s)-np.min(s)
        x0 = np.average(d)
        sig = (np.max(d)-np.min(d))/10
        dist = sig*5
        offset = np.min(s)
        p0 = [a, x0, sig, dist, offset]
    if fitF==knifeEdge:
        a = np.max(s)-np.min(s)
        x0 = np.average(d)
        sig = (np.max(d)-np.min(d))/10
        offset = np.min(s)
        p0 = [a, x0, sig, offset]
    
    print('initial parameter guess', p0)
    fig = plt.figure(figsize=(4.5,3.5), dpi=200)
    ax = fig.add_subplot(111)

    xFit = np.linspace(min(d), max(d), 300)        
#    ax.plot(xFit, fitF(xFit,*p0), color='0.5', ls = '--')        

    popt, perr = curve_fit(fitF, d, s, p0=p0)
    xM = popt[1]
    if not(center):
        xM=0
    yFit = fitF(xFit, *popt)
    ax.plot(xFit-xM, yFit, color='k')
    text = ''
    if poptDict != None:
        for p in poptDict:
            idx = poptDict[p]['idx']
            val = popt[idx]
            unit = poptDict[p]['unit']
            text += p + ' = ' + str(np.round(val,1)) + ' ' + unit + ' '
    ax.set_title(text)    
    ax.plot(d-xM, s, ls='', marker = 'x', color='r')
    ax.set_xlabel(scanDirection + ' (μm)')
    ax.set_ylabel('Loss (arb. units)')
    
    plt.show()
    return popt, perr

#plotFit('D://Measurement Data//ATHOS//20190712//S3UpperWire_20190713_020510//RAWScanData.h5', gauss, 'y', 'SensorValue/SATCL01-DBLM135:B1_LOSS')
