import numpy as np
import matplotlib.pyplot as plt
import h5py as h5
import fitFunctions as ff
from scipy.optimize import curve_fit
import plotWireTomography as pwt
import scipy.constants as sc
from matplotlib.gridspec import GridSpec
from mpl_toolkits.axes_grid1 import make_axes_locatable
import os
from matplotlib.offsetbox import AnchoredText
from scipy.interpolate import interp2d

plt.rcParams.update({'font.size': 12})
props = dict(boxstyle='round', facecolor='wheat', alpha=0.3)
map180 = False

def getValuesWithErrors(function, datapoints, arguments, errors, errorsToEval):
    """
    Compose array where each line corresponds to a parameter with error min/max error on it.

    Parameters
    ----------
    function    :   callable
                    function to be evaluated
    datapoints  :   array like
                    points function is evaluated at
    arguments   :   array like
                    values of parameters
    errors      :   array like
                    errors of values provided under arguments
    errorsToEval:   list
                    specifies which errors should be used
    Returns
#    -------
    out :   array
            Values of function evaluated at datapoints, each row contains results for an error,
            row 0 corresponds to error 0 +, row 1 cprresponds to error 0 -, ...
    """
    errors = np.sqrt(np.diagonal(errors))
    results = np.zeros((2*len(errorsToEval),datapoints.shape[0]))
    for i,elm in enumerate(errorsToEval):
        #evaluate for plus sigma
        argWerrors = np.copy(arguments)
        argWerrors[elm] += 4*errors[elm]
        results[i*2+0] = function(datapoints, *argWerrors)
        #evaluate for minus sigma
        argWerrors = np.copy(arguments)
        argWerrors[elm] -= 4*errors[elm]
        results[i*2+1] = function(datapoints, *argWerrors)
    return results

def getValuesWithErrorsComb(function, datapoints, arguments, errors, errorsToEval):
    """
    Compose array containing the quadratic sum of all deviations from the optimal
    function vaules, computed with different errors

    Parameters
    ----------
    function    :   callable
                    function to be evaluated
    datapoints  :   array like
                    points function is evaluated at
    arguments   :   array like
                    values of parameters
    errors      :   array like
                    errors of values provided under arguments
    errorsToEval:   list
                    specifies which errors should be used
    Returns
    -------
    out :   array
            Quadratic sum of deviation of the function from the ideal result
    """
    errorsDiag = np.sqrt(np.diagonal(errors))
    fResult = np.reshape(function(datapoints, *arguments),(1,-1))

    results = np.repeat(fResult, len(errorsToEval), axis = 0)
    for i,elm in enumerate(errorsToEval):
        #evaluate for plus sigma
        argWerrors = np.copy(arguments)
        argWerrors[elm] += errorsDiag[elm]
        results[i] -= function(datapoints, *argWerrors)
    results = np.square(np.abs(results))
    results = np.sum(results, axis = 0)
    return 3*np.sqrt(results)


def transformRep(x,y, spacingWraped180):
    d = np.zeros_like(x)
    angle = np.zeros_like(x)
    for line in np.arange(0,d.shape[0]):
        d[line] = np.sqrt((x[line]-x[line,0])**2+(y[line]-y[line,0])**2)
        d[line]-= np.average(d[line])
        if spacingWraped180 is True:
            modVal = np.pi
        else:
            modVal = 2*np.pi
        
        angle[line] = np.repeat(np.round(np.rad2deg((np.arctan2((y[line,-1]-y[line,0]),(x[line,-1]-x[line,0]))-np.pi/2)%(modVal))), x.shape[1])
    return [d, angle]

def getData(fileName, sensorName):
    """
    Read data from .h5 file, remove nans

    Parameters
    ----------
    fileName    :   str
                    name of file to be read
    sensorName  :   str
                    name of sensor whos data is loaded
    Returns
    -------
    x   :   array
            Array of x values visited
    y   :   array
            Array of y values visited
    s   :   array
            Array containing Beam Loss measured at (x,y)
    """

    f = h5.File(fileName, 'r')
    x = np.array(f['PositionHEX']["x"])
    y = np.array(f['PositionHEX']["y"])
    
    s = np.array(f[sensorName])
    x, y, s = pwt.removeNan([x,y,s])
    return x,y,s

def getDataManual(fileName, sensorName):
    f = h5.File(fileName, 'r')
    x = np.array(f['PositionHEX']["x"])
    y = np.array(f['PositionHEX']["y"])
    
    s = np.array(f[sensorName])
    x, y, s = pwt.removeNan([x,y,s])
    nShots=40
    setupFilename = 'D:/Measurement Data/ATHOS/20201002/ws/ws_20201003_051658/scanPreparation.h5'
    fSetup = h5.File(setupFilename, 'r')
    edges = [fSetup['Points/P'+str(i)].value for i in range(18)]
    
    xFit = []
    yFit = []
    sFit = []
    for i in range(9):
        xStart = edges[2*i][0]
        xEnd = edges[2*i+1][0]
        yStart = edges[2*i][1]
        yEnd = edges[2*i+1][1]
        xFit.extend(np.linspace(xStart, xEnd, nShots))
        yFit.extend(np.linspace(yStart, yEnd, nShots))
        
    xFit = np.array(xFit)
    yFit = np.array(yFit)
#    fFit = interp2d(x, y, s, fill_value=0, kind = 'cubic')
#    sFit = np.array([fFit(xFit[i], yFit[i]) for i in range(len(xFit))])
    for i in range(len(xFit)):
        distance = np.sqrt((x-xFit[i])**2+(y-yFit[i])**2)
        sFit.append(s[np.argmin(distance)])
    sFit = np.array(sFit)
    return xFit,yFit,sFit

def plotnWires9(pc, angle, d, s, pEdges, beamPofileSim, popt, pcov, errorsShown, scanName=None):
    """
    Plot data for 9 wire case, plot includes: 2d Gaussian, Projections,
    Raw data, error interval

    Parameters
    ----------
    pc              :   object
                        wireScannerModel object required for evaluation
    angle           :   array
                        array of angles appearing in measurement
    d               :   array
                        all displacements for measured data plotting
    s               :   array
                        all beam loss monitor data for plotting
    pEdges          :   float
                        set maximum and minimum displacement/xValue shown in plots
    beamPofileSim   :   2D array
                        simulated beam profile, edge should be f(pEdges)
    popt            :   array
                        parameters for function found via optimization
    pcov            :   array
                        covariance matrix containing information on fits confidence
    errorsShown     :   list
                        list containing the indices of all parameters which errors should be included
    """
    #set up figure for 9 wire case
    f = plt.figure(figsize = (14, 7))
    if scanName != None:
        f.canvas.set_window_title("Gaussian Fit - " + scanName)
    gs = GridSpec(12,2)
    
    ax01 = f.add_subplot(gs[0:2,0])
    plt.setp(ax01.get_xticklabels(), visible=False)
    ax11 = f.add_subplot(gs[2:4,0], sharex = ax01)
    plt.setp(ax11.get_xticklabels(), visible=False)
    ax21 = f.add_subplot(gs[4:6,0], sharex = ax11)
    plt.setp(ax21.get_xticklabels(), visible=False)
    ax31 = f.add_subplot(gs[6:8,0], sharex = ax21)
    plt.setp(ax31.get_xticklabels(), visible=False)
    ax41 = f.add_subplot(gs[8:10,0], sharex = ax31)
    plt.setp(ax41.get_xticklabels(), visible=False)
    ax51 = f.add_subplot(gs[10:12,0], sharex = ax41)
    ax00 = f.add_subplot(gs[7:12,1])
    ax30 = f.add_subplot(gs[0:2,1], sharex = ax51)
    plt.setp(ax30.get_xticklabels(), visible=False)
    ax40 = f.add_subplot(gs[2:4,1], sharex = ax30)
    plt.setp(ax40.get_xticklabels(), visible=False)
    ax50 = f.add_subplot(gs[4:6,1], sharex = ax40)
    #check if swicht to nm should be done
    if popt[3] < 0.5 and popt[4] < 0.5:
        unit = r"$\ \mathrm{nm}$"
        mult = 1000
    else:
        unit = r"$\ \mathrm{\mu m}$"
        mult = 1

    #gen list corresponding to angles
    axList = [ax01,ax11,ax21, ax31, ax41, ax51, ax30, ax40, ax50]
    axLabels = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'k']
    
    for ax, label in zip([*axList, ax00], axLabels):
        if label=='k':
            col = '1'
        else: col = '0'
        anchored_text = AnchoredText(label, loc = 2, frameon=False, pad=0, prop={'color': col, 'size':12, 'fontweight':'bold'})
        ax.add_artist(anchored_text)
#        ax.text(0.05, 0.95, label, transform=ax.transAxes, fontsize=12, fontweight='bold', va='top', color=col)
        
    cList = ["#e50000", "#15b01a", "#0343df", "#06c2ac", "#aaff32", "#f97306", "#7e1e9c", "#ff81c0", "#929591"]
    errors = np.round(np.sqrt(np.diag(pcov)), 2)
    fitProps =  (   r"$\sigma_u=$" + str(np.round(popt[3]*mult, 2)) + r"$\pm$" + str(errors[3]*mult) + unit + "\n" + 
                    r"$\sigma_v = $" + str(np.round(popt[4]*mult, 2)) + r"$\pm$" + str(errors[4]*mult) + unit + "\n" + 
                    r"$\theta=$" + str(np.round(np.rad2deg(popt[-2]), 2)) + r"$\pm$" + str(np.round(np.rad2deg(errors[-2]), 2)) + r"$\,^{\circ}$")
    #without error
#    fitProps =  (   r"$\sigma_u=$" + str(np.floor(10*popt[3]*mult)/10) + unit + "\n" + 
#                    r"$\sigma_v = $" + str(np.floor(10*popt[4]*mult)/10) + unit + "\n" + 
#                    r"$\theta=$" + str(np.floor(10*np.rad2deg(popt[-2]))/10) + r"$\,^{\circ}$")
    #plot fitted beam shape
    c = ax00.imshow(beamPofileSim, extent=(-pEdges,pEdges,pEdges,-pEdges), cmap='magma', aspect = "auto")
    ax00.set(xLabel = r"$x\ (\mathrm{\mu m})$", yLabel = r"$y\ (\mathrm{\mu m})$")
    anchored_text = AnchoredText(fitProps, loc = 1)
    ax00.add_artist(anchored_text)
    #add colorbar for this plot
    divider = make_axes_locatable(ax00)
    cax = divider.append_axes('right', size='5%', pad=0.05)
    f.colorbar(c, cax = cax)
    cax.set_ylabel(r"$(\mathrm{a. u.})$")

    angles, indices = np.unique(angle, return_index = True)
    indices = indices//(angle.shape[1])
    indices.astype(int)
    #iterate over angles to generate plot there
    for i,ax in enumerate(axList):
        #Prepare arrays to evaluate wire scanner simulation
        angleIteration = angles[i]
        indexInData = indices[i]
        angleToEval = np.reshape(np.repeat([angleIteration], pc.nPoints),(1,-1)).T
        pointsToEval = np.concatenate((pc.x.reshape((-1,1)),angleToEval), axis = 1)
        #evaluate
        wirescan = pc.wireScannerBasic(pointsToEval,*popt)
        #plot result of projection of fit result
        ax.plot(pc.x,wirescan, label = r"$\theta = $" + str(int(angleIteration)) + "°", color = cList[i])
        #compute values with error added
        wirescanErrors = getValuesWithErrorsComb(pc.wireScannerBasic, pointsToEval, popt, pcov, errorsShown)
        wirescanMin = wirescan - wirescanErrors
        wirescanMin[wirescanMin<0] = 0
        wirescanMax = wirescan + wirescanErrors
        #add results to plot in form of a grey area
        ax.fill_between(pc.x, wirescanMax, wirescanMin, color = "grey", alpha = 0.5)
        #add original data
        ax.scatter(d[indexInData,:],s[indexInData,:], c = cList[i], marker = "x")
        ax.grid()
        ax.legend(loc=1, fontsize = 10)
        ax.set_xlim(-pEdges, pEdges)
        ax.set(yLabel = r"$(\mathrm{a. u.})$")
        #add a line which helps identify the plots to the image of the2d gaussian
        ax00.plot(pc.x,pc.x*np.tan(-np.deg2rad(angleIteration)), label = r"$\theta = $" + str(angleIteration), color = cList[i], linestyle = ":")
    ax50.set(xLabel = r"$d\ (\mathrm{\mu m})$")
    ax51.set(xLabel = r"$d\ (\mathrm{\mu m})$")
    ax00.set_xlim(-pEdges,pEdges)
    ax00.set_ylim(-pEdges,pEdges)
    ax00.grid()
    plt.subplots_adjust(top=0.98,
                        bottom=0.085,
                        left=0.07,
                        right=0.945,
                        hspace=0.345,
                        wspace=0.195)
#    f.tight_layout()
#    plt.savefig("NineWiresFitMeasurement191214_small.png", dpi=600)
    
    plt.show()

def plotnWires3(pc, angle, d, s, pEdges, beamPofileSim, popt, pcov, errorsShown, scanName=None):
    """
    Plot data for 3 wire case, plot includes: 2d Gaussian, Projections,
    Raw data, error interval

    Parameters
    ----------
    pc              :   object
                        wireScannerModel object required for evaluation
    angle           :   array
                        array of angles appearing in measurement
    d               :   array
                        all displacements for measured data plotting
    s               :   array
                        all beam loss monitor data for plotting
    pEdges          :   float
                        set maximum and minimum displacement/xValue shown in plots
    beamPofileSim   :   2D array
                        simulated beam profile, edge should be f(pEdges)
    popt            :   array
                        parameters for function found via optimization
    pcov            :   array
                        covariance matrix containing information on fits confidence
    errorsShown     :   list
                        list containing the indices of all parameters which errors should be included
    """

    #set up figure for 3 wire case
    f = plt.figure(figsize = (11.4, 8))
    if scanName != None:
        f.canvas.set_window_title("Gaussian Fit - " + scanName)
    gs0 = GridSpec(1,1)
    ax00 = f.add_subplot(gs0[0])

    gs1 = GridSpec(1,1)
    ax30 = f.add_subplot(gs1[0])

    gs2 = GridSpec(2,1)
    ax01 = f.add_subplot(gs2[0])
    plt.setp(ax01.get_xticklabels(), visible=False)
    ax11 = f.add_subplot(gs2[1], sharex = ax01)

    #gen list corresponding to angles
    axList = [ax01, ax11, ax30]
    #check if swicht to nm should be done
    if popt[3] < 0.5 and popt[4] < 0.5:
        unit = r"$\,\mathrm{nm}$"
        mult = 1000
    else:
        unit = r"$\,\mathrm{\mu m}$"
        mult = 1
    cList = ["#e50000", "#15b01a", "#0343df"]
    errors = np.floor(10*np.sqrt(np.diag(pcov)))/10
    ax00.set_title( r"$\sigma_x=$" + str(np.floor(10*mult*popt[3])/10) + r"$\pm$" + str(mult*errors[3]) +
                    unit + r"$, \sigma_y = $" + str(np.floor(10*mult*popt[4])/10) + r"$\pm$" + str(mult*errors[4]) + unit +
                    r"$, \theta=$" + str(np.floor(10*np.rad2deg(popt[-2]))/10) + r"$\pm$" + str(errors[-2]) + r"$\,^{\circ}$" 
                    + "\n" +
                    r"$x_0 = $" + str(np.floor(10*popt[1])/10) + r"$\pm$" + str(errors[1]) +
                    r"$\,\mathrm{\mu m}, y_0 = $" + str(np.floor(10*popt[2])/10) + r"$\pm$" + str(errors[2]) + r"$\,\mathrm{\mu m}$")

    #plot fitted beam shape
    c = ax00.imshow(beamPofileSim, extent=(-pEdges,pEdges,pEdges,-pEdges), cmap='magma')
    ax00.set(xLabel = r"$x\,[\mu m]$", yLabel = r"$y\,[\mu m]$")
    #add colorbar for this plot
    divider = make_axes_locatable(ax00)
    cax = divider.append_axes('right', size='5%', pad=0.05)
    f.colorbar(c, cax = cax)


    #iterate over angles to generate plot there
    for i,ax in enumerate(axList):
        #Prepare arrays to evaluate wire scanner simulation
        angleIteration = np.unique(angle)[i]
        angleToEval = np.reshape(np.repeat([angleIteration], pc.nPoints),(1,-1)).T
        pointsToEval = np.concatenate((pc.x.reshape((-1,1)),angleToEval), axis = 1)
        #evaluate
        wirescan = pc.wireScannerBasic(pointsToEval,*popt)
        #plot result of projection of fit result
        ax.plot(pc.x,wirescan, label = r"$\theta = $" + str(angleIteration) + "°", color = cList[i])
        #compute values with error added
        wirescanErrors = getValuesWithErrorsComb(pc.wireScannerBasic, pointsToEval, popt, pcov, errorsShown)
        wirescanMin = wirescan - wirescanErrors
        wirescanMin[wirescanMin<0] = 0
        wirescanMax = wirescan + wirescanErrors
        #add results to plot in form of a grey area
        ax.fill_between(pc.x, wirescanMax, wirescanMin, color = "grey", alpha = 0.5)
        #add original data
        ax.scatter(d[i,:],s[i,:], c = cList[i], marker = "x")
        ax.grid()
        ax.legend(loc=1)
        ax.set_xlim(-pEdges, pEdges)
        ax.set(yLabel = r"$[\mathrm{a. u.}]$")
        #add a line which helps identify the plots to the image of the2d gaussian
        ax00.plot(pc.x,pc.x*np.tan(-np.deg2rad(angleIteration)), label = r"$\theta = $" + str(angleIteration), color = cList[i])
    ax30.set(xLabel = r"$d\,[\mu m]$")
    ax11.set(xLabel = r"$d\,[\mu m]$")
    ax00.set_xlim(-pEdges,pEdges)
    ax00.set_ylim(-pEdges,pEdges)
    ax00.grid()

    gs0.tight_layout(f, rect = [0.5,   0.5,
                                1, 1])
    gs1.tight_layout(f, rect = [0.5,   0,
                                1, 0.5])
    gs2.tight_layout(f, rect = [0,  0,
                                0.5  ,  1])

    top = min(gs0.top, gs2.top)
    bottom = max(gs1.bottom, gs2.bottom)

    #get center of gs2 to get top and bottomn for gs0 and gs1
    center = gs2.bottom + (gs2.top - gs2.bottom)/2
    gs0.update(top=top, bottom = center)
    gs1.update(bottom=bottom, top = center)
    gs2.update(top=top, bottom=bottom)


    gs0.tight_layout(f, rect = [0.5,   center,
                                1, 1 - (gs0.top-top)])
    gs1.tight_layout(f, rect = [0.5,   0,
                                1, center])
    gs2.tight_layout(f, rect = [0,   0 + (bottom-gs2.bottom),
                                0.5  , 1 - (gs2.top-top)       ])

    plt.show()

def loadAndPrepareInput(fileName, sensor, nWiresInFile, wiresUsed, map180,nPerPoint = 1, manualSave = False):
    """
    load data from file, transform it into displacement and angle formulation

    Parameters
    ----------
    fileName    :   str
                    name of file to be read
    sensor      :   str
                    name of sensor whos data is loaded
    nWiresInFile:   int
                    number of wires contained in the loaded measurement
    wiresUsed   :   list/array
                    list or array selecting which wires are included in the dataset returned
    map180      :   boolean
                    switch to turn on and of projectin of the 0-360 interval to a 0-180 interval
                    When True, mapping to 180 deg will take place
    Returns
    -------
    s       :   array
                 Array containing Beam Loss measured at (d,angle)
    d       :   array
                Array of displacements d visited
    angle   :   array
                Array of angles visited
    """
    if manualSave: 
        x, y, s = getDataManual(fileName, sensor)
    else:
        x, y, s = getData(fileName, sensor)
    
    
    x = x.reshape((nWiresInFile, -1))
    y = y.reshape((nWiresInFile, -1))
    s = s.reshape((nWiresInFile, -1))
    x, y, s = x[wiresUsed], y[wiresUsed], s[wiresUsed]
#    s=s/max(s)
    #get displacements and angles
    [d, angle] = transformRep(x,y, map180)
    for i in range(d.shape[0]):
        initGuess = (np.max(s[i])-np.min(s[i]), d[i][np.argmax(s[i])],  0.25*(np.max(d[i])-np.min(d[i])), np.min(s[i]))
        d[i] -= curve_fit(ff.gauss, d[i], s[i], p0 = initGuess)[0][1]
    if nPerPoint != 1:
        s = s.reshape(-1,nPerPoint).mean(axis = 1).reshape(len(wiresUsed),-1)
        d = d.reshape(-1,nPerPoint).mean(axis = 1).reshape(len(wiresUsed),-1)
        angle = angle.reshape(-1,nPerPoint).mean(axis = 1).reshape(len(wiresUsed),-1)
    return s/np.max(s),d,angle

def evaluateFit(pEdges, nBins, popt):
    """
    evaluate fit on grid

    Parameters
    ----------
    pEdges  :   float
                determins min and max value of grid used to evaluate function
    nBins   :   int
                set number of bins used in x and y direction
    popt    :   array
                parameters for function to evaluate
    Returns
    -------
    profile :   array
                image-like data of the distibution of the function to be evaluated
    """
    #generate meshgrid for plotting
    xIm = np.linspace(-pEdges, pEdges, nBins)
    yIm = np.linspace(-pEdges, pEdges, nBins)
    X,Y = np.meshgrid(xIm,yIm)
    xdata_tuple = (X,Y)

    #calculate results of 2d gaussian
    profile =  np.reshape(ff.twoD_Gaussian(xdata_tuple,*popt[:-1], 0),(nBins,-1))
    return profile

def prepareReadAndLoadPreset(fileName, wireUsagePreset, wiresUsedList, bunchID = '2'):
    """
    take user input and translate it for later usage

    Parameters
    ----------
    fileName        :   str
                        folder where the file to be read is located
    wireUsagePreset :   str
                        string choosing preset, options are: "all9", "all3",
                        "3of9_0","3of9_1", "3of9_2", "3wiresCustom"
    wiresUsedList   :   list/array
                        list of wires to be used, only considered with "3wiresCustom"
    Returns
    -------
    fileName    :   str
                    filename of the actual file to be loaded
    sensor      :   str
                    name of key which specifies the sensor whos data will be loaded
    nWires      :   int
                    number of wires used for the chosen representaion
    wiresUsed   :   array
                    array containing the indices of wires that are used
    """
    #select sensor name based on if its raw or live data
    allFiles = os.listdir(fileName)
    sensor = None
    for fname in allFiles:
        if "RAWScanData.h5" in fname:
            sensor = 'SensorValue/SATCL01-DBLM135:B'+bunchID+'_LOSS'
            fileName += os.sep + fname
            break
        elif "LiveData.h5" in fname:
            sensor = 'SATSY03-DLAC080-DHXP:BLM1_comp'
            fileName += os.sep + fname
            print("Currently not suported for this Analysis tool!")
            break
        elif "simTestData.h5" in fname:
            sensor = 'SimLoss'
            fileName += os.sep + fname
            print("Working with Artificial Data!")
            break
    if sensor is None:
        print('No Supported file found!')


    if wireUsagePreset == "all9":
        nWires = 9
        wiresUsed = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8])
    if wireUsagePreset == "all3":
        nWires = 3
        wiresUsed = np.array([0, 1, 2])
    if wireUsagePreset == "3of9_0":
        nWires = 3
        wiresUsed = np.array([0, 3, 6])
    if wireUsagePreset == "3of9_1":
        nWires = 3
        wiresUsed = np.array([1, 4, 7])
    if wireUsagePreset == "3of9_2":
        nWires = 3
        wiresUsed = np.array([2, 5, 8])
    if wireUsagePreset == "3wiresCustom":
        nWires = 3
        if len(wiresUsedList) == 3:
            wiresUsed = np.array(wiresUsedList)
        else:
            print("No supported number of wires to be used given")
            wiresUsed = None
    return fileName, sensor, nWires, wiresUsed

def gaussianFitWireScan(fileName, wireUsagePreset, nWiresInFile, nBins, dMultipl, pEdges, wiresUsedList = None, bunchID = '2', manualSave = False):
    """
    Perform 2d-gaussian fit on data from wire scan measurement

    Parameters
    ----------
    fileName        :   str
                        folder where the file to be read is located
    wireUsagePreset :   str
                        string choosing preset, options are: "all9", "all3",
                        "3of9_0","3of9_1", "3of9_2", "3wiresCustom"
    nWiresInFile    :   int
                        number of wires contained in the measurement file to be loaded
    nBins           :   int
                        number of Bins used per Axis for all calculations
    dMultipl        :   float
                        set size of interval used for computation in units of max(displacement)
    pEdges          :   float
                        sets min and max value of displacement shown in plots
    errorsShown     :   list
                        list containing the indices of all parameters which errors are included when
                        calculating the "error interval"
    wiresUsedList   :   list/array, optional
                        list of wires to be used, only considered with "3wiresCustom"
    """
    errorsShown =  [0,1,2,3,4,5,6]
    scanName = fileName.split('/')[-1]
    fileName, sensor, nWires, wiresUsed = prepareReadAndLoadPreset(fileName, wireUsagePreset, wiresUsedList, bunchID)
    

    s,d,angle = loadAndPrepareInput(fileName, sensor, nWiresInFile, wiresUsed, map180, manualSave=manualSave)
    
    #initial parameter guess
    x0, y0, theta0 = 0, 0, 0.5
    scanLength = np.max(d[0])-np.min(d[0])
    sig0 = scanLength/15.
    offset0 = np.min(s)
    a0Projection = np.max(s)-np.min(s)
    a0 = (a0Projection)/(2*sig0)
    
    p0 = [a0, x0, y0, sig0, sig0, theta0, offset0]
#    p0=[ 0.09373112 , 0.23098317,  0.14160041,  0.71892549 , 4.30345972 , 0.37117285,  0.09833461]
    #set up pre computed meshgrid fit function for faster computation
    pc = ff.wireScannerModel(dMultipl*np.max(np.abs(d)), nBins)

    #flatten arrays
    pointsEvaluated = np.concatenate((d.reshape((-1,1)),angle.reshape((-1,1))), axis = 1)

    #get mask to remove nan
    mask = np.any(np.isnan(pointsEvaluated), axis=1)
    mask1 = np.any(np.isnan(np.reshape(s, (-1,1))), axis = 1)
    mask |= mask1


    #do call to fit
    popt, pcov = curve_fit( pc.wireScannerBasic,pointsEvaluated[~mask], s.flatten()[~mask], p0=p0,
                            bounds = ([0,-500,-500,0,0,0,-100],[np.inf,500,500,80,80,np.pi,np.inf]), method = "trf")
    print(p0)
    print(popt)
    profile = evaluateFit(pEdges, nBins, popt)

    if nWires == 3:
        plotnWires3(pc, angle, d, s, pEdges, profile, popt, pcov, errorsShown, scanName = scanName)
    elif nWires == 9:
        plotnWires9(pc, angle, d, s, pEdges, profile, popt, pcov, errorsShown, scanName = scanName)
    else:
        print("Number of wires not supported for plotting, printing fit results...")
        print("Optimal parameters found: " + str(popt))
        print("Errors of all parameters: " +str(np.sqrt(np.diagonal(pcov))))

if __name__ == '__main__':
    #set input filename
    #fileName = "//AFS//psi.ch//user//k//kirchner_a//Documents//20190712//S2Polygon9Wires_20190713_055414"
    #fileName = "C://Users//kirchner_a//Documents//ACHIP_Smaract_Control//Analysis//Testing"
#    fileName = "/afs/psi.ch/intranet/SF/Diagnostics/ACHIP/Data/20190712/S2Polygon9Wires_20190713_055414"
#    fileName = "D://Measurement Data//ATHOS//20190712//S2Polygon9Wires_20190713_055414"
    fileName = "/afs/psi.ch/intranet/SF/Diagnostics/ACHIP/Data/20191214/WSM9Wires_20191214_152259"
    fileName = 'D:/Measurement Data/ATHOS/20191214/ACHIP/WSM9Wires_20191214_152259'
    fileName = 'D:/Measurement Data/ATHOS/20200609/S2scanZ-5241_20200609_050402'
    fileName = 'D:/Measurement Data/ATHOS/20201002/ws/ws_20201003_052444'
    fileName = 'D:/Measurement Data/ATHOS/20201222/ws_20201222_052249'
#    fileName = 'D:/Measurement Data/ATHOS/20201223/ws_20201223_014714'

    
#    specify wires to be used, some presets: "all9", "all3", "3of9_0","3of9_1", "3of9_2", "3wiresCustom"
    wireUsagePreset = "all9"
    #set number of wires in input file here
    nWiresInFile = 9
    #set if a 0 to 180 or 0 to 360 mapping should be used
    map180 = False
    #set number of bins used for computation
    nBins = 256
    #set multiplier used to get computation interval from maxiumum displacement
    dMultipl = 2
    #set max value to be plottet
    pEdges = 20
    #set errors to be considered in plot
    errorsShown =  [0,1,2,3,4,5,6]
    wiresUsedList = []
    
    gaussianFitWireScan(fileName, wireUsagePreset, nWiresInFile, nBins, dMultipl, pEdges, bunchID='2', manualSave = False)
