#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
import sys
import h5py as h5
from PyQt5.QtGui import *
from PyQt5.QtWidgets import QDialog, QApplication, QVBoxLayout, QLineEdit, QPushButton, QFileDialog, QFormLayout, QLabel, QComboBox
from wireScanToGaussian import gaussianFitWireScan
import fitFunctions as ff
import os

class Form(QDialog):
    def __init__(self, parent=None):
        super(Form, self).__init__(parent)
		
        self.dataPath = '/afs/psi.ch/intranet/SF/Diagnostics/ACHIP/Data'
        self.dialogs=[]
        self.layout = QVBoxLayout()
        self.dir = QLineEdit(self.dataPath)

        self.bDir = QPushButton("Change Directory")
        self.bDir.clicked.connect(self.getDir)

        self.bStar = QPushButton("Fit Polygon Scan Data (2D Reconstruction)")
        self.bStar.clicked.connect(self.polygonFitInterface)

        self.bGauss = QPushButton("Fit Gauss (1D)")
        self.bGauss.clicked.connect(self.gaussFitInterface)

        self.bDualGauss = QPushButton("Fit Dual Gauss (1D)")
        self.bDualGauss.clicked.connect(self.dualGaussFitInterface)

        self.bKnifeEdge = QPushButton("Fit Knife Edge (1D)")
        self.bKnifeEdge.clicked.connect(self.knifeEdgeFitInterface)

        self.layout.addWidget(self.dir)   
        self.layout.addWidget(self.bDir)
        self.layout.addWidget(self.bStar)
        self.layout.addWidget(self.bGauss)
        self.layout.addWidget(self.bDualGauss)
        self.layout.addWidget(self.bKnifeEdge)

        self.setWindowTitle("Fit Tool")
        self.setLayout(self.layout)
        self.resize(500,500)
    
    def getDir(self):
        d = QFileDialog.getExistingDirectory(self, 'Open File', self.dataPath)
        self.dataPath = d    
        self.dir.setText(d)
        return 

    def polygonFitInterface(self):
        dialog = PolygonWindow(self.dataPath, self)
        self.dialogs.append(dialog)
        dialog.show()
        return 

    def gaussFitInterface(self):
        dialog = GaussWindow(self.dataPath, self)
        self.dialogs.append(dialog)
        dialog.show()
        return 

    def dualGaussFitInterface(self):
        dialog = DualGaussWindow(self.dataPath, self)
        self.dialogs.append(dialog)
        dialog.show()
        return 

    def knifeEdgeFitInterface(self):
        dialog = KnifeEdgeWindow(self.dataPath, self)
        self.dialogs.append(dialog)
        dialog.show()
        return 


class PolygonWindow(QDialog):
    def __init__(self, path, parent=None):
        super(PolygonWindow, self).__init__(parent)
        self.dataPath = path
        self.name = self.dataPath.split('/')[-1]
        self.nBins = 512
        self.dMultiple = 2 # computation range, dMultiple*Measurement Range
        self.layout = QFormLayout()

        self.lName = QLabel(self.name)
        
        self.nWires = self.getNWires()
        self.lNWires = QLabel(str(self.nWires))
        

        self.selectionForWires = ["all9", "all3", "3of9_0","3of9_1", "3of9_2", "3wiresCustom"]
        if self.nWires == 3:
            self.selectionForWires = ["all3"]
        self.cbSelectWires = QComboBox()
        self.cbSelectWires.addItems(self.selectionForWires)

        self.selectionForNBins = ['256', '512', '1024']        
        self.cbselectNBins = QComboBox()
        self.cbselectNBins.addItems(self.selectionForNBins)

        self.selectionForPlotRange = ['5', '10', '20', '40', '80']    
        self.cbSelectPlotRange = QComboBox()
        self.cbSelectPlotRange.addItems(self.selectionForPlotRange)

        self.bRun = QPushButton("Evaluate Fit")
        self.bRun.clicked.connect(self.evaluateFit)
        
        self.layout.addRow('Directory', self.lName)
        self.layout.addRow('nWires', self.lNWires)
        self.layout.addRow('Select Wires', self.cbSelectWires)
        self.layout.addRow('Select nBins', self.cbselectNBins)   
        self.layout.addRow('Select Plot Range', self.cbSelectPlotRange)   
        self.layout.addRow('GO', self.bRun)        

        self.setWindowTitle("Polygon")
        self.layout.setVerticalSpacing(10)
        self.setLayout(self.layout)
        self.resize(400,200)

    def getNWires(self):
        scanPrepFile = h5.File(self.dataPath+'/scanPreparation.h5', 'r')
        return int(len(list(scanPrepFile['Points']))/2)
    
    def evaluateFit(self):
        
        gaussianFitWireScan(self.dataPath, str(self.cbSelectWires.currentText()), self.nWires, 
                            int(self.cbselectNBins.currentText()), self.dMultiple, int(self.cbSelectPlotRange.currentText()))


def getFileFromDir(path):
    #select File Name from path (live or raw)
    if os.path.isfile(path + '//RAWScanData.h5'):
        return path + '//RAWScanData.h5'
    if os.path.isfile(path + '//LiveData.h5'):
        return path + '//LiveData.h5'

def getSensorsFromFile(fileName):
    f = h5.File(fileName, 'r')
    sensors = []
    for s in list(f):
        if 'BLM' in s: sensors.append(s)
    if 'SensorValue' in list(f):
        for s in list(f['SensorValue']):
            if 'BLM' in s: sensors.append('SensorValue/'+s)
    for s in sensors:
        if 'RAW' in s: sensors.remove(s)    
    return sensors

class GaussWindow(QDialog):
    def __init__(self, path, parent=None):
        super(GaussWindow, self).__init__(parent)
        self.dataPath = path
        #select File Name from path (live or raw)
        self.fileName = getFileFromDir(self.dataPath)
        self.availableSensors = getSensorsFromFile(self.fileName)
        self.name = self.dataPath.split('/')[-1]
        #dict for title generation for plot
        self.poptDict = {r'$\sigma$':{'idx': 2, 'unit': 'um'},
                         r'$m$':{'idx': 1, 'unit': 'um'}}
        
        self.layout = QFormLayout()
        self.lName = QLabel(self.name)

        self.cbSensors = QComboBox()
        self.cbSensors.addItems(self.availableSensors)        

        self.directions = ['x', 'y', 'z', 'rx', 'ry', 'rz']
        self.cbDirection = QComboBox()
        self.cbDirection.addItems(self.directions)

        self.bRun = QPushButton("Evaluate Fit")
        self.bRun.clicked.connect(self.evaluateFit)
        
        self.layout.addRow('Name', self.lName)
        self.layout.addRow('Sensor', self.cbSensors)        
        self.layout.addRow('Scan Direction', self.cbDirection)
        self.layout.addRow('GO', self.bRun)        

        self.setWindowTitle("Gauss1D_"+self.name)
        self.layout.setVerticalSpacing(10)
        self.setLayout(self.layout)
        self.resize(400,200)

    def evaluateFit(self):
        ff.plotFit(self.fileName, ff.gauss, str(self.cbDirection.currentText()), str(self.cbSensors.currentText()), self.poptDict)
        return 

class DualGaussWindow(QDialog):
    def __init__(self, path, parent=None):
        super(DualGaussWindow, self).__init__(parent)
        self.dataPath = path
        #select File Name from path (live or raw)
        self.fileName = getFileFromDir(self.dataPath)
        self.availableSensors = getSensorsFromFile(self.fileName)
        self.name = self.dataPath.split('/')[-1]
        #dict for title generation for plot
        self.poptDict = {r'$\sigma$':{'idx': 2, 'unit': 'um'}, 
                         r'$m$':{'idx': 1, 'unit': 'um'}, 
                         r'$d$':{'idx': 3, 'unit': 'um'}}
        
        self.layout = QFormLayout()
        self.lName = QLabel(self.name)

        self.cbSensors = QComboBox()
        self.cbSensors.addItems(self.availableSensors)        

        self.directions = ['x', 'y', 'z', 'rx', 'ry', 'rz']
        self.cbDirection = QComboBox()
        self.cbDirection.addItems(self.directions)

        self.bRun = QPushButton("Evaluate Fit")
        self.bRun.clicked.connect(self.evaluateFit)
        
        self.layout.addRow('Name', self.lName)
        self.layout.addRow('Sensor', self.cbSensors)        
        self.layout.addRow('Scan Direction', self.cbDirection)
        self.layout.addRow('GO', self.bRun)        

        self.setWindowTitle("DualGauss1D_"+self.name)
        self.layout.setVerticalSpacing(10)
        self.setLayout(self.layout)
        self.resize(400,200)

    def evaluateFit(self):
        ff.plotFit(self.fileName, ff.dualGauss, str(self.cbDirection.currentText()), str(self.cbSensors.currentText()), self.poptDict)
        return 

class KnifeEdgeWindow(QDialog):
    def __init__(self, path, parent=None):
        super(KnifeEdgeWindow, self).__init__(parent)
        self.dataPath = path
        #select File Name from path (live or raw)
        self.fileName = getFileFromDir(self.dataPath)
        self.availableSensors = getSensorsFromFile(self.fileName)
        self.name = self.dataPath.split('/')[-1]
        #dict for title generation for plot
        self.poptDict = {r'$\sigma$':{'idx': 2, 'unit': 'um'}, 
                         r'$m$':{'idx': 1, 'unit': 'um'}}
        
        self.layout = QFormLayout()
        self.lName = QLabel(self.name)

        self.cbSensors = QComboBox()
        self.cbSensors.addItems(self.availableSensors)        

        self.directions = ['x', 'y', 'z', 'rx', 'ry', 'rz']
        self.cbDirection = QComboBox()
        self.cbDirection.addItems(self.directions)

        self.bRun = QPushButton("Evaluate Fit")
        self.bRun.clicked.connect(self.evaluateFit)
        
        self.layout.addRow('Name', self.lName)
        self.layout.addRow('Sensor', self.cbSensors)        
        self.layout.addRow('Scan Direction', self.cbDirection)
        self.layout.addRow('GO', self.bRun)        

        self.setWindowTitle("KnifeEdge_"+self.name)
        self.layout.setVerticalSpacing(10)
        self.setLayout(self.layout)
        self.resize(400,200)

    def evaluateFit(self):
        ff.plotFit(self.fileName, ff.knifeEdge, str(self.cbDirection.currentText()), str(self.cbSensors.currentText()), self.poptDict)
        return 

def main():
   app = QApplication(sys.argv)
   ex = Form()
   ex.show()
   sys.exit(app.exec_())
	
if __name__ == '__main__':
   main()
