# -*- coding: utf-8 -*-

import h5py 
import numpy as np
import matplotlib.pyplot as plt

def removeNan(dataList):
    isNan = np.logical_or.reduce([np.isnan(d) for d in dataList])
    idx = np.argwhere(np.logical_not(isNan)).flatten()
    return [d[idx] for d in dataList]

fileName = "D:/Measurement Data/ATHOS/20201222/alignmentGapVall_20201222_041904/LiveData.h5"

f = h5py.File(fileName, "r")
s = np.array(f['SATSY03-DLAC080-DHXP:BLM1_comp'].value)

x = np.array(f['PositionHEX/x'].value)
y = np.array(f['PositionHEX/y'].value)

s,x,y  = removeNan([s,x,y])

fig = plt.figure()
ax = fig.add_subplot(111)

ax.scatter(x, y, c=s/np.max(s))
