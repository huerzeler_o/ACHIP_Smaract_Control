# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import h5py as h5
import fitFunctions as ff
from scipy.optimize import curve_fit
props = dict(boxstyle='round', facecolor='wheat', alpha=0.3)

fileName = 'D://Measurement Data//ATHOS//20190712//S2Polygon9Wires_20190713_055414//RAWScanData.h5'
sensor = 'SensorValue/SATCL01-DBLM135:B1_LOSS'
sensorRaw = 'SensorValue/SATCL01-DBLM135:LOSS_SIGNAL_RAW'
#sensor = 'SensorValue/SATSY03-DBLM085:B1_LOSS'
#sensorRaw = 'SensorValue/SATSY03-DBLM085:LOSS_SIGNAL_RAW'
#fileName = '//AFS//psi.ch//user//k//kirchner_a//Documents//20190712//S2fullAlignment_20190713_044105//LiveData.h5'
#fileName = 'D://Measurement Data//ATHOS//20190712//S2centerCircleScanBothLive_20190713_053102//LiveData.h5'
#sensor = 'SATSY03-DLAC080-DHXP:BLM1_comp'
nWires = 9
sortIndex = np.array([0,5,1,6,2,7,3,8,4])

showProjections = 0
showXY = True


def getData(fileName):
    f = h5.File(fileName, 'r')
#    print(list(f))
    x = np.array(f['PositionHEX']["x"])
    y = np.array(f['PositionHEX']["y"])
    
    s = np.array(f[sensor])
    x, y, s = removeNan([x,y,s])
    return x,y,s

def getDataRaw(fileName):
    f = h5.File(fileName, 'r')
    x = np.array(f['PositionHEX']["x"])
    y = np.array(f['PositionHEX']["y"])
    s = np.array(f[sensorRaw])[:, :1500]
    maxIdx = np.unravel_index(s.argmin(), s.shape)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(s.T)
#    return x,y, np.sum(s[:,520:1000], axis=1)/1e5
    return x,y, np.min(s, axis=1)/1e5

def removeNan(dataList):
    isNan = np.logical_or.reduce([np.isnan(d) for d in dataList])
    idx = np.argwhere(np.logical_not(isNan)).flatten()
    return [d[idx] for d in dataList]
       
def plotXY(x,y,s, scaleRange = [0,1]):
    
    fig = plt.figure(figsize = (12,5), dpi=200)
    ax1 = fig.add_subplot(131)
    ax2 = fig.add_subplot(132)
    ax3 = fig.add_subplot(133)
    
    cmap = plt.get_cmap('gnuplot')
    dataReduction = 1
    c = s-min(s)
    c/=max(c)
    
    c-=scaleRange[0]
    c/= (scaleRange[1]-scaleRange[0])
    
    ax1.plot(x, c, marker = '+', ls = '')
    ax2.plot(y, c, marker = '+', ls = '')
    ax3.scatter(x[::dataReduction], y[::dataReduction], color=cmap(c[::dataReduction]), s = 6)
    
    ax1.set_xlabel('x (um)')
    ax1.set_ylabel('Norm. Loss')
    ax2.set_xlabel('y (um)')
    ax2.set_ylabel('Norm. Loss')    
    ax3.set_ylim(ax3.get_ylim()[::-1])
    ax3.set_aspect('equal')
    ax3.set_xlabel('x (um)')
    ax3.set_ylabel('y (um)')
    ax3.set_title('2D Map')
    ax1.grid();ax2.grid();ax3.grid()
    
    fig.tight_layout()


def plotWireProjection(x, y, s, ax):
    d = np.sqrt((x-x[0])**2+(y-y[0])**2)
    d-= np.average(d)
    ax.plot(d, s, marker="+", ls=" ")
    angle = np.arctan((y[-1]-y[0])/(x[-1]-x[0]))
    ax.set_title(str((np.round(90+180/np.pi*angle)%180))+" deg")
    
    p=curve_fit(ff.gauss, d, s, p0=None)[0]
    xFit = np.linspace(min(d), max(d), 300)
    yFit = ff.gauss(xFit, *p)
    ax.plot(xFit, yFit, color='k')
    textstr = r"$\sigma_{fit}$ = " + str(np.round(abs(p[2]), 1)) + " um" 
   
    ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=11, verticalalignment='top', bbox=props)
    ax.set_xlim(-30, 30)
    ax.set_xlabel("d(x,y) (um)")
    ax.set_ylabel("Loss (a. u.)")
    return 
    
if __name__ == "__main__":    
        
    if showProjections:
        fig = plt.figure(figsize = (12,9), dpi=200)
        ax1 = fig.add_subplot(331)
        ax2 = fig.add_subplot(332)
        ax3 = fig.add_subplot(333)
        ax4 = fig.add_subplot(334)
        ax5 = fig.add_subplot(335)
        ax6 = fig.add_subplot(336)
        ax7 = fig.add_subplot(337)
        ax8 = fig.add_subplot(338)
        ax9 = fig.add_subplot(339)
        axs = [ax1, ax2, ax3, ax4, ax5, ax6, ax7, ax8, ax9]
        x, y, s = getData(fileName)
        x = x.reshape((nWires, -1))
        y = y.reshape((nWires, -1))
        s = s.reshape((nWires, -1))
        x, y, s = x[sortIndex], y[sortIndex], s[sortIndex]
        for i in range(nWires):
            plotWireProjection(x[i], y[i], s[i], axs[i])
            
        fig.tight_layout()
        #fig.savefig("//afs//psi.ch//intranet//SF//Diagnostics//ACHIP//Data//20190712//plots//wireTomo.png", dpi=600)
    
    if showXY:
        x, y, s = getData(fileName)
        plotXY(x,y,s, scaleRange = [0.2,0.4])
        
    
    plt.show()
    
