# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import h5py as h5
from fitFunctions import knifeEdge
from scipy.optimize import curve_fit

fileName = 'D:\\Measurement Data\\20190615_ATHOS\\S3 scan x left edge_20190615_134625\\LiveData.h5'
#fileName = 'D:\\Measurement Data\\20190615_ATHOS\\S3 scan x right edge_20190615_131900\\LiveData.h5'
direction = 'x'
sensor = 'SATSY03-DLAC080-DHXP:BLM1_comp'

fileName = 'D:\\Measurement Data\\20190615_ATHOS\\S3 scan y top edge near left edge_20190615_140756\\LiveData.h5'
direction = 'y'
sensor = 'SATSY03-DLAC080-DHXP:BLM1_comp'

#fileName = 'D:\\Measurement Data\\20190615_ATHOS\\S3 scan upper edge_20190615_125601\\RAWScanData.h5'
#direction = 'y'
#sensor = 'SensorValue/SATSY03-DBLM085:B1_LOSS'


fig = plt.figure(figsize = (5,4), dpi = 200)
ax = fig.add_subplot(111)

def getData(fileName):
    f = h5.File(fileName, 'r')
    x = np.array(f['PositionHEX'][direction])
    y = np.array(f[sensor])
    x, y = removeNan(x,y)
    x-=np.average(x)
    return x,y

def removeNan(x, y):
    isNan = np.logical_and(np.isnan(x), np.isnan(y))
    idx = np.argwhere(np.logical_not(isNan)).flatten()
    return x[idx], y[idx]
       
def plotScatter(fileName, ax): 
    ax.scatter(*getData(fileName), marker='.', s=10)
    return 

def plotFit(fileName, ax):
    x, y = getData(fileName)
    p=curve_fit(knifeEdge, x, y, p0=None)[0]
    xFit = np.linspace(min(x), max(x), 300)
    yFit = knifeEdge(xFit, *p)
    ax.plot(xFit, yFit, color='k')
    return p[2]

plotScatter(fileName, ax)
sig = plotFit(fileName, ax)
print(sig)

ax.set_title(r'Knife Edge Scan, $\sigma_{'+direction+'}$ = ' + str(np.round(abs(sig), 1)) + ' um')
ax.set_xlabel('x (um)')
ax.set_ylabel('Beam Loss Monitor (arb. units)')