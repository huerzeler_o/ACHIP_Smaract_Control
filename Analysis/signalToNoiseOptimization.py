# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import h5py as h5
import scipy as sp
import scipy.integrate as integrate

fileName = 'D://Measurement Data//ATHOS//20190712//S2Polygon9Wires_20190713_055414//RAWScanData.h5'
sensor = 'SensorValue/SATCL01-DBLM135:B1_LOSS'
#sensor = 'SensorValue/SATSY03-DBLM085:B1_LOSS'
sensorRaw = 'SensorValue/SATCL01-DBLM135:LOSS_SIGNAL_RAW'
#sensorRaw = 'SensorValue/SATSY03-DBLM085:LOSS_SIGNAL_RAW'

nWires = 9
idxWire = 5
blmRange = (0, 1500)
blmBkgRange = (0,50)
profileBkgRange = (0,10)

f = h5.File(fileName, 'r')
x = np.array(f['PositionHEX']["x"])
y = np.array(f['PositionHEX']["y"])
s = np.array(f[sensorRaw])
profileDirect = np.array(f[sensor])
profileDirect2 = np.array(f[sensor2])
x = x.reshape((nWires, -1))[idxWire]
y = y.reshape((nWires, -1))[idxWire]
s = s.reshape((nWires, -1, 2048))[idxWire]
profileDirect = profileDirect.reshape((nWires, -1))[idxWire]
profileDirect2 = profileDirect2.reshape((nWires, -1))[idxWire]
optimizeBoundary = True

def profileFromRaw(s, a=0, b=-1, method = 'sum'):
    
    if method =='sum':
        profile = np.sum(s[:, a:b], axis=1)
    if method =='trapz':
        profile = np.trapz(s[:, a:b], axis=1)
    if method == 'simps':
        profile = integrate.simps(s[:, a:b], axis=1)
        
    profile = normalize(profile)
#    print('SNR: ', snr(profile))
    return profile

def normalize(profile):
    profileNew = profile - np.average(profile[profileBkgRange[0]:profileBkgRange[1]])
    if np.max(profileNew>-np.min(profileNew)):
        profileNew/=np.max(profileNew)
    else:
        profileNew/=np.min(profileNew)
    return profileNew

def snr(profile):
    profile = normalize(profile)
    bkgNoise = np.std(profile[profileBkgRange[0]:profileBkgRange[1]])
    return np.max(profile)/bkgNoise

blmBkg = np.average(s[:, blmBkgRange[0]:blmBkgRange[1]])
s-=blmBkg
s/=np.min(s)

if optimizeBoundary:
    snrs = []
    lowerBounds = [] 
    upperBounds = [] 
    for a in np.arange(480,510, 1, int):
        for b in np.arange(a+2, 550, 1, int):
            p = profileFromRaw(s, a=a, b=b)
            snrs.append(snr(p))
            lowerBounds.append(a)
            upperBounds.append(b)
            
    
    
    
    plt.figure()
    plt.tricontourf(lowerBounds, upperBounds, snrs)
    
    optimalLower = lowerBounds[np.argmax(snrs)]
    optimalUpper = upperBounds[np.argmax(snrs)]
else:
    optimalLower = 490
    optimalUpper = 502
    

print('optimalSNR: ', np.max(snrs))
print('optimalLower: ', optimalLower)
print('optimalUpper: ', optimalUpper)

plt.figure()
plt.plot(profileFromRaw(s, optimalLower, optimalUpper), marker = 'o', ls = '')
plt.plot(normalize(profileDirect), marker = 'o', ls = '')
#plt.plot(normalize(profileDirect2), marker = 'o', ls = '')

print('direct profile SNR: ', snr(profileDirect))
plt.show()

