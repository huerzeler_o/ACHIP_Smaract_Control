# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import h5py as h5
import fitFunctions as ff
from scipy.optimize import curve_fit
import plotWireTomography as pwt
import scipy.constants as sc
from matplotlib.gridspec import GridSpec
from mpl_toolkits.axes_grid1 import make_axes_locatable
import os
import reconstructTomographically as rt
import wireScanToGaussian as rf
from matplotlib.offsetbox import AnchoredText

plt.rcParams.update({'font.size': 12})

def plot9WiresAndTomo(pc, angle, d, s, pEdges, beamPofileSim, popt, pcov, errorsShown, profileTomo, scanName=None):
    """
    Plot data for 9 wire case, plot includes: 2d Gaussian, Projections,
    Raw data, error interval

    Parameters
    ----------
    pc              :   object
                        wireScannerModel object required for evaluation
    angle           :   array
                        array of angles appearing in measurement
    d               :   array
                        all displacements for measured data plotting
    s               :   array
                        all beam loss monitor data for plotting
    pEdges          :   float
                        set maximum and minimum displacement/xValue shown in plots
    beamPofileSim   :   2D array
                        simulated beam profile, edge should be f(pEdges)
    popt            :   array
                        parameters for function found via optimization
    pcov            :   array
                        covariance matrix containing information on fits confidence
    errorsShown     :   list
                        list containing the indices of all parameters which errors should be included
    profileTomo     :   array
                        profile reconstructed via tomography
    scanName        :   str, optional
                        name of measurement for plots title
    """
    #set up figure for 9 wire case
    f = plt.figure(figsize = (14, 7))
    if scanName != None:
        f.canvas.set_window_title("Gaussian Fit - " + scanName)
    width = 4
    center = width //2
    gs = GridSpec(6,width)
    ax0 = f.add_subplot(gs[4:,center:center+center//2])
    ax00 = f.add_subplot(gs[4:,center+center//2:], sharex = ax0, sharey = ax0)
    plt.setp(ax00.get_yticklabels(), visible=False)
    #cax = f.add_subplot(gs[0:3,-1])
    ax30 = f.add_subplot(gs[0, center:])
    ax40 = f.add_subplot(gs[1, center:], sharex = ax30)
    ax50 = f.add_subplot(gs[2, center:], sharex = ax40)
    ax01 = f.add_subplot(gs[0,0:center], sharex = ax50)
    ax11 = f.add_subplot(gs[1,0:center], sharex = ax01)
    ax21 = f.add_subplot(gs[2,0:center], sharex = ax11)
    ax31 = f.add_subplot(gs[3,0:center], sharex = ax21)
    ax41 = f.add_subplot(gs[4,0:center], sharex = ax31)
    ax51 = f.add_subplot(gs[5,0:center], sharex = ax41)
    plt.setp(ax30.get_xticklabels(), visible=False)
    plt.setp(ax40.get_xticklabels(), visible=False)
    plt.setp(ax01.get_xticklabels(), visible=False)
    plt.setp(ax11.get_xticklabels(), visible=False)
    plt.setp(ax21.get_xticklabels(), visible=False)
    plt.setp(ax31.get_xticklabels(), visible=False)
    plt.setp(ax41.get_xticklabels(), visible=False)
    #gen list corresponding to angles
    axList = [ax01,ax11,ax21, ax31, ax41, ax51, ax30, ax40, ax50]
    cList = ["#e50000", "#15b01a", "#0343df", "#06c2ac", "#aaff32", "#f97306", "#7e1e9c", "#ff81c0", "#929591"]
    errors = np.floor(10*np.sqrt(np.diag(pcov)))/10
    #check if swicht to nm should be done
    if popt[3] < 0.5 and popt[4] < 0.5:
        unit = r"$\,\mathrm{nm}$"
        mult = 1000
    else:
        unit = r"$\,\mathrm{\mu m}$"
        mult = 1
    fitProps =  (r"$\sigma_x=$" + str(np.floor(10*popt[3]*mult)/10) + r"$\pm$" + str(errors[3]*mult) + unit + "\n" + 
                    r"$\sigma_y = $" + str(np.floor(10*popt[4]*mult)/10) + r"$\pm$" + str(errors[4]*mult) + unit + "\n" + 
                    r"$\theta=$" + str(np.floor(10*np.rad2deg(popt[-2]))/10) + r"$\pm$" + str(errors[-2]) + r"$\,^{\circ}$")
    vMin = min(np.min(beamPofileSim), np.min(profileTomo))
    vMax = max(np.max(beamPofileSim), np.max(profileTomo))
    ax00.set_title("Fit")
    #plot fitted beam shape
    c = ax00.imshow(beamPofileSim, extent=(-pEdges,pEdges,pEdges,-pEdges), cmap='magma',vmin = vMin, vmax = vMax, interpolation  = 'none')
    ax00.set(xLabel = r"$x\,[\mu m]$")#, yLabel = r"$y\,[\mu m]$")
    #anchored_text = AnchoredText(fitProps, loc = 2)
    #ax0.add_artist(anchored_text)
    #boxProps = dict(boxstyle='round', facecolor='white', alpha=1)
    #ax0.text(0.05, 0.95, fitProps, transform=ax0.transAxes, verticalalignment='top', bbox = boxProps)
    #add colorbar for this plot
    divider = make_axes_locatable(ax00)
    cax = divider.append_axes('right', size='5%', pad=0.05)
    
    #plot tomographic beam shape
    ax0.set_title("Tomography")
    c1 = ax0.imshow(profileTomo, extent=(-pEdges,pEdges,pEdges,-pEdges), cmap='magma', vmin = vMin, vmax = vMax, interpolation  = 'none')
    ax0.set(xLabel = r"$x\,[\mu m]$", yLabel = r"$y\,[\mu m]$")
    #add colorbar for this plot
    #divider1 = make_axes_locatable(ax0)
    #cax1 = divider1.append_axes('right', size='5%', pad=0.05)
    f.colorbar(c, cax = cax)
    cax.set_ylabel(r"$[\mathrm{a. u.}]$")
    #f.colorbar(c1, cax = cax1)
    #cax1.set_ylabel(r"$[\mathrm{a. u.}]$")

    angles, indices = np.unique(angle, return_index = True)
    indices = indices//(angle.shape[1])
    indices.astype(int)
    #iterate over angles to generate plot there
    for i,ax in enumerate(axList):
        #Prepare arrays to evaluate wire scanner simulation
        angleIteration = angles[i]
        indexInData = indices[i]
        angleToEval = np.reshape(np.repeat([angleIteration], pc.nPoints),(1,-1)).T
        pointsToEval = np.concatenate((pc.x.reshape((-1,1)),angleToEval), axis = 1)
        #evaluate
        wirescan = pc.wireScannerBasic(pointsToEval,*popt)
        wirescanTomo = pc.wireScannerBasic(pointsToEval,*popt, profile = profileTomo)
        #plot result of projection of fit result
        ax.plot(pc.x,wirescan, label = r"$\theta = $" + str(int(angleIteration)) + "°", color = cList[i])
        ax.plot(pc.x,wirescanTomo, label = "Tom.", color = "black")
        #compute values with error added
        wirescanErrors = rf.getValuesWithErrorsComb(pc.wireScannerBasic, pointsToEval, popt, pcov, errorsShown)
        wirescanMin = wirescan - wirescanErrors
        wirescanMin[wirescanMin<0] = 0
        wirescanMax = wirescan + wirescanErrors
        #add results to plot in form of a grey area
        ax.fill_between(pc.x, wirescanMax, wirescanMin, color = "grey", alpha = 0.5)
        #add original data
        ax.scatter(d[indexInData,:],s[indexInData,:], c = cList[i], marker = "x")
        ax.grid()
        ax.legend(loc=1)
        ax.set_xlim(-pEdges, pEdges)
        ax.set(yLabel = r"$[\mathrm{a. u.}]$")
        #add a line which helps identify the plots to the image of the 2d gaussian
        ax0.plot(pc.x,pc.x*np.tan(-np.deg2rad(angleIteration)), color = cList[i], linestyle = ":")
        ax00.plot(pc.x,pc.x*np.tan(-np.deg2rad(angleIteration)), color = cList[i], linestyle = ":")
    ax50.set(xLabel = r"$d\,[\mu m]$")
    ax51.set(xLabel = r"$d\,[\mu m]$")
    ax0.set_xlim(-pEdges,pEdges)
    ax0.set_ylim(-pEdges,pEdges)
    ax0.grid()
    ax00.set_xlim(-pEdges,pEdges)
    ax00.set_ylim(-pEdges,pEdges)
    ax00.grid()
    #gs.tight_layout(f)
    plt.subplots_adjust(top=0.985,
                        bottom=0.085,
                        left=0.07,
                        right=0.945,
                        hspace=0.2,
                        wspace=0.46)
#    plt.savefig("Analysis/Plots/NineWiresTomoAndFitMeasurement12_07_2019.pdf")
    plt.show()

#set input stuff here

#set input filename
fileName = "//AFS//psi.ch//user//k//kirchner_a//Documents//20190712//S2Polygon9Wires_20190713_055414"
#fileName = "C://Users//kirchner_a//Documents//ACHIP_Smaract_Control//Analysis//Testing"
#fileName = "/afs/psi.ch/intranet/SF/Diagnostics/ACHIP/Data/20190712/S2Polygon9Wires_20190713_055414"
#fileName = "D://Measurement Data//ATHOS//20190712//S2Polygon9Wires_20190713_055414"
fileName = "/afs/psi.ch/intranet/SF/Diagnostics/ACHIP/Data/20191214/WSM9Wires_20191214_152259"

#fileName = "/afs/psi.ch/intranet/SF/Diagnostics/ACHIP/Data/20200118/S2_WS_centerY_9.5pC_20200118_103142" #before optimization
#fileName = "D://Measurement Data//ATHOS//20200118//S2_WS_NewFocus_20200118_111053" #after optimization

#specify wires to be used, some presets: "all9", "all3", "3of9_0","3of9_1", "3of9_2", "3wiresCustom"
wireUsagePreset = "all9"
#set number of wires in input file here
nWiresInFile = 9
#set if a 0 to 180 or 0 to 360 mapping should be used
map180 = False
#set number of bins used for computation
nBins = 256
#set max value to be plottet
pEdges = 20
#set errors to be considered in plot
errorsShown =  [0,1,2,3,4,5,6]
wiresUsedList = []
#set what to do with tomography projection matrices, options: "interpolate", "loadInterpolated"
matrixHandeling = "interpolate"
#set filename for huge input matrix
fnameMatrix = "TransformMatrices/Membrane20191214.h5"
nStepsTomo = 512
errorsShown =  [0,1,2,3,4,5,6]
scanName = fileName.split('/')[-1]

fileName, sensor, nWires, wiresUsed = rf.prepareReadAndLoadPreset(fileName, wireUsagePreset, wiresUsedList, bunchID='2')

s,d,angle = rf.loadAndPrepareInput(fileName, sensor, nWiresInFile, wiresUsed, map180)

#load matrix for tomography
if matrixHandeling == "interpolate":
    #interpolate Matrix
#    Ainterp, dMat, xMat, nBinsRecGrid, anglesMat = rt.interpolateMatrix(fnameMatrix, d.reshape(-1,2).mean(axis = 1).reshape(len(wiresUsed), -1), "Measurement" + scanName)
    Ainterp, dMat, xMat, nBinsRecGrid, anglesMat = rt.interpolateMatrix(fnameMatrix, d, "Measurement" + scanName)
elif matrixHandeling == "loadInterpolated":
    Ainterp, dMat, xMat, nBinsRecGrid, anglesMat = rt.readMatrix(fnameMatrix, "Measurement" + scanName)

#initial parameter guess
x0, y0, theta0 = 0, 0, 0
scanLength = np.max(d[0])-np.min(d[0])
sig0 = scanLength/15.
offset0 = np.min(s)
a0Projection = np.max(s)-np.min(s)
a0 = (a0Projection)/(2*sig0)

p0 = [a0, x0, y0, sig0, sig0, theta0, offset0]
#set up pre computed meshgrid fit function for faster computation
pc = ff.wireScannerModel(pEdges, nBinsRecGrid)

#flatten arrays
pointsEvaluated = np.concatenate((d.reshape((-1,1)),angle.reshape((-1,1))), axis = 1)

#get mask to remove nan
mask = np.any(np.isnan(pointsEvaluated), axis=1)
mask1 = np.any(np.isnan(np.reshape(s, (-1,1))), axis = 1)
mask |= mask1

#do call to fit
popt, pcov = curve_fit( pc.wireScannerBasic,pointsEvaluated[~mask], s.flatten()[~mask], p0=p0,
                        bounds = ([0,-500,-500,0,0,0,0],[np.inf,500,500,80,80,np.pi,np.inf]), method = "trf")
print(p0)
print(popt)
#generate profile from fit
profile = rf.evaluateFit(pEdges, nBinsRecGrid, popt)
#begin with tomographic reconstruction

profileTomo = rt.applyTomography(profile, Ainterp, nBinsRecGrid, s.reshape(len(wiresUsed), -1), nStepsTomo, 0.01, nWires, 0.6, popt[-1])

if nWires == 9:
    plot9WiresAndTomo(pc, angle, d, s, pEdges, profile, popt, pcov, errorsShown, profileTomo, scanName = scanName)