import numpy as np
from reconstructTomographically import generateMatrix
#Output Filename
fnameOut = "TransformMatrices/20191214.h5"
#number of Pixels on x-axis (square image is assumed) supported by matrix.
#This has to match the Pixel number used later.
nBinsRecGrid = 256
#edge of reconstruction grid
edgeRecGrid = 20
#wire displacements
dMat = np.linspace(-edgeRecGrid, edgeRecGrid, nBinsRecGrid)
angles = [0,40,80,120,160,200,240,280,320]  # for free standing
angles = [30,70,110,150,190,230,270,310,350]  # for membrane standing

A, dMat, xMat, nBinsRecGrid, anglesMat = generateMatrix(fnameOut, nBinsRecGrid, edgeRecGrid, dMat, angles)
print("Finished")
