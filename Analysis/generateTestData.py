# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import h5py as h5
import fitFunctions as ff
from scipy.optimize import curve_fit
import plotWireTomography as pwt
import scipy.constants as sc
from matplotlib.gridspec import GridSpec
from mpl_toolkits.axes_grid1 import make_axes_locatable
import os
import reconstructTomographically as rt
import wireScanToGaussian as rf

def genPoly(normalAxis,nWires,radius,scanLength,offAngle,center):
    '''
    this function generates a polygon path for wire scan tomography. 
    Input
    center: np.array([x,y,z,rx,ry,rz]) in um,deg
    nWires: int 
    radius: flaot um
    scanLength: flaot um
    offAngle: offset angle of the wire star, 0 means wire 0 is horizontal (aligned with pos. x axis). in deg

    Output
    A list of length 2*nWires is returned. Points in the same format as center are returned in the following order:
    start wire 0, end wire 0, start wire 1, end wire 1, ... 
    '''
    scanPointsPolygon = []
    if normalAxis == 'x':
        coordinate1 = 1
        coordinate2 = 2

    elif normalAxis == 'y':
        coordinate1 = 0
        coordinate2 = 2

    if normalAxis == 'z':
        coordinate1 = 0
        coordinate2 = 1

    for i in range(nWires):
        beta = 2.*np.pi/nWires*i + offAngle/180*np.pi # angle of i-th wire in rad 
        for j in range(2): # for start and end point
            offset = np.zeros(6)
            offset[coordinate1] = radius*np.cos(beta) + (-1)**(j+1)*scanLength/2.*np.sin(-beta)
            offset[coordinate2] = radius*np.sin(beta) + (-1)**(j+1)*scanLength/2.*np.cos(-beta)
            scanPointsPolygon.append(center+offset)
    return scanPointsPolygon

#setup polygon
normalAxis = "z"
nWires = 9
radius = 3
scanLength = 2
offAngle = 0
center = np.array([0,0,0,0,0,0])
nStepsLine = 51
#edges of field used to sim beam
pEdges = 2
#nBins used for sim of beam
nBins = 2048
#parameters of gaussian shaped beam
popt = [175,0,0,0.3,0.35,0,0]
#offest to add on Data
offset = 150
#noise on data
noiseAmplitude = 15
#points per position
nPointsPerPos = 2
#fname for simulated data
fnameScanSim = "Analysis/Testing/simTestData.h5"
#generate corners of polygon
polyCorners = genPoly(normalAxis, nWires, radius, scanLength, offAngle, center)
pointsToEvalAt = np.zeros((6,nWires*nStepsLine))
#generate all points on polygon
for k in range(0,len(polyCorners), 2):
    pointsToEvalAt[0][k//2*nStepsLine:(k//2+1)*nStepsLine] = np.linspace(polyCorners[k][0], polyCorners[k+1][0], nStepsLine)
    pointsToEvalAt[1][k//2*nStepsLine:(k//2+1)*nStepsLine] = np.linspace(polyCorners[k][1], polyCorners[k+1][1], nStepsLine)
    pointsToEvalAt[2][k//2*nStepsLine:(k//2+1)*nStepsLine] = np.zeros((nStepsLine))
    pointsToEvalAt[3][k//2*nStepsLine:(k//2+1)*nStepsLine] = np.zeros((nStepsLine))
    pointsToEvalAt[4][k//2*nStepsLine:(k//2+1)*nStepsLine] = np.zeros((nStepsLine))
    pointsToEvalAt[5][k//2*nStepsLine:(k//2+1)*nStepsLine] = np.zeros((nStepsLine))
pointsToEvalAt = np.repeat(pointsToEvalAt, nPointsPerPos, axis = 1)
#get x and y in terms of wires
x = pointsToEvalAt[0].reshape((nWires, -1))
y = pointsToEvalAt[1].reshape((nWires, -1))
#generate beam profile
profileSim = rf.evaluateFit(pEdges, nBins, popt)
pc = ff.wireScannerModel(pEdges, nBins)
#get displacements
d, angle = rf.transformRep(x,y, False)

scanResNoNoise = pc.wireScannerBasic(np.array([d.flatten(), angle.flatten()]).T, 0,0,0,0,0,0,0, profile = profileSim)
scanResNoNoise += offset
scanResNoise = scanResNoNoise + noiseAmplitude * 2 * (np.random.rand(scanResNoNoise.shape[0]) - 0.5)
with h5.File(fnameScanSim, 'w') as hdf:
    hdf.create_dataset('PositionHEX/x' , data=pointsToEvalAt[0])
    hdf.create_dataset('PositionHEX/y' , data=pointsToEvalAt[1])
    hdf.create_dataset('PositionHEX/z' , data=pointsToEvalAt[2])
    hdf.create_dataset('PositionHEX/rx' , data=pointsToEvalAt[3])
    hdf.create_dataset('PositionHEX/ry' , data=pointsToEvalAt[4])
    hdf.create_dataset('PositionHEX/rz' , data=pointsToEvalAt[5])
    hdf.create_dataset('SimLoss' , data=scanResNoise)
for i in range(0, d.shape[0]):
    plt.plot(d[i,:], scanResNoise.reshape((nWires, -1))[i,:])
plt.show()
print("Finished")
