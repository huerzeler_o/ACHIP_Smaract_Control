# -*- coding: utf-8 -*-

from plotWireTomography import getData


fileName = '//afs//psi.ch//intranet//SF//Diagnostics//ACHIP//Data//20190712//S2Polygon9Wires_20190713_055414//RAWScanData.h5'
sensor = 'SensorValue/SATCL01-DBLM135:B1_LOSS'
sensorRaw = 'SensorValue/SATCL01-DBLM135:LOSS_SIGNAL_RAW'
nWires = 9

sortIndex = np.array([0,5,1,6,2,7,3,8,4])

fig = plt.figure(figsize = (6,6), dpi=200)
ax1 = fig.add_subplot(111)

def plotSinogram(x, y, s, ax):
    xSorted = x[sortIndex, :]
    ySorted = y[sortIndex, :]
    sSorted = s[sortIndex, :]
    norm = np.max(sSorted, axis=1)

#    sSorted = np.divide(sSorted.T, norm).T
    ax.imshow(sSorted, aspect='auto', origin = 'upper')

x, y, s = getData(fileName)
x = x.reshape((nWires, -1))
y = y.reshape((nWires, -1))
s = s.reshape((nWires, -1))

plotSinogram(x,y,s,ax1)