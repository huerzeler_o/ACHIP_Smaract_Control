# -*- coding: utf-8 -*-

import numpy as np
import h5py as h
import matplotlib.pyplot as plt
plt.close("all")

from dataCollection import dataSet20200118 as dataSet

from wakeAnalyser import loadStack, filterStack

for d in dataSet:
    if d["category"] == "XB scan":
#    if d["category"] == "L1 phase scan":
        
        print(d['file'])
        imgStack, x, y = loadStack(d)
        imgStackOn = filterStack(imgStack)
    #    plt.plot(x, np.sum(imgStackOn, axis=1).T, color= "k", alpha= 0.2, lw=0.5)
        
        
        d['x'] = x
        d['xProjection'] = np.sum(imgStackOn, axis=1).T
        
        fileName = 'measurementSpectrum'+str(d['l1Pha'])+'_'+str(d['xbAmp'])+'_OFF'
        if d['structure'] == True:
            fileName = 'measurementSpectrum'+str(d['l1Pha'])+'_'+str(d['xbAmp'])+'_ON'
        
        np.save('/afs/psi.ch/intranet/SF/Diagnostics/ACHIP/Data/20200118/Spectrum/'+fileName+'.npy', d)
        