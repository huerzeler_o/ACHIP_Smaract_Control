import numpy as np
import h5py as h
import matplotlib.pyplot as plt
import os



import scipy.interpolate as si
from scipy.ndimage.morphology import binary_erosion as be


def load(fileName, vMin, zeroCrossingString):
    f = h.File(fileName, 'r')
    
    images = np.array(f['Raw_data/Beam images'+zeroCrossingString].value)
    image = images[2,-1] # first index phase, second index multiple shots same phase
    x = np.array(f['Raw_data/xAxis'].value)
    y = np.array(f['Raw_data/yAxis'].value)
    calibFactor1 = f['Meta_data/Calibration factor 1'].value
    calibFactor2 = f['Meta_data/Calibration factor 2'].value
    calibFactor1, calibFactor2 = calcTimeFactors(fileName, vMin)
    dispersion = -f['Input/Dispersion'].value
    print(dispersion)
    e0 = f['Input/Energy'].value
    print(e0)
    e = x*1e-6/dispersion*e0
    t1 = y/calibFactor1
    t2 = y/calibFactor2
    t=t1
    if zeroCrossingString == " 2":
        t = t2
    return image, t,e, x,y

def subtractBackground(img):
    b0 = np.average(img[:10, :10])
    b1 = np.average(img[:10, -10:])
    b2 = np.average(img[-10:, :10])
    b3 = np.average(img[-10:, -10:])
    
    return img-0.25*(b0+b1+b2+b3)

def reduceSpots(img):
    img = img*(img>1e-2*np.max(img))
    img = img*be(img, iterations =3)
    return img
    
def findROI(img, x, y, level=0.3):
    thresh = level*np.max(img)
    aboveThreshold = np.argwhere(img>thresh)
    xminIdx = np.min(aboveThreshold[:,0])
    xmaxIdx = np.max(aboveThreshold[:,0])
    yminIdx = np.min(aboveThreshold[:,1])
    ymaxIdx = np.max(aboveThreshold[:,1])
    xminIdx -=200
    xmaxIdx +=200
    yminIdx -=200
    ymaxIdx +=200
    return img[xminIdx:xmaxIdx, yminIdx:ymaxIdx], x[xminIdx:xmaxIdx], y[yminIdx:ymaxIdx]

def findROIProj(img, x, y, level=0.1):
    xProjection = np.sum(img, axis=1)
    yProjection = np.sum(img, axis=0)
    xthresh = level*np.max(xProjection)
    ythresh = level*np.max(yProjection)
    xAboveThreshold = np.argwhere(xProjection>xthresh)
    yAboveThreshold = np.argwhere(yProjection>ythresh)
    xminIdx = np.min(xAboveThreshold)-200
    xmaxIdx = np.max(xAboveThreshold)+200
    yminIdx = max([0,np.min(yAboveThreshold)-200])
    ymaxIdx = np.max(yAboveThreshold)+200
#    xminIdx = 0
#    xmaxIdx = -1
#    yminIdx = 0
#    ymaxIdx = -1
    
    print(xminIdx, xmaxIdx, yminIdx, ymaxIdx)
    
    return img[xminIdx:xmaxIdx, yminIdx:ymaxIdx], x[xminIdx:xmaxIdx], y[yminIdx:ymaxIdx]

def calcTimeFactors(fileName, vMin):
    f = h.File(fileName, 'r')
    e0 = f['Input/Energy'].value
    
    calibFactor1 = f['Meta_data/Calibration factor 1'].value
    calibFactor2 = f['Meta_data/Calibration factor 2'].value
    v = f['Raw_data/TDC voltage'].value
    calibFactor1 = calcFactor(calibFactor1, -v, vMin, e0)
    calibFactor2 = calcFactor(calibFactor2, v, vMin, e0)
    
    return calibFactor1, calibFactor2

def calcFactor(cInit, v, vMin, p):
    
    aI = np.arctan(vMin/p)
    aS = np.arctan(v/p)
    return np.abs(cInit*np.tan(aI+aS)/np.tan(aS))



if __name__ == "__main__":
#    plt.close("all")
    fileNames = np.array([   
            '/sf/data/measurements/2020/01/18/Bunch_length_meas_2020-01-18_15-00-05.h5',
                    '/sf/data/measurements/2020/01/18/Bunch_length_meas_2020-01-18_15-25-34.h5',
                    '/sf/data/measurements/2020/01/18/Bunch_length_meas_2020-01-18_15-18-44.h5',
                    '/sf/data/measurements/2020/01/18/Bunch_length_meas_2020-01-18_15-41-05.h5',
                    '/sf/data/measurements/2020/01/18/Bunch_length_meas_2020-01-18_15-44-46.h5',
                    '/sf/data/measurements/2020/01/18/Bunch_length_meas_2020-01-18_15-47-43.h5'
                    ])
    phases = np.array([73.5, 65.5, 65.5, 65.5, 65.5, 65.5])
    XBAmps = np.array([6.0, 4.5, 7.5, 10.5, 13.0, 14.5])
    vMins = np.array([-0.66, -0.75, -0.53, -0.42, -0.35, -0.26])
   
    
    nFinal = int(2e4)
    zeroCrossings = ["1", "2"]
    for zeroCrossing in zeroCrossings[0]:
        for fileName, phase, XBAmp, vMin in zip([fileNames[0]], [phases[0]], [XBAmps[0]], [vMins[0]]):
#            plt.close("all")
            zeroCrossingString = "" # " 2" for 2 and "" for 1
            if zeroCrossing == "2": zeroCrossingString = " 2"
            
            saveDir = "/afs/psi.ch/intranet/SF/Diagnostics/ACHIP/Data/20200118/BL/20200423/"+zeroCrossing+"/"
            if not os.path.exists(saveDir): 
                os.makedirs(saveDir)
            saveFileName = "LongDist"+str(phase)+"_"+str(XBAmp)
            
            img, t, e, x, y = load(fileName, vMin)
            img = subtractBackground(img)
            img = reduceSpots(img)
            img, t, e = findROI(img, t, e)
            #plt.figure()
            #plt.imshow(img, extent= [min(x), max(x), min(y), max(y)], aspect = "auto")
            #print(img.shape, x.shape)
            
            
            imgNorm = img/np.max(img)
            imgInterp = si.interp2d(t, e, imgNorm.T[:,::-1])
            
            def reduceParticles(tPart, ePart):
                rand = np.random.random(len(tPart))
            #    value = imgInterp(ePart, tPart)
                value = si.dfitpack.bispeu(imgInterp.tck[0], imgInterp.tck[1], imgInterp.tck[2], imgInterp.tck[3], imgInterp.tck[4], tPart, ePart)[0]
                keep = np.argwhere(rand<value).flatten()
                print(keep.shape)
            
                return tPart[keep], ePart[keep]
            
            tPart = np.array([])
            ePart = np.array([])
            nPartInitial = int(1e6)
            while len(tPart)<nFinal:
                
                tPartNew = np.random.random(nPartInitial)*(np.max(t)-np.min(t))+np.min(t)
                ePartNew = np.random.random(nPartInitial)*(np.max(e)-np.min(e))+np.min(e)
                tPartNew, ePartNew = reduceParticles(tPartNew, ePartNew)
                tPart = np.append(tPart, tPartNew)
                ePart = np.append(ePart, ePartNew)
                
                print(len(tPart))
            
            tPart = tPart[:nFinal]
            ePart = ePart[:nFinal]
            print(len(tPart))
            
            fig = plt.figure(figsize=(12,6))
            ax1 = fig.add_subplot(121)
            ax2 = fig.add_subplot(122)
            ax1.imshow(img.T, extent = [min(t), max(t), min(e), max(e)], aspect='auto', origin='lower')
            ax2.hist2d(tPart, ePart, bins=256)
            
            dist = {'t': tPart, 'e': ePart, 'vMin': vMin}
            
#            np.save(saveDir+saveFileName+".npy", dist)
#            fig.savefig(saveDir+saveFileName+".png")
