# -*- coding: utf-8 -*-
import numpy as np
import h5py as h
import matplotlib.pyplot as plt
import os
import scipy.interpolate as si
from scipy.ndimage.morphology import binary_erosion as be
from exportBL import load,subtractBackground, reduceSpots, findROI,findROIProj


#    plt.close("all")
fileNames = np.array([   
        'D:/Measurement Data/ATHOS/20201223/bl/Bunch_length_meas_2020-12-23_00-07-30.h5',
        'D:/Measurement Data/ATHOS/20201223/bl/Bunch_length_meas_2020-12-23_00-09-24.h5',
        'D:/Measurement Data/ATHOS/20201223/bl/Bunch_length_meas_2020-12-23_00-11-54.h5',       
        'D:/Measurement Data/ATHOS/20201223/bl/Bunch_length_meas_2020-12-23_00-13-56.h5',
        'D:/Measurement Data/ATHOS/20201223/bl/Bunch_length_meas_2020-12-23_00-15-44.h5',
        'D:/Measurement Data/ATHOS/20201223/bl/Bunch_length_meas_2020-12-23_00-17-37.h5',
        
        'D:/Measurement Data/ATHOS/20201223/bl/Bunch_length_meas_2020-12-22_23-39-35.h5', # injector measurement
        
        
        ])

phases = np.array([74.67,72.67,71.67,70.67,69.67,69.17, 0])
vMins = np.array([0,0,0,0,0,0,-0.32])
   

nFinal = int(2e5)
zeroCrossings = ["1", "2"]
for zeroCrossing in zeroCrossings:
    for fileName, phase, vMin in zip(fileNames[-1:], phases[-1:], vMins[-1:]):
#            plt.close("all")
        zeroCrossingString = "" # " 2" for 2 and "" for 1
        if zeroCrossing == "2": zeroCrossingString = " 2"
        
        saveDir ='D:/Measurement Data/ATHOS/20201223/bl/'+str(phase)+'/'+zeroCrossing+'/'
        if not os.path.exists(saveDir): 
            os.makedirs(saveDir)
        saveFileName = "LongDistAramis"+str(phase)
        
        img, t, e, x, y = load(fileName, vMin, zeroCrossingString)

        
        img = subtractBackground(img)
        img = reduceSpots(img)
        img, t, e = findROI(img, t, e, level=0.25)
        plt.figure()
        plt.imshow(img, extent= [min(e), max(e), min(t), max(t)], aspect = "auto")

        #print(img.shape, x.shape)
        
        
        imgNorm = img/np.max(img)
        imgInterp = si.interp2d(t, e, imgNorm.T[:,::-1])
        
        def reduceParticles(tPart, ePart):
            rand = np.random.random(len(tPart))
        #    value = imgInterp(ePart, tPart)
            value = si.dfitpack.bispeu(imgInterp.tck[0], imgInterp.tck[1], imgInterp.tck[2], imgInterp.tck[3], imgInterp.tck[4], tPart, ePart)[0]
            keep = np.argwhere(rand<value).flatten()
            print(keep.shape)
        
            return tPart[keep], ePart[keep]
        
        tPart = np.array([])
        ePart = np.array([])
        nPartInitial = int(2e6)
        while len(tPart)<nFinal:
            
            tPartNew = np.random.random(nPartInitial)*(np.max(t)-np.min(t))+np.min(t)
            ePartNew = np.random.random(nPartInitial)*(np.max(e)-np.min(e))+np.min(e)
            tPartNew, ePartNew = reduceParticles(tPartNew, ePartNew)
            tPart = np.append(tPart, tPartNew)
            ePart = np.append(ePart, ePartNew)
            
            print(len(tPart))
        
        tPart = tPart[:nFinal]
        ePart = ePart[:nFinal]

        
        fig = plt.figure(figsize=(14,6))
        ax1 = fig.add_subplot(141)
        ax2 = fig.add_subplot(142)
        ax3 = fig.add_subplot(143)
        ax4 = fig.add_subplot(144)
        
        ax1.imshow(img.T, extent = [min(t), max(t), min(e), max(e)], aspect='auto', origin='lower', cmap='hot')
        ax2.hist2d(tPart, ePart, bins=256, cmap='hot')
        ax2.set_xlim(ax1.get_xlim())
        ax2.set_ylim(ax1.get_ylim())
        ax2.set_facecolor('k')
        particleCurrent, bins = np.histogram(tPart, bins = 246 )
        particleCurrent = particleCurrent/np.max(particleCurrent)
        imgCurrent = np.sum(img, axis=1)
        imgCurrent = imgCurrent/np.max(imgCurrent)
        ax3.plot(bins[:-1],particleCurrent, label = 'Histogram')
        ax3.plot(t[::-1],imgCurrent, label = 'Image')
        
        particleSpectrum, bins = np.histogram(ePart, bins = 246 )
        particleSpectrum = particleSpectrum/np.max(particleSpectrum)
        imgSpectrum = np.sum(img, axis=0)
        imgSpectrum = imgSpectrum/np.max(imgSpectrum)
        ax4.plot(bins[:-1],particleSpectrum, label = 'Histogram')
        ax4.plot(e,imgSpectrum, label = 'Image')
        
        
        for ax in [ax1, ax2]:
            ax.set_ylabel(r'$\Delta E$ (MeV)')
        for ax in [ax1, ax2, ax3]:
            ax.set_xlabel(r'$t$ (fs)')
        ax4.set_xlabel(r'$\Delta E$ (MeV)')
        
        ax1.set_title('LPS Image')
        ax2.set_title('Particle Histogram')
        for ax in [ax3, ax4]:
            ax.legend()

        dist = {'t': tPart, 'e': ePart, 'vMin': vMin}
        fig.tight_layout()
        np.save(saveDir+saveFileName+".npy", dist)
        fig.savefig(saveDir+saveFileName+".png")
