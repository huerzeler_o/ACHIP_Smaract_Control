# -*- coding: utf-8 -*-

import numpy as np
import h5py as h
import matplotlib.pyplot as plt
import os
from exportBL import subtractBackground, reduceSpots

#plt.close("all")

import scipy.interpolate as si
from scipy.ndimage.morphology import binary_erosion as be

#fileName = '/sf/data/measurements/2020/01/18/Bunch_length_meas_2020-01-18_15-00-05.h5'; phase = 73.5; XBAmp = 6.0

fileName = '/sf/data/measurements/2020/01/18/Bunch_length_meas_2020-01-18_15-25-34.h5'; phase = 65.5; XBAmp = 4.5
fileName = '/sf/data/measurements/2020/01/18/Bunch_length_meas_2020-01-18_15-18-44.h5'; phase = 65.5; XBAmp = 7.5; vMin = -0.53
#fileName = '/sf/data/measurements/2020/01/18/Bunch_length_meas_2020-01-18_15-41-05.h5'; phase = 65.5; XBAmp = 10.5
#fileName = '/sf/data/measurements/2020/01/18/Bunch_length_meas_2020-01-18_15-44-46.h5'; phase = 65.5; XBAmp = 13.0
#fileName = '/sf/data/measurements/2020/01/18/Bunch_length_meas_2020-01-18_15-47-43.h5'; phase = 65.5; XBAmp = 14.5

charge = 10e-12

def calcTimeFactors(fileName, vMin):
    f = h.File(fileName, 'r')

    calibFactor = f['Meta_data/Calibration factor'].value
    dispersion = -f['Input/Dispersion'].value
#    print(dispersion)
#    print(calibFactor)
    e0 = f['Input/Energy'].value
    
    
#    images1 = np.array(f['Raw_data/Beam images'].value)
#    images2 = np.array(f['Raw_data/Beam images 2'].value)
#    image1 = images1[2,2]
#    image1 = subtractBackground(image1)
#    image1 = reduceSpots(image1)
#    image2 = images2[2,2]
#    image2 = subtractBackground(image2)
#    image2 = reduceSpots(image2)
#    x = np.array(f['Raw_data/xAxis'].value)
#    y = np.array(f['Raw_data/yAxis'].value)
    calibFactor1 = f['Meta_data/Calibration factor 1'].value
    calibFactor2 = f['Meta_data/Calibration factor 2'].value
    dispersion = -f['Input/Dispersion'].value
    v = f['Raw_data/TDC voltage'].value
    calibFactor1 = calcFactor(calibFactor1, -v, vMin, e0)
    calibFactor2 = calcFactor(calibFactor2, v, vMin, e0)
    
    return calibFactor1, calibFactor2

#    e = x*1e-6/dispersion*e0
#    t1 = y/(calibFactor1*2)
#    t2 = y/calibFactor2
#    
#    current1 = np.sum(image1, axis=1)
#    current1/= np.sum(current1*abs(t1[1]-t1[0]))
#    current1*=charge*1e15
#    
#    current2 = np.sum(image2, axis=1)
#    current2/= np.sum(current2*abs(t2[1]-t2[0]))
#    current2*=charge*1e15
#    plt.plot(t1,current1)
#    plt.plot(t2,current2)
#    return f

def calcFactor(cInit, v, vMin, p):
    
    aI = np.arctan(vMin/p)
    aS = np.arctan(v/p)
    return np.abs(cInit*np.tan(aI+aS)/np.tan(aS))

#f=calcTimeFactors(fileName, vMin)