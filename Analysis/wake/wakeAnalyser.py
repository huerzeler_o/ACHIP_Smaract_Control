# -*- coding: utf-8 -*-

import numpy as np
import h5py as h
import matplotlib.pyplot as plt
plt.close("all")

from dataCollection import dataSet20200118 as dataSet

dispersion = 0.318
energy = 3e9

def loadStack(dataDict):
    fileName = dataDict["file"]
    path = "/sf/data/measurements/2019/12/17/" 
    if "20191218" in fileName:
        path = "/sf/data/measurements/2019/12/18/"
    if "20200118" in fileName:
        path = "/sf/data/measurements/2020/01/18/"
    f = h.File(path+fileName, "r")
    imgStack = np.array(f["camera1/image"])
    x = np.array(f["camera1/x_axis"])
    y = np.array(f["camera1/y_axis"])
    return  imgStack, x[0], y[0]

def filterStack(imgStack):
    flatStack = imgStack.reshape(imgStack.shape[0], -1)
    maxVals = np.max(flatStack, axis = 1)
    return imgStack[np.where(maxVals>1000)]

def plotSpectraAligned(dataDict, ax):

    imgStack, x, y = loadStack(dataDict)
    imgStackOn = filterStack(imgStack)
    de = x*1e-6/dispersion*energy*1e-6
    
    projections = 1e-3*np.sum(imgStackOn, axis=1)
    
    for p in projections:
        threshold = 0.5*max(p)
        offset = de[np.argwhere(p>threshold).flatten()[-1]]
        ax.plot(de-offset, p, color= "k", alpha= 0.2, lw=0.5)
    
    ax.set_xlabel(r"$E - E_0$ (MeV)")
    ax.set_ylabel("Density (a.u.)")

def plotSpectra(dataDict, ax):

    imgStack, x, y = loadStack(dataDict)
    imgStackOn = filterStack(imgStack)
    de = x*1e-6/dispersion*energy*1e-6
    ax.plot(de, 1e-3*np.sum(imgStackOn, axis=1).T, color= "k", alpha= 0.2, lw=0.5)
    
    ax.set_xlabel(r"$E - E_0$ (MeV)")
    ax.set_ylabel("Density (a.u.)")


rfStepDataSet = []
for d in dataSet:
    if d["category"] == "rf step scan":
        rfStepDataSet.append(d)

#for i in range(4):
#    fig = plt.figure(figsize = (12,6))
#    ax1 = fig.add_subplot(121)
#    ax2 = fig.add_subplot(122)
#    
#    d = rfStepDataSet[i*2]
#    plotSpectra(d, ax1)
#    ax1.set_title("Structure Out, XB-Phase-Step: " + str(d["rfPhaseStep"]))
#    
#    d = rfStepDataSet[i*2+1]
#    plotSpectra(d, ax2)
#    ax2.set_title("Structure In, XB-Phase-Step: " + str(d["rfPhaseStep"]))
#    
#    ax2.set_xlim(ax1.get_xlim())
#    ax2.set_ylim(ax1.get_ylim())
#    fig.tight_layout()
#    fig.savefig(str(i)+".png", dpi=600)

if __name__ == "__main__":   
#    chargeDataSet = []
#    for d in dataSet:
#        if d["category"] == "charge scan":
#            chargeDataSet.append(d)
#    
#    for i in range(4):
#        fig = plt.figure(figsize = (12,6))
#        ax1 = fig.add_subplot(121)
#        ax2 = fig.add_subplot(122)
#
#        d = chargeDataSet[i*2+1]
#        plotSpectra(d, ax1)
#        ax1.set_title("Structure Out, Charge: " + str(d["q"]) + " pC")
#        
#        d = chargeDataSet[i*2]
#        plotSpectra(d, ax2)
#        ax2.set_title("Structure In, Charge: " + str(d["q"]) + " pC")
#        
#
#        
#        ax2.set_xlim(ax1.get_xlim())
#        ax2.set_ylim(ax1.get_ylim())
#        fig.tight_layout()
#        fig.savefig("figures/"+str(d["q"])+"pC.png", dpi=600)
    
    selectedDataSet = []
    for d in dataSet:
        if d["category"] == "L1 phase scan":
            selectedDataSet.append(d)
    print(selectedDataSet)
    for i in range(-1,0):
        fig = plt.figure(figsize = (12,6))
        ax1 = fig.add_subplot(121)
        ax2 = fig.add_subplot(122)
    
        d = selectedDataSet[i*2+1]
        plotSpectra(d, ax1)
#        ax1.set_title("Structure Out, L1-Phase: " + str(d["l1Pha"]) + " deg")
        ax1.set_title("Structure Out, XB Amplitude: " + str(d["xbAmp"]) + " MV")
        d = selectedDataSet[i*2]
        plotSpectra(d, ax2)
#        ax2.set_title("Structure In, L1-Phase: " + str(d["l1Pha"]) + " deg")
        ax1.set_title("Structure In, XB Amplitude: " + str(d["xbAmp"]) + " MV")
        print(d['l1Pha'])
    
        
        ax1.set_xlim(ax2.get_xlim())
#        ax1.set_ylim(ax2.get_ylim())
        fig.tight_layout()
        
        fig.savefig("/afs/psi.ch/intranet/SF/Diagnostics/ACHIP/Data/20200118/Spectrum/"+'measurementSpectrum'+'65.5_'+str(d['xbAmp'])+".png", dpi=300)