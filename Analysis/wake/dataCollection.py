dataSet20191218 = [ 
            {
                "file" : "20191218_020549_SATCL01-DSCR150_camera_stack.h5",
                "rfPhaseStep" : 0,
                "q" : 0.8,
                "iris" : 0.15,
                "laserEnergy" : 16.0,
                "structure" : True,
                "x" : -350,
                "category" : "charge scan"
                }, 
            {
                "file" : "20191218_020919_SATCL01-DSCR150_camera_stack.h5",
                "rfPhaseStep" : 0,
                "q" : 0.8,
                "iris" : 0.15,
                "laserEnergy" : 16.0,
                "structure" : False,
                "x" : -350,
                "category" : "charge scan"
                }, 
            {
                "file" : "20191218_022904_SATCL01-DSCR150_camera_stack.h5",
                "rfPhaseStep" : 0,
                "q" : 6.7,
                "iris" : 0.4,
                "laserEnergy" : 16.0,
                "structure" : True,
                "x" : -350,
                "category" : "charge scan"
                }, 
            {
                "file" : "20191218_023109_SATCL01-DSCR150_camera_stack.h5",
                "rfPhaseStep" : 0,
                "q" : 6.7,
                "iris" : 0.4,
                "laserEnergy" : 16.0,
                "structure" : False,
                "x" : -350,
                "category" : "charge scan"
                }, 
            {
                "file" : "20191218_031747_SATCL01-DSCR150_camera_stack.h5",
                "rfPhaseStep" : 0,
                "q" : 9.8,
                "iris" : 0.5,
                "laserEnergy" : 16.0,
                "structure" : True,
                "x" : -350,
                "category" : "charge scan"
                }, 
            {
                "file" : "20191218_032006_SATCL01-DSCR150_camera_stack.h5",
                "rfPhaseStep" : 0,
                "q" : 9.8,
                "iris" : 0.5,
                "laserEnergy" : 11.3,
                "structure" : False,
                "x" : -350,
                "category" : "charge scan"
                }, 
            {
                "file" : "20191218_042739_SATCL01-DSCR150_camera_stack.h5",
                "rfPhaseStep" : 0,
                "q" : 3.9,
                "iris" : 0.4,
                "laserEnergy" : 11.3,
                "structure" : True,
                "x" : -350,
                "category" : "charge scan"
                }, 
            {
                "file" : "20191218_043010_SATCL01-DSCR150_camera_stack.h5",
                "rfPhaseStep" : 0,
                "q" : 3.9,
                "iris" : 0.4,
                "laserEnergy" : 11.3,
                "structure" : False,
                "x" : -350,
                "category" : "charge scan"
                },         
        
        
            {
                "file" : "20191218_065259_SATCL01-DSCR150_camera_stack.h5",
                "rfPhaseStep" : -2.5,
                "q" : 6.7,
                "iris" : 0.4,
                "laserEnergy" : 16.0,
                "structure" : False,
                "x" : -350,
                "category" : "rf step scan"
                }, 
            {
                "file" : "20191218_065533_SATCL01-DSCR150_camera_stack.h5",
                "rfPhaseStep" : -2.5,
                "q" : 6.7,
                "iris" : 0.4,
                "laserEnergy" : 16.0,
                "structure" : True,
                "x" : -350,
                "category" : "rf step scan"
                }, 
            {
                "file" : "20191218_023109_SATCL01-DSCR150_camera_stack.h5",
                "rfPhaseStep" : 0,
                "q" : 6.7,
                "iris" : 0.4,
                "laserEnergy" : 16.0,
                "structure" : False,
                "x" : -350,
                "category" : "rf step scan"
                }, 
            {
                "file" : "20191218_022904_SATCL01-DSCR150_camera_stack.h5",
                "rfPhaseStep" : 0,
                "q" : 6.7,
                "iris" : 0.4,
                "laserEnergy" : 16.0,
                "structure" : True,
                "x" : -350,
                "category" : "rf step scan"
                },                     
            {
                "file" : "20191218_063808_SATCL01-DSCR150_camera_stack.h5",
                "rfPhaseStep" : 2.5,
                "q" : 6.7,
                "iris" : 0.4,
                "laserEnergy" : 16.0,
                "structure" : False,
                "x" : -350,
                "category" : "rf step scan"
                }, 
            {
                "file" : "20191218_064007_SATCL01-DSCR150_camera_stack.h5",
                "rfPhaseStep" : 2.5,
                "q" : 6.7,
                "iris" : 0.4,
                "laserEnergy" : 16.0,
                "structure" : True,
                "x" : -350,
                "category" : "rf step scan"
                }, 
            {
                "file" : "20191218_062959_SATCL01-DSCR150_camera_stack.h5",
                "rfPhaseStep" : 5,
                "q" : 6.7,
                "iris" : 0.4,
                "laserEnergy" : 16.0,
                "structure" : False,
                "x" : -350,
                "category" : "rf step scan"
                }, 
            {
                "file" : "20191218_063202_SATCL01-DSCR150_camera_stack.h5",
                "rfPhaseStep" : 5,
                "q" : 6.7,
                "iris" : 0.4,
                "laserEnergy" : 16.0,
                "structure" : True,
                "x" : -350,
                "category" : "rf step scan"
                },
            {   
                "file" : "20191218_062400_SATCL01-DSCR150_camera_stack.h5",
                "rfPhaseStep" : 10,
                "q" : 6.7,
                "iris" : 0.4,
                "laserEnergy" : 16.0,
                "structure" : False,
                "x" : -350,
                "category" : "rf step scan"
                }, 
             {
                "file" : "20191218_062655_SATCL01-DSCR150_camera_stack.h5",
                "rfPhaseStep" : 10,
                "q" : 6.7,
                "iris" : 0.4,
                "laserEnergy" : 16.0,
                "structure" : True,
                "x" : -350,
                "category" : "rf step scan"
                }
   
             
             
        ]

"""
+10 deg without structure: 20191218_062400_SATCL01-DSCR150_camera_stack.h5
+10 deg with structure: 20191218_062655_SATCL01-DSCR150_camera_stack.h5
+5 deg without structure: 20191218_062959_SATCL01-DSCR150_camera_stack.h5
+5 deg with structure: 20191218_063202_SATCL01-DSCR150_camera_stack.h5
+15 deg with structure: 20191218_063356_SATCL01-DSCR150_camera_stack.h5
+15 deg without structure: 20191218_063610_SATCL01-DSCR150_camera_stack.h5
+2.5 deg without structure: 20191218_063808_SATCL01-DSCR150_camera_stack.h5
+2.5 deg with structure: 20191218_064007_SATCL01-DSCR150_camera_stack.h5
+7.5 deg with structure: 20191218_064201_SATCL01-DSCR150_camera_stack.h5
+7.5 deg without structure: 20191218_064401_SATCL01-DSCR150_camera_stack.h5
+12.5 deg without structure: 20191218_064554_SATCL01-DSCR150_camera_stack.h5
+12.5 deg with structure: 20191218_064849_SATCL01-DSCR150_camera_stack.h5
-2.5 deg without structure: 20191218_065259_SATCL01-DSCR150_camera_stack.h5
-2.5 deg with structure: 20191218_065533_SATCL01-DSCR150_camera_stack.h5
"""

dataSet20200118 = [ 
            {
                "file" : "20200118_123630_SATCL01-DSCR150_camera_stack.h5",
                "xbAmp" : 13.07,
                "q" : 9.5,
                "structure" : True,
                "category" : "original"
                }, 
            {
                "file" : "20200118_123813_SATCL01-DSCR150_camera_stack.h5",
                "xbAmp" : 13.07,
                "q" : 9.5,
                "structure" : False,
                "category" : "original"
                }, 
            
            {
                "file" : "20200118_131922_SATCL01-DSCR150_camera_stack.h5",
                "xbAmp" : 2.0,
                "l1Pha" : 65.5,
                "q" : 9.5,
                "structure" : True,
                "category" : "XB scan"
                }, 
            {
                "file" : "20200118_132128_SATCL01-DSCR150_camera_stack.h5",
                "xbAmp" : 2.0,
                "l1Pha" : 65.5,
                "q" : 9.5,
                "structure" : False,
                "category" : "XB scan"
                },    

            {
                "file" : "20200118_133137_SATCL01-DSCR150_camera_stack.h5",
                "xbAmp" : 4.5,
                "l1Pha" : 65.5,
                "q" : 9.5,
                "structure" : True,
                "category" : "XB scan"
                }, 
            {
                "file" : "20200118_133242_SATCL01-DSCR150_camera_stack.h5",
                "xbAmp" : 4.5,
                "l1Pha" : 65.5,
                "q" : 9.5,
                "structure" : False,
                "category" : "XB scan"
                },  

            {
                "file" : "20200118_133541_SATCL01-DSCR150_camera_stack.h5",
                "xbAmp" : 6.0,
                "l1Pha" : 65.5,
                "q" : 9.5,
                "structure" : True,
                "category" : "XB scan"
                }, 
            {
                "file" : "20200118_133700_SATCL01-DSCR150_camera_stack.h5",
                "xbAmp" : 6.0,
                "l1Pha" : 65.5,
                "q" : 9.5,
                "structure" : False,
                "category" : "XB scan"
                },               

            {
                "file" : "20200118_133945_SATCL01-DSCR150_camera_stack.h5",
                "xbAmp" : 7.5,
                "l1Pha" : 65.5,
                "q" : 9.5,
                "structure" : True,
                "category" : "XB scan"
                }, 
            {
                "file" : "20200118_134048_SATCL01-DSCR150_camera_stack.h5",
                "xbAmp" : 7.5,
                "l1Pha" : 65.5,
                "q" : 9.5,
                "structure" : False,
                "category" : "XB scan"
                },   
                    
            {
                "file" : "20200118_134414_SATCL01-DSCR150_camera_stack.h5",
                "xbAmp" : 9.0,
                "l1Pha" : 65.5,
                "q" : 9.5,
                "structure" : True,
                "category" : "XB scan"
                }, 
            {
                "file" : "20200118_134524_SATCL01-DSCR150_camera_stack.h5",
                "xbAmp" : 9.0,
                "l1Pha" : 65.5,
                "q" : 9.5,
                "structure" : False,
                "category" : "XB scan"
                },   

            {
                "file" : "20200118_134825_SATCL01-DSCR150_camera_stack.h5",
                "xbAmp" : 10.5,
                "l1Pha" : 65.5,
                "q" : 9.5,
                "structure" : True,
                "category" : "XB scan"
                }, 
            {
                "file" : "20200118_135003_SATCL01-DSCR150_camera_stack.h5",
                "xbAmp" : 10.5,
                "l1Pha" : 65.5,
                "q" : 9.5,
                "structure" : False,
                "category" : "XB scan"
                },   
                    
            {
                "file" : "20200118_135409_SATCL01-DSCR150_camera_stack.h5",
                "xbAmp" : 13.0,
                "l1Pha" : 65.5,
                "q" : 9.5,
                "structure" : True,
                "category" : "XB scan"
                }, 
            {
                "file" : "20200118_135531_SATCL01-DSCR150_camera_stack.h5",
                "xbAmp" : 13.0,
                "l1Pha" : 65.5,
                "q" : 9.5,
                "structure" : False,
                "category" : "XB scan"
                },  
             
            {
                "file" : "20200118_135933_SATCL01-DSCR150_camera_stack.h5",
                "xbAmp" : 14.5,
                "l1Pha" : 65.5,
                "q" : 9.5,
                "structure" : True,
                "category" : "XB scan"
                }, 
            {
                "file" : "20200118_140118_SATCL01-DSCR150_camera_stack.h5",
                "xbAmp" : 14.5,
                "l1Pha" : 65.5,
                "q" : 9.5,
                "structure" : False,
                "category" : "XB scan"
                },   
                    
            {
                "file" : "20200118_140312_SATCL01-DSCR150_camera_stack.h5",
                "xbAmp" : 16.0,
                "l1Pha" : 65.5,
                "q" : 9.5,
                "structure" : True,
                "category" : "XB scan"
                }, 
            {
                "file" : "20200118_140425_SATCL01-DSCR150_camera_stack.h5",
                "xbAmp" : 16.0,
                "l1Pha" : 65.5,
                "q" : 9.5,
                "structure" : False,
                "category" : "XB scan"
                },                      

            {
                "file" : "20200118_141056_SATCL01-DSCR150_camera_stack.h5",
                "xbAmp" : 7.5,
                "l1Pha" : 64.5,
                "q" : 9.5,
                "structure" : True,
                "category" : "L1 phase scan"
                }, 
            {
                "file" : "20200118_141231_SATCL01-DSCR150_camera_stack.h5",
                "xbAmp" : 7.5,
                "l1Pha" : 64.5,
                "q" : 9.5,
                "structure" : False,
                "category" : "L1 phase scan"
                },   
            
            {
                "file" : "20200118_133945_SATCL01-DSCR150_camera_stack.h5",
                "xbAmp" : 7.5,
                "l1Pha" : 65.5,
                "q" : 9.5,
                "structure" : True,
                "category" : "L1 phase scan"
                }, 
            {
                "file" : "20200118_134048_SATCL01-DSCR150_camera_stack.h5",
                "xbAmp" : 7.5,
                "l1Pha" : 65.5,
                "q" : 9.5,
                "structure" : False,
                "category" : "L1 phase scan"
                },                      
          
            {
                "file" : "20200118_141522_SATCL01-DSCR150_camera_stack.h5",
                "xbAmp" : 7.5,
                "l1Pha" : 66.5,
                "q" : 9.5,
                "structure" : True,
                "category" : "L1 phase scan"
                }, 
            {
                "file" : "20200118_141627_SATCL01-DSCR150_camera_stack.h5",
                "xbAmp" : 7.5,
                "l1Pha" : 66.5,
                "q" : 9.5,
                "structure" : False,
                "category" : "L1 phase scan"
                },   
            
            {
                "file" : "20200118_141933_SATCL01-DSCR150_camera_stack.h5",
                "xbAmp" : 7.5,
                "l1Pha" : 67.5,
                "q" : 9.5,
                "structure" : True,
                "category" : "L1 phase scan"
                }, 
            {
                "file" : "20200118_142106_SATCL01-DSCR150_camera_stack.h5",
                "xbAmp" : 7.5,
                "l1Pha" : 67.5,
                "q" : 9.5,
                "structure" : False,
                "category" : "L1 phase scan"
                },   
                    
            {
                "file" : "20200118_142602_SATCL01-DSCR150_camera_stack.h5",
                "xbAmp" : 7.5,
                "l1Pha" : 73.5,
                "q" : 9.5,
                "structure" : True,
                "category" : "L1 phase scan"
                }, 
            {
                "file" : "20200118_142409_SATCL01-DSCR150_camera_stack.h5",
                "xbAmp" : 7.5,
                "l1Pha" : 73.5,
                "q" : 9.5,
                "structure" : False,
                "category" : "L1 phase scan"
                },   

            {
                "file" : "20200118_143721_SATCL01-DSCR150_camera_stack.h5",
                "xbAmp" : 6.0,
                "l1Pha" : 73.5,
                "q" : 9.5,
                "structure" : True,
                "category" : "L1 phase scan"
                }, 
            {
                "file" : "20200118_143832_SATCL01-DSCR150_camera_stack.h5",
                "xbAmp" : 6.0,
                "l1Pha" : 73.5,
                "q" : 9.5,
                "structure" : False,
                "category" : "L1 phase scan"
                }, 

        ]

