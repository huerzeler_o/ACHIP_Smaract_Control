# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import h5py as h
from exportBL import subtractBackground, reduceSpots, findROI

def load(fileName, zeroCrossing = 1, idx1=0, idx2=0):
    f = h.File(fileName, 'r')
    zeroCrossingString = ""
    if zeroCrossing == 2: zeroCrossingString = " 2"
    images = np.array(f['Raw_data/Beam images'+zeroCrossingString].value)
    image = images[idx1,idx2]
    x = np.array(f['Raw_data/xAxis'].value)
    y = np.array(f['Raw_data/yAxis'].value)
    calibFactor1 = f['Meta_data/Calibration factor 1'].value
    calibFactor2 = f['Meta_data/Calibration factor 2'].value
    dispersion = -f['Input/Dispersion'].value
    e0 = f['Input/Energy'].value
#    print(e0)
    e = x*1e-6/dispersion*e0
    t1 = y/calibFactor1
    t2 = y/calibFactor2
    t=t1
    if zeroCrossing == "2":
        t = t2
    return image, t,e, x,y


fileNames = np.array([   
            '/sf/data/measurements/2020/01/18/Bunch_length_meas_2020-01-18_15-00-05.h5',
                    '/sf/data/measurements/2020/01/18/Bunch_length_meas_2020-01-18_15-25-34.h5',
                    '/sf/data/measurements/2020/01/18/Bunch_length_meas_2020-01-18_15-18-44.h5',
                    '/sf/data/measurements/2020/01/18/Bunch_length_meas_2020-01-18_15-41-05.h5',
                    '/sf/data/measurements/2020/01/18/Bunch_length_meas_2020-01-18_15-44-46.h5',
                    '/sf/data/measurements/2020/01/18/Bunch_length_meas_2020-01-18_15-47-43.h5'
                    ])
phases = np.array([73.5, 65.5, 65.5, 65.5, 65.5, 65.5])
XBAmps = np.array([6.0, 4.5, 7.5, 10.5, 13.0, 14.5])
vMins = np.array([-0.66, -0.75, -0.53, -0.42, -0.35, -0.26])
   

def findCOM(img, t, e):
    projT = np.sum(img, axis = 1)
    projE = np.sum(img, axis = 0)
    comT = np.sum(t*projT)/np.sum(projT)
    comE = np.sum(e*projE)/np.sum(projE)
    return comT, comE

def findSlice(img, t, e):
    comSliceT = np.sum(img*np.repeat([e], len(t), axis=0), axis=1)/np.sum(img, axis=1)
    eForStd = np.repeat([e], len(t), axis=0)-np.repeat([comSliceT], len(e), axis = 0).T
    stdSliceT = np.sqrt(np.sum(img*(eForStd)**2, axis = 1)) / np.sqrt(np.sum(img, axis=1))
    plt.plot(t, stdSliceT)

for i in range(1,6):
    plt.figure()
    for idx1 in range(5):
        img, t, e, x, y = load(fileNames[i], zeroCrossing=1, idx2= idx1)
        img = subtractBackground(img)
        img = reduceSpots(img)
    #    img, t, e = findROI(img, t, e)
        findSlice(img, t, e)
        
       
    