# -*- coding: utf-8 -*-

import numpy as np
import h5py as h
import os
import matplotlib.pyplot as plt
import shutil


fileNames = np.array([   
        'D:/Measurement Data/ATHOS/20201223/bl/Bunch_length_meas_2020-12-23_00-07-30.h5',
        'D:/Measurement Data/ATHOS/20201223/bl/Bunch_length_meas_2020-12-23_00-09-24.h5',
        'D:/Measurement Data/ATHOS/20201223/bl/Bunch_length_meas_2020-12-23_00-11-54.h5',       
        'D:/Measurement Data/ATHOS/20201223/bl/Bunch_length_meas_2020-12-23_00-13-56.h5',
        'D:/Measurement Data/ATHOS/20201223/bl/Bunch_length_meas_2020-12-23_00-15-44.h5',
        'D:/Measurement Data/ATHOS/20201223/bl/Bunch_length_meas_2020-12-23_00-17-37.h5',
                ])

phases = np.array([74.67,72.67,71.67,70.67,69.67,69.17])
vMins = np.array([0,0,0,0,0,0])
   
m0 = 0.511 # rest mass electron
c = 299792458
charge = 10e-12
zeroCrossings = ["1", "2"]
for zeroCrossing in zeroCrossings:
    for fileName, phase, vMin in zip(fileNames, phases, vMins):
#            plt.close("all")
        zeroCrossingString = "" # " 2" for 2 and "" for 1
        if zeroCrossing == "2": zeroCrossingString = " 2"
        
        directory ='D:/Measurement Data/ATHOS/20201223/bl/'+str(phase)+'/'+zeroCrossing+'/'
        distFileName = "LongDistAramis"+str(phase)
        saveDir = 'C:/Users/Benedikt Hermann/Dropbox/ACHIP Wake 20201223/LPS/L1Phase'+str(phase)+'/'+zeroCrossing+'/'
        if os.path.exists(saveDir): 
            shutil.rmtree(saveDir)
        os.makedirs(saveDir)
        
        d = np.load(directory+distFileName+'.npy')[()]
        
        g = -d['e']/m0
        t = d['t']
#        if (zeroCrossing == '2' and phase!=75.5) or (zeroCrossing == '1' and phase==75.5):
#            t=-t
        if zeroCrossing == '1':
            t=-t       
            
        t-=np.average(t)
        g-=np.average(g)
        
        e0 = 3.2e3
        g0 = e0/m0
        
        R56 = 3.17e-3
#        R56 = 0
        
        dt = g/g0*R56/c*1e15
        tNew = t+dt
        
        current, bins = np.histogram(tNew, bins=512)
        current = current/np.sum(current)*charge/(bins[1]-bins[0])*1e15
        tCurrent = bins[:-1]
        tNew-=tCurrent[0]
        tCurrent-=tCurrent[0]
        
        spectrum, bins = np.histogram(g, bins=512)
        gSpectrum = bins[:-1]
        
        fig = plt.figure(figsize=(12,6))
        ax1 = fig.add_subplot(131)
        ax2 = fig.add_subplot(132)
        ax3 = fig.add_subplot(133)

        ax1.hist2d(tNew, g, bins=512, cmap='hot')
        ax2.plot(tCurrent, current)
        ax3.plot(gSpectrum, spectrum)


        ax1.set_ylabel(r'$\Delta \gamma$')
        for ax in [ax1, ax2]:
            ax.set_xlabel(r'$t$ (fs)')
        ax3.set_xlabel(r'$\Delta \gamma$')
        
        ax2.set_ylabel(r'$I$ (A)')
        for ax in [ax2,ax3]:
            ax.set_ylim(0)
            ax.grid()
        ax2.set_xlim(0)
        ax2.set_title('L1 Phase = '+str(phase)+' deg')
        fig.tight_layout()
        

        fig.savefig(saveDir+'LPS.png', dpi=300)
        
        f = h.File(saveDir+'LPS.h5', 'a')
        f.create_dataset('deltaGamma', data = g)
        f.create_dataset('t', data = tNew)
        f.close()
        file = open(saveDir+'currentCST.txt', 'w')
        for i in range(len(tCurrent)):
            file.write(str(tCurrent[i])+', '+str(current[i])+'\n')
        file.close()
        
        