# -*- coding: utf-8 -*-
import numpy as np
import h5py as h
import matplotlib.pyplot as plt
import os
import scipy.interpolate as si
from scipy.ndimage.morphology import binary_erosion as be
from exportBL import load,subtractBackground, reduceSpots, findROI

path = '/afs/psi.ch/intranet/SF/Diagnostics/ACHIP/Data/20210222/'
pathSave = path

fileNames = [
'dispCalibration2_20210222_071229.h5',
'W8G10pC_20210222_061342.h5',
'W8G10pC_20210222_061601.h5',
'W8G10pC_20210222_061815.h5',
'W8G10pC_20210222_062128.h5',
'W8G10pC_20210222_062341.h5',
'W8G10pC_20210222_062550.h5',
'W8G10pC_20210222_062805.h5',
'W8G10pC_20210222_063020.h5',
'W8G10pC_20210222_063309.h5',
'W8G10pC_bkg_L1Phase69.7_20210222_070346.h5',
'W8G10pC_bkg_L1Phase70.7_20210222_070316.h5',
'W8G10pC_bkg_L1Phase71.7_20210222_070245.h5',
'W8G10pC_bkg_L1Phase72.7_20210222_070213.h5',
'W8G10pC_bkg_L1Phase73.7_20210222_070138.h5',
'W8G10pC_VerticalScan_20210222_064049.h5',
'W8G10pC_VerticalScan_20210222_064307.h5',
'W8G10pC_VerticalScan_20210222_064503.h5',
'W8G10pC_VerticalScan_S10Phase70.7_20210222_065155.h5',
'W8G10pC_VerticalScan_S10Phase71.7_20210222_065355.h5',
'W8G10pC_VerticalScan_S10Phase72.7_20210222_065544.h5',
'W8G10pC_VerticalScan_S10Phase73.7_20210222_065733.h5',
]

for fileName in fileNames:
    print(fileName)

    f = h.File(path+fileName, 'r')
    if os.path.isfile(pathSave+'ROI_'+fileName):
        os.remove(pathSave+'ROI_'+fileName)
    fd = h.File(pathSave+'ROI_'+fileName, 'a')

    for k in list(f):
        if 'SensorValue' == k:
            fd.create_group('SensorValue')
            for k in list(f['SensorValue']):
                if 'FPIC' not in k:
                    f.copy('SensorValue/'+k, fd['SensorValue'])
        else:
            f.copy(k, fd)
    width = int(f['SensorValue/SATCL01-DSCR150:WIDTH'].value)
    height = int(f['SensorValue/SATCL01-DSCR150:HEIGHT'].value)
    imgs = f['SensorValue/SATCL01-DSCR150:FPICTURE'].value.reshape((-1,height,width))
    imgs = imgs[:,925:1250, 400:2400]
#    img = imgs[40]
#    plt.imshow(img)
    
    fd.create_dataset('SensorValue/SATCL01-DSCR150:ROIPICTURE', data=imgs)
    
    f.close()
    fd.close()