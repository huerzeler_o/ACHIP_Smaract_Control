# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import h5py as h5
import fitFunctions as ff
from skimage.transform import radon
import scipy.constants as sc
from wireScanToGaussian import *
from tqdm import tqdm
from scipy.ndimage.filters import gaussian_filter

def generateMatrix(fnameOut, nBinsRecGrid, edgeRecGrid, dMat, angles):
    """
    Generate Transform Matrix for tomographic reconstruction and writes it to h5 file in the OriginalMatrix group

    Parameters
    ----------
    fnameOut    :   str
                    filename of generated matrix
    nBinsRecGrid:   int
                    number of Pixels along x-Axis of reconstruction grid, grid has to be square
    edgeRecGrid:    float
                    x and y axis of reconstruction grid will range from -edgeRecGrid to +edgeRecGrid
    dMat        :   array
                    wire displacement positions  for measurement supported by matrix
    angles      :   array
                    angles of wires appearing in measured data on which the tomographic reconstruction is applied
    Returns
    -------
    Matrix      :   array
                    transform matrix
    dMat        :   array
                    wire displacements used by matrix
    xMat        :   array
                    x axis of square grid used for reconstruction
    nBinsRecGrid:   int
                    number of pixels along x axis of reconstruction grid
    angles      :   array
                    Array of angles visited
    """
    #make sure there are only unique angles used
    angles = np.unique(angles)
    #get number of wires
    nWires = len(angles)
    #set up pre computed meshgrid fit function for faster computation
    pcM = ff.wireScannerModel(edgeRecGrid, nBinsRecGrid)
    #generate matrices
    A = np.zeros((nWires,nBinsRecGrid**2,len(dMat)))
    #generate points where transform will be evaluated
    anglesToEval = np.tile(angles, pcM.nPoints).reshape((-1,1))
    pointsToEval = np.concatenate((np.repeat(dMat, nWires).reshape((-1,1)),anglesToEval), axis = 1)
    #generate empty array to use as image
    evalArray = np.zeros((nBinsRecGrid, nBinsRecGrid))
    #iterate over all elements in image
    for j in tqdm(range(nBinsRecGrid)):
        for k in range(nBinsRecGrid):
            #set one pixel to one
            evalArray[j,k] = float(pcM.circMask[j,k])
            #evaluate radon transform
            rd = pcM.wireScannerBasic(pointsToEval, 0,0,0,0,0,0,0, profile = evalArray)
            #bring in right shape
            rd = np.reshape(rd, (-1,nWires)).T
            #write into transform array
            A[:,j*nBinsRecGrid+k,:] =  rd
            #reset input image
            evalArray[j,k] = 0
    with h5.File(fnameOut, 'w') as hf:
        org = hf.create_group("OriginalMatrix")
        org.create_dataset("TransformMatrix"+str(nWires)+"Wires",  data=A, compression = "gzip")
        org.create_dataset("xMat", data = pcM.x)
        org.create_dataset("dMat", data = dMat)
        org.create_dataset("angles", data = angles)
        org.create_dataset("nBinsRecGrid", data = [nBinsRecGrid])
    return A, dMat, pcM.x, nBinsRecGrid, angles

def readMatrix(fname, groupName):
    """
    Read Transform Matrix from h5 file

    Parameters
    ----------
    fname       :   str
                    filename of matrix
    groupName   :   str
                    name of group to be read
    Returns
    -------
    Matrix      :   array
                    transform matrix
    dMat        :   array
                    wire displacements used by matrix
    xMat        :   array
                    x axis of square grid used for reconstruction
    nBinsRecGrid:   int
                    number of pixels along x axis of reconstruction grid
    angles      :   array
                    Array of angles visited
    
    """
    with h5.File(fname, 'r') as hf:
        A = hf[groupName]["TransformMatrix9Wires"][:]
        dMat = hf[groupName]["dMat"][:]
        nBinsRecGrid = hf[groupName]["nBinsRecGrid"][0]
        xMat = hf[groupName]["xMat"][:]
        angles = hf[groupName]["angles"][:]
#        A = hf["TransformMatrix9Wires"][:]
#        dMat = hf["dMat"][:]
#        nBinsRecGrid = hf["nBinsRecGrid"][0]
#        xMat = hf["xMat"][:]
#        angles = hf["angles"][:]
    return A, dMat, xMat, nBinsRecGrid, angles

def interpolateMatrix(fname, dMea, groupName, forceNew = False):
    """
    Read Transform Matrix from h5 file, interpolate it and write it back as a new group

    Parameters
    ----------
    fname       :   str
                    filename of matrix
    groupName   :   str
                    name of group to be read
    Returns
    -------
    Matrix      :   array
                    transform matrix
    dMat        :   array
                    wire displacements used by matrix
    xMat        :   array
                    x axis of square grid used for reconstruction
    nBinsRecGrid:   int
                    number of pixels along x axis of reconstruction grid
    angles      :   array
                    Array of angles visited
    forceNew    :   boolean, optional
                    force regeneration of interpolated matrix
    """
    Matrix, dMat, xMat, nBinsRecGrid, angles = readMatrix(fname, "OriginalMatrix")
    #check if Matrix is usable
    if np.min(dMat)>np.min(dMea) or np.max(dMat)<np.max(dMea):
        print("Matrix can not be interpolated for given data")
    #check if desired name is already used, if so, load this data
    with h5.File(fname, 'a') as hf:
        if groupName in hf.keys() and forceNew is False:
            print("Desired groupname is already existing, loading data")
            return readMatrix(fname, groupName)
        else:
            nWires = len(np.unique(angles))
            #interpolate matrix
            MatrixInterp = np.zeros((Matrix.shape[0], Matrix.shape[1], dMea.shape[1]))
            stepD = np.diff(dMat)
            print("Reducing projection matrix to needed size...")
            for i in tqdm(range(Matrix.shape[1])):
                for n in range(Matrix.shape[0]):
                    stepMatrix = np.diff(Matrix[n,i,:])
                    #find closest indices
                    ind = np.searchsorted(dMat, dMea[n], side = "right") - 1
                    MatrixInterp[n,i,:] = stepMatrix[ind]/stepD[ind]*(dMea[n]-dMat[ind]) + Matrix[n,i,ind]
            if forceNew is False:
                interp = hf.create_group(groupName)
                interp.create_dataset("TransformMatrix"+str(nWires)+"Wires",  data=MatrixInterp, compression = "gzip")
                interp.create_dataset("xMat", data = xMat)
                interp.create_dataset("dMat", data = dMea)
                interp.create_dataset("nBinsRecGrid", data = [nBinsRecGrid])
                interp.create_dataset("angles", data = angles)
            else:
                del hf[groupName]
                interp = hf.create_group(groupName)
                interp.create_dataset("TransformMatrix"+str(nWires)+"Wires",  data=MatrixInterp, compression = "gzip")
                interp.create_dataset("xMat", data = xMat)
                interp.create_dataset("dMat", data = dMea)
                interp.create_dataset("nBinsRecGrid", data = [nBinsRecGrid])
                interp.create_dataset("angles", data = angles)
            return MatrixInterp, dMea, xMat, nBinsRecGrid, angles

def applyTomography(startProfile, transformMatrix, nBinsMatrix, projections, nIterationsMax, epsilon, nWires, sigmaSmooth, offset, sigmaSmoothFinal = 0, plot = False):
    divPlots = 512
    #generate circular mask
    circMask = ff.create_circular_mask(nBinsMatrix, nBinsMatrix)
    #make copy to opperate on
    profileNew = np.copy(startProfile)
    #apply it to input
    profileNew[~circMask]=0
    print("Applying tomographic reconstruction...")
    #prepare norm factor
    sums = np.sum(transformMatrix[:,:,:], axis = 2)
    #prepare mask
    sumMask = sums != 0
    #prepare plot
    if plot is True:
        f, ax = plt.subplots(ncols = int(nIterationsMax/divPlots), sharex = True, sharey = True)
        ax = ax.flatten()
    #begin iterations
    for n in tqdm(range(nIterationsMax)):
        #pick random angle
        i = np.random.randint(0, high = nWires - 1)
        #get simulated projections
        interpSimY = np.matmul(transformMatrix[i,:,:].T,profileNew.flatten())+offset
        #get fraction describing deviation
        fraction = projections[i]/interpSimY
        #condition to break if result is good
        if np.all(np.abs(1 - fraction) < epsilon):
            break
        #do backprojection
        backproj = np.matmul(transformMatrix[i,:,:],fraction)
        #apply normalization
        backproj[sumMask[i]] /= sums[i][sumMask[i]]
        #update profile
        profileNew.flat[sumMask[i]] *= backproj[sumMask[i]]
        #smoothen
        profileNew = gaussian_filter(profileNew, sigmaSmooth)
        #profileNew *= 1+0.1*(np.random.random_sample(profileNew.shape)-0.5)
        #plot
        if plot is True and n%divPlots == 0:
            ax[n//divPlots].set_title("Step" + str(n) + " Angle used:" + str(i))
            ax[n//divPlots].imshow(gaussian_filter(profileNew, sigmaSmoothFinal))
    profileNew = gaussian_filter(profileNew, sigmaSmoothFinal)
    if plot is True:
        plt.show()
    return profileNew

def testL1(profileStart, transformMatrix, nBinsMatrix, projections, d, angle, nWires, offset, pc, epsilon, nIterationsMax, sigmaMin, sigmaMax, nStepsSigma, sigmaSmoothFinal = 0, livePlot = True):
    #get points where measurements were taken
    pointsEvaluated = np.concatenate((d.reshape((-1,1)),angle.reshape((-1,1))), axis = 1)
    #generate array of sigmas
    sigmas = np.linspace(sigmaMin, sigmaMax, nStepsSigma)
    #generate circular mask
    circMask = ff.create_circular_mask(nBinsMatrix, nBinsMatrix)
    #make copy to opperate on
    profileNew = np.copy(profileStart)
    #apply it to input
    profileNew[~circMask]=0
    profileNew = profileNew.reshape(profileNew.shape[0], profileNew.shape[1], 1)
    profileNew = np.repeat(profileNew, nStepsSigma, axis = 2)
    #generate array to store results
    metrics = np.zeros((nIterationsMax+1, nStepsSigma))
    xAxis = [0.9] + list(range(1, nIterationsMax+1))
    #prepare updating plot
    if livePlot is True:
        plt.ion()
        f, ax = plt.subplots()
        ax.set_xlabel("n [iterations]")
        ax.set_ylabel("L1-Norm")
        ax.grid()
        line = list()
        minVal = np.inf
        #get reading for starting point
        for k in range(metrics.shape[1]):
            #do projection via radon to get ahold of norm
            metrics[0,k] = np.sum(np.abs((   pc.wireScannerBasic(pointsEvaluated, 0,0,0,0,0,0,offset,profile = profileNew[:,:,k]).flatten())
                                            -projections.flatten()))/len(projections.flatten())
            minVal = min(minVal, np.min(metrics[0,k]))
            line.append(ax.plot(xAxis, metrics[:,k].T, label = r"$\sigma = $" + str(np.floor(100*sigmas[k])/100) + r"$\,\mathrm{\mu m}$")[0])
        ax.legend(ncol = 3, title = r"$\sigma$ of gaussian smoothing filter, stepsize of measurement was "
                                    + str(np.floor(100*np.mean(np.diff(d[0])))/100) + r"$\,\mathrm{\mu m}$")
        maxVal = np.max(metrics)
        ax.set_ylim(minVal-1,maxVal+1)
        ax.set_xscale("log")
        ax.set_yscale("log")
    else:
        #get reading for starting point
        for k in range(metrics.shape[1]):
            #do projection via radon to get ahold of norm
            metrics[0,k] = np.sum(np.abs((   pc.wireScannerBasic(pointsEvaluated, 0,0,0,0,0,0,0,profile = gaussian_filter(profileNew[:,:,k], sigmaSmoothFinal)).flatten() + offset)
                                            -projections.flatten()))/len(projections.flatten())
    print("Calculating L1-Norms...")
    #prepare norm factor
    sums = np.sum(transformMatrix[:,:,:], axis = 2)
    #prepare mask
    sumMask = sums != 0
    #begin iterations
    for n in tqdm(range(nIterationsMax)):
        #pick random angle
        i = np.random.randint(0, high = nWires - 1)
        for k, sigma in enumerate(sigmas):
            #get simulated projections
            interpSimY = np.matmul(transformMatrix[i,:,:].T,profileNew[:,:,k].flatten())+offset
            #get fraction describing deviation
            fraction = projections[i]/interpSimY
            #condition to break if result is good
            if np.all(np.abs(1 - fraction) < epsilon):
                break
            #do backprojection
            backproj = np.matmul(transformMatrix[i,:,:],fraction)
            #apply normalization
            backproj[sumMask[i]] /= sums[i][sumMask[i]]
            #update profile
            profileNew[:,:,k].flat[sumMask[i]] *= backproj[sumMask[i]]
            #smoothen
            profileNew[:,:,k] = gaussian_filter(profileNew[:,:,k], sigma)
            #do projection via radon to get ahold of norm
            metrics[n+1,k] = np.sum(np.abs((   pc.wireScannerBasic(pointsEvaluated, 0,0,0,0,0,0,offset,profile = profileNew[:,:,k]).flatten())
                                            -projections.flatten()))/len(projections.flatten())
            if livePlot is True:
                if n%128 == 0:
                    #plot all the lines
                    line[k].set_ydata(metrics[:,k].T)
                    f.canvas.draw()
                    f.canvas.flush_events()
                    maxVal = max(maxVal, np.max(metrics[:,k]))
                    minVal = min(minVal, np.min(metrics[n+1,k]))
                    ax.set_ylim(minVal-1,maxVal+1)
                    ax.set_xlim(0.9, n+1)
            
    return metrics, sigmas, xAxis

if __name__ == '__main__':
    fnameMatrix = "TransformMatrices//test.h5"
    np.random.seed(0)
    #set input filename
    #filePath = "//AFS//psi.ch//user//k//kirchner_a//Documents//20190712//S2Polygon9Wires_20190713_055414"
    filePath = "/afs/psi.ch/intranet/SF/Diagnostics/ACHIP/Data/20190712/S2Polygon9Wires_20190713_055414"
    filePath = "/afs/psi.ch/intranet/SF/Diagnostics/ACHIP/Data/20191214/WSM9Wires_20191214_152259"
    #specify wires to be used, some presets: "all9", "all3", "3of9_0","3of9_1", "3of9_2", "3wiresCustom"
    wireUsagePreset = "all9"
    #set number of wires in input file here
    nWiresInFile = 9
    #set number of bins used for computation
    nBinsMat = 10000
    #set multiplier used to get computation interval from maxiumum displacement
    dMultipl = 2
    popt = [ 17.75362998,   0.65427387,  -0.24858024,  17.63736685,   2.63562088, 0.43273819, 134.44084097]
    nPerPoint = 1
    fileName, sensor, nWires, wiresUsed = prepareReadAndLoadPreset(filePath, wireUsagePreset, [])
    s,d,angles = loadAndPrepareInput(fileName, sensor, nWiresInFile, wiresUsed, False, nPerPoint = nPerPoint)
    #interpolate Matrix
    Ainterp, dMat, xMat, nBinsRecGrid, anglesMat = interpolateMatrix(fnameMatrix, d, "MeasurementS2Polygon9Wires_20190713_055414")
    #generate initial guess of profile to start with
    profile = evaluateFit(np.max(xMat), nBinsRecGrid, popt)
    if False:
        #generate wireScanModel to allow faster evaluation of transform for norm calculation
        pc = ff.wireScannerModel(np.max(xMat), nBinsRecGrid)
        #settings for where L1 is computed
        sigmaMin = 0
        sigmaMax = 1
        nStepsSigma = 10
        nIterationsMax = 4096
        #get L1 norm
        metrics, sigmas, xAxis = testL1(profile, Ainterp, nBinsRecGrid, s, d, angles, nWires, popt[-1], pc, 0.01, nIterationsMax, sigmaMin, sigmaMax, nStepsSigma)
        f, ax = plt.subplots()
        #plot all the lines
        for i in range(metrics.shape[1]):
            ax.plot(xAxis, metrics[:,i].T, label = r"$\sigma = $" + str(np.floor(100*sigmas[i])/100) + r"$\,\mathrm{\mu m}$")
        ax.set_xlabel("n [iterations]")
        ax.set_ylabel("L1-Norm")
        ax.legend(ncol = 3, title = r"$\sigma$ of gaussian smoothing filter, stepsize of measurement was "
                                    + str(np.floor(100*np.mean(np.diff(d[0])))/100) + r"$\,\mathrm{\mu m}$")
        ax.grid()
        plt.show()
    else:
        #run of tomographic reconstruction
        applyTomography(profile, Ainterp, nBinsRecGrid, s, 2048, 0.0001, nWires, 0.31, popt[-1], plot = True)

