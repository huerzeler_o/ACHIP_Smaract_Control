# This is the epics soft ioc running on the sf-achipi03 at SwissFEL

#!/usr/bin/env python
import time
import sys
from pcaspy import Driver, SimpleServer
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)
GPIO.setup(12, GPIO.OUT)

from interfaces import ACHIPI03

# define the epics variables test
prefix = 'SATSY03-DLAC080-DHXP:'

pvdb = {
        'SETUP': {'type': 'int',
                    'value': 0},
        'STATUS0': {'type': 'char',
                    'scan': 1,
                    'count': 100000},
        'STATUS1': {'type': 'char',
                    'scan': 1,
                    'count': 100000},
        'CONNECTED0': {'type': 'int',
                    'value': 0},
        'CONNECTED1': {'type': 'int',
                    'value': 0},
        'SUM0': {'type': 'int',
                    'scan': .5,
                    'value': 0},
        'SUM1': {'type': 'int',
                    'scan': .5,
                    'value': 0},
        'FLAG': {'type': 'int',
                    'value': 0},
        'PICTURE0': {'type': 'char',
                    'count': 1000},
        'PICTURE1': {'type': 'char',
                    'count': 1000},
        'FILENAME': {'type': 'char',
                    'count': 1000},
        'CLOSE': {'type': 'int',
                    'value': 0},
        'EXPOSURE0': {'type': 'int',
                    'value': 10},
        'EXPOSURE1': {'type': 'int',
                    'value': 10},
        'LEDON': {'type': 'int',
                    'value': 0},
}

class iocDriver(Driver):

    def __init__(self):
        super(iocDriver, self).__init__()
        self.cam0 = ACHIPI03('21574217')
        self.cam1 = ACHIPI03('21574226')
        # LED PWM object for pin 12
        self.p = GPIO.PWM(12, 10000)

    def write(self, reason, val):
        status = True

        # run the setup
        if reason == 'SETUP':
            if val == True:
                self.cam0.setup()
                self.setParam('CONNECTED0', 1)
                self.cam1.setup()
                self.setParam('CONNECTED1', 1)
            else:
                self.cam0.close()
                self.setParam('CONNECTED0', 0)
                self.cam1.close()
                self.setParam('CONNECTED1', 0)
        
        elif reason == 'FILENAME':
            val = val + '_'
            self.cam0.setName(val)
            self.cam1.setName(val)
            self.setParam('FILENAME', self.strToChr(val))

        elif reason == 'FLAG':
            self.cam0.flag = val
            self.cam1.flag = val
            self.setParam(reason, val)
        
        # read out the picture
        elif reason == 'PICTURE0':
            path = str(self.cam0.savePicture())
            self.setParam(reason, self.strToChr(path))
            self.updatePVs()
            return path
        elif reason == 'PICTURE1':
            path = str(self.cam1.savePicture())
            self.setParam(reason, self.strToChr(path))
            self.updatePVs()
            # return path?
            return path

        elif reason == 'EXPOSURE0':
            if 4 > val:
                val = 4
            self.cam0.setExposureAbs(val)
            self.setParam(reason, val)
            self.updatePVs()
            return True
        elif reason == 'EXPOSURE1':
            if 4 > val:
                val = 4
            self.cam1.setExposureAbs(val)
            self.setParam(reason, val)
            self.updatePVs()
            return True
        
        elif reason == 'CLOSE':
            self.cam0.close()
            self.cam1.close()
            self.setParam('CONNECTED0', 0)
            self.setParam('CONNECTED1', 0)
            return True

        elif reason == 'LEDON':
            self.p.start(val*10.)
            self.setParam(reason, val)

        self.updatePVs()
        return status

    def read(self, reason):
        status = True

        # read out the sum
        if reason == 'SUM0':
            sum0 = self.cam0.getSum(1)
            self.setParam(reason, sum0)
            self.updatePVs()
            return sum0

        if reason == 'SUM1':
            sum1 = self.cam1.getSum(1)
            self.setParam(reason, sum1)
            self.updatePVs()
            return sum1

        # read out the status
        if reason == 'STATUS0':
            info =str(self.cam0.ret)
            self.setParam('STATUS0', self.strToChr(info))
            self.updatePVs()
            return info
        if reason == 'STATUS1':
            info = str(self.cam1.ret)
            self.setParam('STATUS1', self.strToChr(info))
            self.updatePVs()
            return info
        
        # read out if the cameras are connected
        if reason == 'CONNECTED0':
            self.getParam('CONNECTED0')
            self.updatePVs()
            return True
        if reason == 'CONNECTED1':
            self.getParam('CONNECTED1')
            self.updatePVs()
            return True

        self.updatePVs()
        #print('PVs updated...')
        return status

    def strToChr(self, msg): # convert string to character array
        return [ord(s) for s in msg]

if __name__ == '__main__': #create PVs based on prefix and pvdb definition
    server = SimpleServer()
    server.createPV(prefix, pvdb)
    driver = iocDriver()
    while True:
        server.process(0.1) # process CA transactions
