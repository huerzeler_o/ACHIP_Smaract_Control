# This Class enables a remote readout of the Basler cameras.

import numpy as np
from pypylon import pylon
from pypylon import genicam
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import sys
import os
import time
import random

class ACHIPI03():
    """ Raspberry Pi connected to 2 Basler cameras. """
    def __init__(self, serialNumber):
            self.exposureTimeAbs = 10
            self.camera = pylon.InstantCamera()
            self.serialNumber = serialNumber # 21574217, 21574226
            self.fileName = serialNumber
            self.ret = ''
            self.setup()
            self.flag = 0

    def setup(self):
        try:
            # Get the transport layer factory.
            tlFactory = pylon.TlFactory.GetInstance()

            # Get all attached devices and exit application if no device is found.
            devices = tlFactory.EnumerateDevices()
            if len(devices) == 0:
                raise pylon.RUNTIME_EXCEPTION("No camera present.")
            
            if devices[0].GetSerialNumber() == self.serialNumber:
                self.camera.Attach(tlFactory.CreateDevice(devices[0]))
            
            elif devices[1].GetSerialNumber() == self.serialNumber:
                self.camera.Attach(tlFactory.CreateDevice(devices[1]))

            else:
                self.ret = 'Serial number doesnt match.'
                print(self.ret)
                return self.ret

            self.camera.Open()
            self.camera.ExposureTimeAbs.SetValue(self.exposureTimeAbs)
            self.camera.StartGrabbing(pylon.GrabStrategy_LatestImageOnly, pylon.GrabLoop_ProvidedByUser)

            self.ret = 'Camera (serial nr. '+str(self.serialNumber)+'): Setup done...'
            print(self.ret)
            #return self.ret

        except genicam.GenericException as e:
            self.ret = "An exception occurred.", e.GetDescription()
            print(self.ret)
    
    def readCamera(self):
        # read out the sum of all the pixels

        success = False
        while not success:
            grabResult = self.camera.RetrieveResult(5000, pylon.TimeoutHandling_ThrowException)
            if grabResult.GrabSucceeded():
                success = True
                array = grabResult.GetArray()
                grabResult.Release()
                #self.camera.StopGrabbing()
                #self.ret = 'Cam read out.'
                #print(self.ret)
                return array
            else:
                self.ret = 'camera grab error code: ' + str(grabResult.GetErrorCode())
                self.camera.StopGrabbing()
                print(self.ret)
                time.sleep(.1) # give time to recover
                self.camera.StartGrabbing(pylon.GrabStrategy_LatestImageOnly, pylon.GrabLoop_ProvidedByUser)

    def getSum(self, amount):
        if self.flag == 1:
            value = 0
            for i in range(amount):
                value += np.sum(self.readCamera())
            result = value/amount
            #self.ret = 'Calculated Sum: ' + str(result)
        else:
            result = int(random.random()*(-100))
            #self.ret = 'Generated Sum: ' + str(result)
        #print(self.ret)
        return result

    def savePicture(self):
        array = self.readCamera()
        filename = '/home/pi/Documents/ACHIP/ACHIP_Camera_Control/output/'+ self.fileName +'.png'
        plt.imsave(filename, array)
        self.ret = ('Picture saved: ' + str(filename))
        print(self.ret)
        return self.ret

    def setExposureAbs(self, exposureTimeAbs):
        self.camera.Open()
        self.camera.ExposureTimeAbs.SetValue(exposureTimeAbs)
        self.ret = 'ExposureTimeAbs set to ' + str(self.camera.ExposureTimeAbs())
        print(self.ret)
        return self.ret

    def close(self):
        self.camera.Close()
        self.ret = 'Closed.'
        print (self.ret)
        return self.ret

    def setName(self, name):
        self.fileName = name + str(self.serialNumber)
        self.ret = 'Filename set to ' + self.fileName
        print(self.ret)
        return self.fileName







