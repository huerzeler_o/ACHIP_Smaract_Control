import sys
from PyQt4 import QtCore,QtGui
from PyQt4.uic import loadUiType
from epics import caget, caput

from PyQt4.QtCore import pyqtSlot, QTimer, QThread, QSize
from PyQt4 import uic, Qt
from PyQt4.QtGui import QSizePolicy, QMessageBox, QInputDialog, QLineEdit, ksqueezedtextlabel

Ui_MainWindow, QMainWindow = loadUiType('ControlUI.ui')



class Main(QMainWindow, Ui_MainWindow):         # class to start the application
    def __init__(self, ):                       # definition of the constructor
        super(Main, self).__init__()
        self.setupUi(self)                      # call the UI's setup function
        self.TakePicture0.clicked.connect(self.takePic0)
        self.TakePicture1.clicked.connect(self.takePic1)
        self.fileName.valueChanged.connect(self.rename)

    def takePic0(self):
        caget('MTEST:PICTURE0')
        print('Picture0 is taken')

    def takePic1(self):
        caget('MTEST:PICTURE1')
        print('Picture1 is taken')
    
    def rename(self):
        caput('MTEST:FILENAME', 'blabla')


if __name__ == "__main__":                      # main routine
    app = QtGui.QApplication(sys.argv)
    app.setStyle('cleanlooks')                  # needed on some systems

    main = Main()
    main.show()

    sys.exit(app.exec_())