import numpy as np
from epics import caget, caput
import matplotlib.pyplot as plt
from time import sleep, time
from h5py import File

blmName = 'SINLH02-DBLM235'
bunchID = '1'

loss = blmName+':B'+bunchID+'_LOSS'
lossRaw = blmName+':LOSS_SIGNAL_RAW'
roiStart = int(caget(blmName+':B'+bunchID+'_ROI_START'))
roiStop = int(caget(blmName+':B'+bunchID+'_ROI_STOP'))
#print(caget(lossRaw))

fig = plt.figure()
ax = fig.add_subplot(111)

data =[]
tStart = time()
for i in range(10000):
    t0=time()
    raw = np.array(caget(lossRaw))
    data.append(raw)
#    ax.plot(raw[roiStart:roiStop])
    sleep(max(0, 1e-2-(time()-t0)))
    
    
    
print(time()-tStart)

data=np.array(data, dtype=int)

file = File('data.h5', 'a')
file.create_dataset('rawHist', data=data)