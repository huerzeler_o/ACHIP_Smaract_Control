import visa
from pyvisa.resources import MessageBasedResource
import matplotlib.pyplot as plt
import numpy as np
scopeIP = "129.129.130.169"
visa.log_to_screen()
rm = visa.ResourceManager()
scope = rm.open_resource("TCPIP::" + scopeIP + "::INSTR", resource_pyclass=MessageBasedResource)
scope.write("COMM_HEADER OFF")
scope.write(r"""vbs 'app.acquisition.triggermode = "stopped" ' """)
scope.write(r"""vbs 'app.acquisition.triggermode = "single" ' """)
scope.write(r"""vbs 'app.measure.clearall ' """)
scope.write(r"""vbs 'app.measure.clearsweeps ' """)
scope.write(r"""vbs 'app.measure.showmeasure = true ' """)
scope.write(r"""vbs 'app.measure.statson = true ' """)
scope.write(r"""vbs 'app.measure.p1.view = true ' """)
scope.write(r"""vbs 'app.measure.p1.paramengine = "Area" ' """)
scope.write(r"""vbs 'app.measure.p1.source1 = "C1" ' """)
for i in range(0,3):
    r = scope.query(r"""vbs? 'return=app.acquisition.acquire( 0.1 , True )' """)
    r = scope.query(r"""vbs? 'return=app.WaitUntilIdle(5)' """)
    res = scope.query(r"""vbs? 'return=app.measure.p1.out.result.value' """)
    """
    for j in range(0,10):
        wav = scope.query(rvbs? 'return=app.C1.GetScaledWaveform("c1", 5000, 1)' ")
        print(wav)
    """
    print(res)
    if r==0:
        print("Time out from WaitUntilIdle, return = {0}".format(r))
res = scope.query(r"""vbs? 'return=app.acquisition.measure.p1.out.result.value' """)
print(res)