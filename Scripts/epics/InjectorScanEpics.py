# -*- coding: utf-8 -*-

import sys
import os
import h5py
import matplotlib.pyplot as plt
import numpy as np
from epics import caget, caput, caget_many
import datetime
from time import sleep, time
from shutil import copyfile
from scipy.optimize import curve_fit

######################################  

#Define scan parameters below

######################################

scanName = 'PSI800nm' + '_' + str(datetime.datetime.now().strftime("%Y%m%d_%H%M%S"))


# 5 um Sample 3 PSI
#startPoint = 156.441-0.0150 # start, end point, in mm, range: 130-346      
#endPoint = startPoint+2*0.0150                            
#startPoint = 156.384-0.02
#endPoint = 156.384+0.02    

# 2 um Sample 4 PSI
# startPoint = 132.696-0.02 
# endPoint = startPoint+2*0.02     

# 1 um Sample 4 PSI
#startPoint = 133.396-0.01 # start, end point, in mm, range: 130-346      
#endPoint = startPoint+2*0.01

# 800nm Sample 2 PSI
startPoint = 177.178-0.008 # start, end point, in mm, range: 130-346      
endPoint = startPoint+2*0.008

# 900nm Sample 1 FERMI
#startPoint = 198.696- 0.008 # start, end point, in mm, range: 130-346      
#endPoint = startPoint+2*0.008

# 1um Sample 1 FERMI
#startPoint = 199.397-0.008 # start, end point, in mm, range: 130-346      
#endPoint = startPoint+2*0.008


repRate = 1
nSteps = 3 # number of steps from start to end. (number of Measurement positions: nSteps+1)
nShots = 3 # number of shots for each position

delayTime = 0.2 # wait time after position reached in each step
useMotor = False # True for measurement
debug = False # False for Measurement, takes beamOK=True
showGaussFit = True # in case of error related to gauss fit: False

points = [startPoint, endPoint]  

directory = '/afs/psi.ch/intranet/SF/Diagnostics/ACHIP/Data/InjectorWS/20190331/'
if not os.path.exists(directory):
    os.makedirs(directory)

fileName = directory + scanName + '.h5'

######################################  

#Define PVs to record below

######################################

setPosPV = 'SINDI02-DLAC055:MOTOR_Y1'
getPosPV = 'SINDI02-DLAC055:MOTOR_Y1.RBV'

### epics sensors read every step
sensors = ['SINDI02-DBLM025:B1_LOSS', 'SINDI02-DBLM085:B1_LOSS', 
	   'S10DI01-DBLM045:B1_LOSS', 'SINDI02-DBLM085:LOSS_SIGNAL_RAW',
           'SINDI02-DBPM040:Q1', 'SINDI02-DBPM040:X1', 'SINDI02-DBPM040:Y1',
           'SINDI02-DBPM080:Q1', 'SINDI02-DBPM080:X1', 'SINDI02-DBPM080:Y1']

filterPV = 'SINDI02-DBLM085:B1_LOSS'
plotSensors = ['SINDI02-DBLM085:B1_LOSS', 'SINDI02-DBLM085:LOSS_SIGNAL_RAW']

### Epics Channels, read once before Scan, e.g. quad
EpicsChannels = ['SINDI02-MQUA050:I-SET',
                 'SINDI02-MCRX050:I-SET',
                 'SINDI02-MCRY050:I-SET',
                 'SINDI02-MQUA060:I-SET', 
                 'SINDI02-MCRX060:I-SET',
                 'SINDI02-MCRY060:I-SET',
                 'SINDI02-DBLM084:AL2-WS-GAIN-TOTAL']

posReadRec = []
posSetRec = []
sensorValues = {}
for sen in sensors:
    sensorValues[sen]=[]

EpicsSensorValues = {}
for sen in EpicsChannels:
    EpicsSensorValues[sen]=[]
    
pulseIdRec = []
tsRec = []


def filter_method(d):
    filterPVIdx = sensors.index(filterPV)
    filterData = [s[filterPVIdx] for s in d]
    idx = np.argmax(filterData)
    return d[idx]
    
# Reads out the actual data once the stage has stopped
def readData():
    
    global nTotalShots
    global nTotalShotsMeasured
    global progress
    
    
    nShotsMeasured = 0
    
    while nShotsMeasured < nShots:    
        
        d = []
        t0 = time()
        while(time()-t0 < 1/repRate):
            d.append(caget_many(sensors))
            sleep(0.001)
        
        dFilter = filter_method(d) 
        for sen in sensors:
            sensorValues[sen].append(dFilter[sensors.index(sen)])
        posReadRec.append(caget(getPosPV))
        posSetRec.append(caget(setPosPV))
        nShotsMeasured += 1
        nTotalShotsMeasured += 1
        progress = nTotalShotsMeasured/nTotalShots*100
        print('shot: '+str(nShotsMeasured)+'/'+str(nShots))
        print('overall progress: '+str(progress)+' %')
        
    return True
      
# saves the data arrays into a .h5 file
def writeFile():
    with h5py.File(fileName, 'a') as hdf:
        hdf.create_dataset('PositionRead', data = np.array(posReadRec, dtype = float))
        hdf.create_dataset('PositionSet', data = np.array(posSetRec, dtype = float))
        for sen in sensors:
            hdf.create_dataset('SensorValue/'+sen, data = np.array(sensorValues[sen], dtype = float))
        for sen in EpicsChannels:
            hdf.create_dataset('SensorValue/'+sen, data = np.array(EpicsSensorValues[sen], dtype = float))
        
        

def positionReached(goal):
    
    currentPosition = caget(getPosPV)
   
    # make a delta for x,y,z and another one with rx,ry,rz
    delta = np.abs(goal-currentPosition)
    
    #if delta <= 0.001 and caget('SINDI02-DLAC055:MOTOR_Y1.MOVN')==0: # 1000 nm
    if caget('SINDI02-DLAC055:MOTOR_Y1.MOVN')==0: # not moving
         
        print('position reached, Delta Position is: ' + str(delta))
     
        
        return True
    else:
        print('   position not reached')
        print('   Goal is:             ' + str(goal))
        print('   Actual position:     ' + str(currentPosition))
        print('   Delta Position is:   ' + str(delta))

        return False

# charges the calculated positions into the absolute position and move
def loadPoint(goal):
    if useMotor:
        caput(setPosPV, goal)
        while not positionReached(goal):
            sleep(.1)

def doStep(delta, goal):
    if useMotor:
        caput('SINDI02-DLAC055:MOTOR_Y1.TWV', delta)
        caput('SINDI02-DLAC055:MOTOR_Y1.TWF', 1)
        while not positionReached(goal):
            sleep(.1)
        
# function to scan from pointA to pointB
def scan(pointA, pointB):
    ### read Epics channels before scan
    for sen in EpicsChannels:
        val=caget(sen)
        EpicsSensorValues[sen].append(val)
    delta = (pointB - pointA)/nSteps
    loadPoint(pointA)
    for i in range(nSteps+1):
        print('---------------------------------------------------------------')
        print('step: '+str(i)+'/'+str(nSteps))
        sleep(delayTime)
        readData()
        #loadPoint(pointA+i*delta)
        doStep(delta, pointA+i*delta)

def gauss(x,a,x0,sigma, offset):
    return a*np.exp(-(x-x0)**2/(2*sigma**2))+offset
def plotGaussFit(ax, xdata, ydata):
    x = np.array(xdata)
    y = np.array(ydata)
    n = len(x)                          #the number of data
    # initial parameter guesses for gauss fit
    offset = min(y)
    amp = max(y)-offset
#    mean = np.sum(x*(y-offset))/np.sum(y)                   #note this correction
#    sigma = np.sqrt(np.sum((y-offset)*(x-mean)**2)/np.sum(y))        #note this correction
    mean = np.sum(x*(y-offset))/np.sum(y-offset)                   
    sigma = np.sqrt(np.sum((y-offset)*(x-mean)**2)/np.sum(y-offset))        
    
    
    popt,pcov = curve_fit(gauss,x,y,p0=[amp,mean,sigma, offset])
    xFit = np.linspace(min(x), max(x), 1000)
    ax.plot(xFit, gauss(xFit,*popt), label='Gauss Fit', ls = '-', color = 'k')
    ax.set_title('Gauss Fit Sig. '+str(popt[2]))
    
def plotData(outputFile):
    file = h5py.File(fileName, 'r')
    unit = 'mm'
    
    #plot loss profile
    fig1 = plt.figure(figsize = (9,6))
    ax1 = fig1.add_subplot(111)
    
    posData = np.array(file['PositionRead'].value)
    for s in plotSensors:
        data = np.array(file['SensorValue/'+s].value)
        if data.ndim==1:
            data = np.array(file['SensorValue/'+s].value)
            data-= np.average(data[-10:])
            mean = np.sum(posData*data)/np.sum(data)
            rms = np.sqrt(np.sum((posData-mean)**2*data))/np.sqrt(np.sum(data))
            print(mean, rms)
            ax1.set_title('rms: '+str(rms))
            ax1.plot(posData, data, label = s, ls = '-', marker = '+', ms = 10, color = 'r')
            if showGaussFit:
                try:
                    plotGaussFit(ax1, posData, data)
                except:
                    pass
            
    ax1.set_xlabel('Position ('+unit+')')
    ax1.set_ylabel('BLM Loss')
    ax1.ticklabel_format(style='sci', scilimits = (-2,2), axis='both')

    ax1.legend()

    fig1.tight_layout()
    fig1.savefig(directory + scanName + '_BLM.png')
    
    #plot waveforms
    if False:
        fig2 = plt.figure(figsize = (16,9), dpi = 200)
        ax2 = fig2.add_subplot(111)
        
        for s in plotSensors:
            data = np.array(file['SensorValue/'+s].value)
            if data.ndim==2:
                for i in range(len(posData)):
                    ax2.plot(data[i], label = str(posData[i])+' mm', ls = '-')
			
        ax2.set_xlabel('Sample')
        ax2.set_ylabel('Waveform')
        ax2.ticklabel_format(style='sci', scilimits = (-2,2), axis='both')
        
        ax2.legend()
    
        fig2.tight_layout()
        fig2.savefig(directory + scanName + '_Waveform.png')

        
nScans = int(len(points)/2)
nTotalShots = nScans*(nSteps+1)*nShots
nTotalShotsMeasured = 0
progress = 0.

i=0
while i < len(points):
    print('---------------------------------------------------------------')
    print('---------------------------------------------------------------')
    print('scan: '+str(int(i/2+1))+'/'+str(int(nScans)))
    scan(points[i], points[i+1])
    i += 2

# save the file 
writeFile()

# plot data
plotData(fileName)
plt.show()

print('Script ended successfully.')
sys.exit(0)
