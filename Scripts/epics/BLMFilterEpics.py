from epics import caget, caput
from time import sleep, time
import numpy as np

#blm = 'SATSY03-DBLM085:B1_LOSS'
blm = 'SATCL01-DBLM135:B1_LOSS'
beamOK = 'SIN-CVME-TIFGUN-EVR0:BEAMOK'
lossREC = 'SATSY03-DLAC080-DHXP:BLM1'
repRate = 1

while True:
    d = []
    t0 = time()
    while(time()-t0 < 1/repRate):
        d.append(caget(blm))
        sleep(0.001)
    blmValue = np.max(d)           
    #write blmValue to epics waveform
    try:
        caput(lossREC, blmValue)
        print(blm, blmValue)
    except:
        print(blm, 'incorrectly grabbed.')
    

