import sys
import os
from epics import caget, caput
import pifacedigitalio
import datetime
import logging
from time import time, sleep
import numpy as np

'''OFFICIAL BAG LIMIT'''
BAGLimit = 0.036 # charge in uC that is allowed during 1 hour.

logging.basicConfig(format='%(asctime)s %(levelname)s %(name)s: %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', level=logging.DEBUG, filename='/home/pi/Documents/security_routine.log')

pifacedigital = pifacedigitalio.PiFaceDigital()
relail0 = pifacedigital.output_pins[0]
relail1 = pifacedigital.output_pins[1]
epicsWaitTime = 1 # time to wait for epics channel to receive
actualState = -1

# initial set
logging.info('Start script.........')
relail0.turn_off()
sleep(0.1)
relail1.turn_off()
sleep(0.1)

# alarm function
def setAlarm(reason, message = ''):
    global actualState
    caput('SATSY03-DLAC080-DHXP:MPSStatusMessage', message)
    if reason == 0 and actualState != reason: # no alarm
        relail0.turn_on()
        relail1.turn_on()
        actualState = 0
        caput('SATSY03-DLAC080-DHXP:Alarm', 0)
        logging.info(message + '\x1b[1;32;40m' + ' -----> no alarm' + '\x1b[0m')
    if reason == 1 and actualState != reason: # l0 alarm 1Hz for screen position
        relail0.turn_off()
        relail1.turn_on()
        actualState = 1
        caput('SATSY03-DLAC080-DHXP:Alarm', 1)
        logging.info(message + '\x1b[1;31;40m' + ' -----> set alarm: l0' + '\x1b[0m')
    if reason == 2 and actualState != reason: # l1 alarm total block
        relail0.turn_off()
        relail1.turn_off()
        actualState = 2
        caput('SATSY03-DLAC080-DHXP:Alarm', 2)
        logging.warning(message + '\x1b[1;31;40m' + ' -----> set alarm: l1' + '\x1b[0m')

# make sure the ethernet connection is up and running
while os.system('ping -c 1 -q sf-gw') != 0:
    logging.warning('ping sf-gw failed')
    setAlarm(2, 'No connection at startup.')
    sleep(1)

logging.info('Network found, Start Security Routine')
logging.info('Test PV: SF Mode: '+str(caget('SATSY03-DLAC080-DHXP:SwissFELmode')))
# start security routine
currentHist = np.array([])
integratedCharge = 0 # charge per last hour during achip mode in pC
projectedCharge = 0 # projected 1h sum from last shot
loopTime = 0 # calculation time of one loop

while True:
    try:
        # read all required PVs and timestamp
        startTime = time()        
        connected = caget('SATSY03-DLAC080-DHXP:isConnected', timeout=epicsWaitTime)
        referenced = caget('SATSY03-DLAC080-DHXP:isReferenced', timeout=epicsWaitTime)
        SwissFELmode = caget('SATSY03-DLAC080-DHXP:SwissFELmode', timeout=epicsWaitTime)
        parkPosition = caget('SATSY03-DLAC080-DHXP:ParkPosition', timeout=epicsWaitTime)
        screenPosition = caget('SATSY03-DLAC080-DHXP:ScreenPosition', timeout=epicsWaitTime)
        chargeBeforeAchip1 = 200.
        chargeBeforeAchip2 = 200.
        try:
            chargeBeforeAchip1 = float(caget('SATSY03-DBPM060:EST-Q1-1S-ACALC', timeout=epicsWaitTime))
            chargeBeforeAchip2 = float(caget('SATSY03-DBPM060:EST-Q2-1S-ACALC', timeout=epicsWaitTime))
        except:
            chargeBeforeAchip1 = float(caget('SATSY01-DBPM060:EST-Q1-1S-ACALC', timeout=epicsWaitTime))
            chargeBeforeAchip2 = float(caget('SATSY01-DBPM060:EST-Q2-1S-ACALC', timeout=epicsWaitTime))
    except:
        setAlarm(2, 'Error: Cannot connect to the PVs.')
        logging.error('Error: Cannot connect to the PVs.')
    
    try:
        totalCharge = chargeBeforeAchip1 + chargeBeforeAchip2 # in pC/s
        if SwissFELmode or parkPosition:
            totalCharge = 0.
        
        currentHist = np.append(currentHist, totalCharge)
        
        #remove entries from list that are older than 1 hour
        if len(currentHist) > 3600:    
            currentHist = currentHist[-3600:]
        
        integratedCharge = np.sum(currentHist)*1e-6 # in uC during last hour
        projectedCharge = totalCharge*3600*1e-6 # in uC
    except:
        setAlarm(2, 'Error: Charge Limit Calculation')
        logging.error('Error: Charge Limit Calculation')    
    
    
    try:
        if connected and referenced:
            if SwissFELmode:
                if parkPosition:
                    setAlarm(0, 'SwissFEL mode and in park position. --> no alarm')
                else:
                    setAlarm(2, 'SwissFEL mode and not in park position. --> set alarm: L1') #L1 no beam
            if SwissFELmode == False:
                if parkPosition:
                    setAlarm(0, 'ACHIP mode and in park position.')
                elif screenPosition:
                    setAlarm(1, 'ACHIP mode and in screen position. --> set alarm: L0') #L0, 1hz beam
                else:
                    if integratedCharge > BAGLimit:
                        setAlarm(2, 'ACHIP mode BAG limit reached. --> set alarm: L1') #L1 no beam
                    else:
                        setAlarm(0, 'ACHIP mode BAG limit not reached. --> no alarm')
        else:
            setAlarm(2, 'Error: Conncection: '+str(connected)+', Reference: '+str(referenced))
    except:
        setAlarm(2, 'Error: Failed to set/reset alarm to relais board')
        logging.error('Error: Failed to set/reset alarm to relais board')
    try:
        caput('SATSY03-DLAC080-DHXP:MPSTime', int(startTime),timeout=epicsWaitTime)
        caput('SATSY03-DLAC080-DHXP:ACHIPCharge1h', integratedCharge,timeout=epicsWaitTime)
        caput('SATSY03-DLAC080-DHXP:ACHIPCharge1hPrediction', projectedCharge,timeout=epicsWaitTime)
    except:
#        setAlarm(2, 'Error: Cannot connect to the PVs.')
        logging.error('Error: Cannot connect to the status (put) PVs.')
    
    endTime = time()
    sleep(max(0.1, 1-(endTime-startTime))) # sleep for rest of second
    
