import sys
import os
import h5py
import matplotlib.pyplot as plt
import numpy as np
from epics import caget, caput
import datetime
import time
from time import sleep
#from shutil import copyfile

######################################  

#Define scan parameters below

######################################

#directory = '/afs/psi.ch/intranet/SF/data/2020/10/03/'
#directory = '/afs/psi.ch/intranet/SF/Diagnostics/ACHIP/Data/20210313/'
directory = '/afs/psi.ch/intranet/SF/data/2021/03/13/'
scanName = 'w8XB2D' + '_' + str(datetime.datetime.now().strftime("%Y%m%d_%H%M%S"))
#scanName = 'Background' + '_' + str(datetime.datetime.now().strftime("%Y%m%d_%H%M%S"))

start = -1140
end = -1040
                    
points = [start, end]  

nSteps = 45 # number of steps from start to end. (number of Measurement positions: nSteps+1)
nShots = 1 # number of shots for each position

delayTime = 0. # wait time after position reached in each step
useMotor = 1# True for measurement

if not os.path.exists(directory):
    os.makedirs(directory)

fileName = directory + scanName + '.h5'

######################################  

#Define PVs to set and record below

######################################

#setPosPV = 'SATSY03-DLAC080-DHXP:absposX'
#goPosPV = 'SATSY03-DLAC080-DHXP:hSet6d'
#getPosPV = 'SATSY03-DLAC080-DHXP:posX'

setPosPV = 'SATSY03-DLAC080-DHXP:absposY'
goPosPV = 'SATSY03-DLAC080-DHXP:hSet6d'
getPosPV = 'SATSY03-DLAC080-DHXP:posY'

#setPosPV = 'S20:SET-E-GAIN-OP'
#goPosPV = 'SATSY03-DLAC080-DHXP:hSet6d'
#getPosPV = 'S20:SET-E-GAIN-OP'


### epics sensors, read at every shot
sensors = ['SATCL01-DSCR150:FPICTURE', 'SATSY02-DBPM210:Q2', 'S10:GET-BEAM-PHASE-OP', 'SATSY03-DLAC080-DHXP:BLM1', 'SATSY03-DLAC080-DHXP:posX', 'SATSY03-DLAC080-DHXP:posY'] 
#sensors = ['SATCL01-DSCR150:FPICTURE', 'SATSY02-DBPM210:Q2', 'S10:GET-BEAM-PHASE-OP', 'SATSY03-DLAC080-DHXP:BLM1', 'SATSY03-DLAC080-DHXP:posX', 'SATSY03-DLAC080-DHXP:posY'] 

### Epics Channels, read once before Scan, e.g. quad
sensorsOnce = ['SATCL01-DSCR150:HEIGHT', 'SATCL01-DSCR150:WIDTH', 'S10BC02-MQSK350:I-READ', 'SINBC02-MQSK350:I-READ']

#Hexapod and Lense Pos 
for p in ['posX', 'posY', 'posZ', 'posRX', 'posRY', 'posRZ', 'posLIN3X', 'posLIN3Y', 'posLIN3Z']:
    sensorsOnce.append('SATSY03-DLAC080-DHXP:'+p)
#Permanent Magnet Pos
for p in ['1:POSITION-X', '1:POSITION-Y' , '2:POSITION-X', '2:POSITION-Y' ,'3:POSITION-X', '3:POSITION-Y' , '7:POSITION-X', '7:POSITION-Y' ,'8:POSITION-X', '8:POSITION-Y' , '9:POSITION-X', '9:POSITION-Y']:
    sensorsOnce.append('SATSY03-MQUA08'+p)

def readData(length):
    
    global nTotalShots
    global nTotalShotsMeasured
    global progress
    
    
    nShotsMeasured = 0
    
    while nShotsMeasured < nShots:    
        
        
        tsRec.append(time.time())
        for sen in sensors:
            if 'FPICTURE'in sen:
                sensorValues[sen].append(np.array(caget(sen), dtype = np.uint16)[:length])
            else:
                sensorValues[sen].append(np.array(caget(sen), dtype = float))
        posReadRec.append(caget(getPosPV))
        posSetRec.append(caget(setPosPV))
        nShotsMeasured += 1
        nTotalShotsMeasured += 1
        progress = nTotalShotsMeasured/nTotalShots*100
        print('shot: '+str(nShotsMeasured)+'/'+str(nShots))
        print('overall progress: '+str(progress)+' %')
        sleep(1)
    return True
      
# saves the data arrays into a .h5 file
def writeFile():
    with h5py.File(fileName, 'a') as hdf:
        hdf.create_dataset(getPosPV, data = np.array(posReadRec, dtype = float))
        hdf.create_dataset(setPosPV, data = np.array(posSetRec, dtype = float))
        for sen in sensors:
            if 'FPICTURE' in sen:
                hdf.create_dataset('SensorValue/'+sen, data = np.array(sensorValues[sen], dtype = np.uint16))
            else:
                hdf.create_dataset('SensorValue/'+sen, data = np.array(sensorValues[sen], dtype = float))
        for sen in sensorsOnce:
            hdf.create_dataset('SensorValueOnce/'+sen, data = np.array(sensorsOnceValues[sen], dtype = float))
        
        
        hdf.create_dataset('timeStamp', data = np.array(tsRec, dtype = float))
      

def positionReached(goal):
    
    currentPosition = caget(getPosPV)
   
    # make a delta for x,y,z and another one with rx,ry,rz
    delta = np.abs(goal-currentPosition)
    
    if delta <= 0.01: 
         
        print('target reached, Delta Position is: ' + str(delta))
     
        
        return True
    else:
        print('   target not reached')
        print('   Goal is:             ' + str(goal))
        print('   Actual val:     ' + str(currentPosition))
        print('   Delta val is:   ' + str(delta))

        return False

# charges the calculated positions into the absolute position and move
def loadPoint(goal):
    if useMotor:
        caput(setPosPV, goal)
        caput(goPosPV, [1])
        
        while not positionReached(goal):
            time.sleep(.1)
        
# function to scan from pointA to pointB
def scan(pointA, pointB):
    ### read Epics channels before scan
    for sen in sensorsOnce:
        val=caget(sen)
        sensorsOnceValues[sen].append(val)
    stepSize = (pointB - pointA)/nSteps
    height = int(sensorsOnceValues['SATCL01-DSCR150:HEIGHT'][0])
    width = int(sensorsOnceValues['SATCL01-DSCR150:WIDTH'][0])

    for i in range(nSteps+1):
        print('---------------------------------------------------------------')
        print('step: '+str(i)+'/'+str(nSteps))
        loadPoint(pointA+i*stepSize)
        sleep(delayTime)
        readData(height*width)
        
       


def plotData(outputFile):
    file = h5py.File(fileName, 'r')
    
    fig1 = plt.figure(figsize = (12,6))
    ax1 = fig1.add_subplot(121)
    ax2 = fig1.add_subplot(122)
    posData = np.array(file[getPosPV].value)
   
    imgsRaw = np.array(file['SensorValue/SATCL01-DSCR150:FPICTURE'].value)
    width = int(file['SensorValue/SATCL01-DSCR150:WIDTH'].value)
    height = int(file['SensorValue/SATCL01-DSCR150:HEIGHT'].value)
    
    imgs = imgsRaw[:, :int(height*width)].reshape(len(imgsRaw), height, width)
    
    ax1.imshow(imgs[0])
    ax1.set_title('img[0]')
    ax2.plot(posData, np.sum(imgs, axis=(1,2)), ls='-', marker = '+', ms = 10, color = 'c')
    
    ax2.set_xlabel(getPosPV)
    ax2.set_ylabel('Image Sum')
    ax2.ticklabel_format(style='sci', scilimits = (-2,2), axis='both')

    fig1.tight_layout()
    fig1.savefig(directory + scanName + '_IMGSUM.png')
    

posReadRec = []
posSetRec = []
sensorValues = {}
for sen in sensors:
    sensorValues[sen]=[]

sensorsOnceValues = {}
for sen in sensorsOnce:
    sensorsOnceValues[sen]=[]
    
tsRec = []      
nScans = int(len(points)/2)
nTotalShots = nScans*(nSteps+1)*nShots
nTotalShotsMeasured = 0
progress = 0.

i=0
while i < len(points):
    print('---------------------------------------------------------------')
    print('---------------------------------------------------------------')
    print('scan: '+str(int(i/2+1))+'/'+str(int(nScans)))
    scan(points[i], points[i+1])
    i += 2

# save the file 
writeFile()

# plot data
#plotData(fileName)
plt.show()

print('Script ended successfully.')
sys.exit(0)
