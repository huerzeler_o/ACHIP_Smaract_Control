from bsread import source
from epics import caget, caput
from time import sleep
from pcaspy import Driver, SimpleServer

bsChannels = ['SARCL02-DBLM135:B1_LOSS']

bsChannels = [
'S10BC01-DBLM065:B1_LOSS',
'S10CB04-DBLM240:B1_LOSS',
'S10CB06-DBLM240:B1_LOSS',
'S10CB08-DBLM240:B1_LOSS',
'S10DI01-DBLM015:B1_LOSS',
'S10DI01-DBLM045:B1_LOSS',
'S20CB02-DBLM435:B1_LOSS',
'S20SY02-DBLM075:B1_LOSS',
'S20SY03-DBLM025:B1_LOSS',
'S20SY03-DBLM110:B1_LOSS',
'S30CB02-DBLM445:B1_LOSS',
'S30CB06-DBLM445:B1_LOSS',
'S30CB10-DBLM445:B1_LOSS',
'S30CB14-DBLM445:B1_LOSS',
'SARCL02-DBLM135:B1_LOSS',
'SARCL02-DBLM355:B1_LOSS',
'SARUN01-DBLM065:B1_LOSS',
'SARUN03-DBLM030:B1_LOSS',
'SARUN04-DBLM030:B1_LOSS',
'SARUN05-DBLM030:B1_LOSS',
'SARUN06-DBLM030:B1_LOSS',
'SARUN07-DBLM030:B1_LOSS',
'SARUN08-DBLM030:B1_LOSS',
'SARUN09-DBLM030:B1_LOSS',
'SARUN10-DBLM030:B1_LOSS',
'SARUN11-DBLM030:B1_LOSS',
'SARUN12-DBLM030:B1_LOSS',
'SARUN13-DBLM030:B1_LOSS',
'SARUN14-DBLM030:B1_LOSS',
'SARUN15-DBLM030:B1_LOSS',
'SARUN15-DBLM035:B1_LOSS',
'SARUN20-DBLM035:B1_LOSS',
'SATBD01-DBLM205:B1_LOSS',
'SATCB01-DBLM245:B1_LOSS',
'SATCL01-DBLM135:B1_LOSS',
'SATCL02-DBLM295:B1_LOSS',
'SATCL02-DBLM435:B1_LOSS',
'SATDI01-DBLM095:B1_LOSS',
'SATDI01-DBLM105:B1_LOSS',
'SATDI01-DBLM225:B1_LOSS',
'SATDI01-DBLM305:B1_LOSS',
'SATMA01-DBLM065:B1_LOSS',
'SATSY03-DBLM085:B1_LOSS',
'SATUN06-DBLM005:B1_LOSS',
'SATUN14-DBLM405:B1_LOSS',
'SATUN22-DBLM005:B1_LOSS',
'SINDI02-DBLM025:B1_LOSS',
'SINDI02-DBLM085:B1_LOSS',
'SINLH02-DBLM230:B1_LOSS',
'SINLH02-DBLM235:B1_LOSS'
]



beamOK = 'SIN-CVME-TIFGUN-EVR0:BEAMOK'

### Epics Channel containing all bs values + pulseID as last value
prefix = 'BS2EPICS:' #PV prefix
pvdb = {'filteredData':{'type': 'float',
                'count': len(bsChannels)+1,}
        }

class myDriver(Driver):
    def __init__(self):
        super(myDriver, self).__init__()
        
    def setNewData(self, data):
        self.setParam('filteredData', data)
        self.updatePVs()
        
server = SimpleServer()
server.createPV(prefix, pvdb)
driver = myDriver()

def filter_method(m):
    #return m.data.data[beamOK].value == 1
    return True

with source(channels=[*bsChannels, beamOK]) as stream:
    while True:
        message = stream.receive(filter=filter_method)
        pulseId = message.data.pulse_id
        data = [message.data.data[ch].value for ch in bsChannels]
        data.append(pulseId)
        print(pulseId)
        driver.setNewData(data)
        server.process(0.)