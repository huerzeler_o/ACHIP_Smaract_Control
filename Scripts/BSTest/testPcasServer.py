from pcaspy import Driver, SimpleServer
import numpy as np
from epics import caget

#channel to modify
epicsChannel = 'SARCL02-DBLM135:B1_LOSS'

### Epics Channel definition 
prefix = 'PCASPYTEST2:' #PV prefix
pvdb = {'data':{'type': 'float',
                'count': 2,
                'prec': 5,}
        }

class myDriver(Driver):
    def __init__(self):
        super(myDriver, self).__init__()
        
    def setNewData(self, data):
        
        self.setParam('data', data)
        self.updatePVs()
        
server = SimpleServer()
server.createPV(prefix, pvdb)
driver = myDriver()

def f(data):
    return 2.*data

while True:
    data = caget(epicsChannel)
    newData = [data, f(data)]
    driver.setNewData(newData)
    server.process(0.1)