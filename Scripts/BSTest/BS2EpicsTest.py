# -*- coding: utf-8 -*-
from epics import caget, caput, PV
from time import sleep, time
import matplotlib.pyplot as plt
import numpy as np

bs2epics = 'BS2EPICS:filteredData'
PV0 = PV(bs2epics)
# for comparision with epics direct
channel = 'SARCL02-DBLM135:B1_LOSS'

PV1 = PV(channel)

fig = plt.figure()
ax = fig.add_subplot(111)

epicsValue = []
bs2epicsValue = []

for i in range(1000):
    t0 = time()
    
    #epicsValue.append(PV1.get())
    bs2epicsValue.append(PV0.get())
    
    t1 = time()
    
    #print(epicsValue[-1])
    #print(bs2epicsValue[-1])
    #print('----------------')   
    sleep(abs(0.01-(t1-t0)))
    

PV0.disconnect()
PV1.disconnect()
    
bs2epicsValue = np.array(bs2epicsValue)

#ax.plot( epicsValue, label = 'epics direct', marker='+', ls='', ms=5)
ax.plot( bs2epicsValue[:,0], label = 'bs2epics', marker='+', ls='', ms=5)
ax2 = ax.twinx()
ax2.plot(bs2epicsValue[:,-1]-bs2epicsValue[:,-1][0])

ax.legend()

plt.show()
    