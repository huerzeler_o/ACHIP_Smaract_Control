# -*- coding: utf-8 -*-
from bsread import source
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as ani


sensors = ['SATSY03-DBLM085:LOSS_SIGNAL_RAW', 'SATCL01-DBLM135:LOSS_SIGNAL_RAW']
#sensors = ['SARUN04-DBLM030:LOSS_SIGNAL_RAW',]


colors = ['b', 'g']
saturationLimit = 100 # if signal is lower than this value, turn color red

beamOK = 'SIN-CVME-TIFGUN-EVR0:BUNCH-2-OK'
#beamOK = 'SIN-CVME-TIFGUN-EVR0:BUNCH-1-OK'

signalLength = 2048

debug = False

fig, ax = plt.subplots()
ax.set_title('BLM Raw Live Viewer')
ax.set_xlabel('index')
ax.set_ylabel('signal (arb. units)')
ax.axhline(saturationLimit, color = 'k')
signals = {}
for sen in sensors:
    signals[sen] = ax.plot(np.arange(signalLength),np.zeros(signalLength), ls = '-', marker = '.', label = sen)[0]

ax.set_xlim(0, signalLength)
ax.set_ylim(0, 2200)

def filter_method(m):
    if debug:
        return True
    else:
#        return m.data.data[beamOK].value == 1
        return m.data.pulse_id % 100 == 99




with source(channels=[*sensors, beamOK]) as stream:
    def update(i):

        stream.connect()        
        message = stream.receive(filter=filter_method)
        
        pulseId = message.data.pulse_id
        ax.set_title('Pulse ID: '+str(pulseId))
        
        for sen in sensors:
            data = np.array(message.data.data[sen].value)
            signals[sen].set_ydata(data)
            if min(data)<saturationLimit: 
                col = 'r'                
            else:
                col = colors[sensors.index(sen)]
            signals[sen].set_color(col)
        ax.legend(loc=3)
        
    ani = ani.FuncAnimation(fig, update, interval=500, repeat=True) 

    plt.show()
