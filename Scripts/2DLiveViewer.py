# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as ani
from epics import caget, caput
import h5py as h5

fig, ax = plt.subplots(figsize=(12,8))
ax.set_title('2D Scan Live Viewer')
ax.set_xlabel('x (um)')
ax.set_ylabel('y (um)')
ax.patch.set_facecolor('0.3')

sc = ax.scatter([],[], alpha = 1, edgecolors=None, linewidth='0')
#hexapodPosition, = ax.plot([],[], ls = '', marker = 'h', color='g')
hexapodPositionX = ax.axvline(x=27000, ls = '--', color='1', alpha=0.8, lw=0.5)
hexapodPositionY = ax.axhline(0, ls = '--', color='1', alpha=0.8, lw=0.5)

cmap = plt.get_cmap('gnuplot')

sm = plt.cm.ScalarMappable(cmap=cmap, norm=plt.Normalize(vmin=0, vmax=1))
sm._A = []
plt.colorbar(sm)

autoScale = True

def onclick(event):
    global autoScale
    if event.button==2 or event.button==3:
        print('%s click: button=%d, x=%d, y=%d, xdata=%f, ydata=%f' %
              ('double' if event.dblclick else 'single', event.button,
               event.x, event.y, event.xdata, event.ydata))
    if event.button==2:
        print('set position')
        caput('SATSY03-DLAC080-DHXP:absposX' , event.xdata)
        caput('SATSY03-DLAC080-DHXP:absposY', event.ydata)
        caput('SATSY03-DLAC080-DHXP:hSet6d',  [1,1,1,1,1,1])
    if event.button==3:
        print('switch auto scale to ', not(autoScale))
        autoScale = not(autoScale)


cid = fig.canvas.mpl_connect('button_press_event', onclick)

def generateRandom():
    n=1000
    x = np.random.rand(n)
    y = np.random.rand(n)
    c = np.random.rand(n)
    return removeNan(x, y, c)

def readSavedData():
#    fileName = '/afs/psi.ch/intranet/SF/Diagnostics/ACHIP/Data/20190615/hole S4_20190615_111832/LiveData.h5'
#    fileName = '/afs/psi.ch/intranet/SF/Diagnostics/ACHIP/Data/20190615/S3 scan upper edge_20190615_130111/LiveData.h5'
    fileName = 'D:\\Measurement Data\\20190615_ATHOS\\hole S3_20190615_121945\\LiveData.h5'
    f = h5.File(fileName, 'r')
    x = np.array(f['PositionHEX/x'])
    y = np.array(f['PositionHEX/y'])
    c = np.array(f['SATSY03-DLAC080-DHXP:BLM1_comp'])
    return removeNan(x, y, c)    
    
def iocStream():
    x = np.array(caget('SATSY03-DLAC080-DHXP:posX_comp'))
    y = np.array(caget('SATSY03-DLAC080-DHXP:posY_comp'))
    c = np.array(caget('SATSY03-DLAC080-DHXP:BLM1_comp'))
    return removeNan(x, y, c)

def removeNan(x, y, c):
    isNan = np.logical_and(np.logical_and(np.isnan(x), np.isnan(y)), np.isnan(c))
    idx = np.argwhere(np.logical_not(isNan)).flatten()
    return x[idx], y[idx], c[idx] 


dataStream = iocStream

def animate(i):
    global autoScale
    x,y,c = dataStream()
    
    
    xHexapod = caget('SATSY03-DLAC080-DHXP:posX')
    yHexapod = caget('SATSY03-DLAC080-DHXP:posY')   
    print(xHexapod)
#    xHexapod = []
#    yHexapod = []
#    hexapodPosition.set_data(xHexapod, yHexapod)
    hexapodPositionX.set_data([xHexapod, xHexapod], [0,1])
    hexapodPositionY.set_data([0,1], [yHexapod, yHexapod])
#    ax.axvline(x=xHexapod, color='g')
    if autoScale:
        minx = np.nanmin(x)
        maxx = np.nanmax(x)
        miny = np.nanmin(y)
        maxy = np.nanmax(y)
        ax.set_xlim(minx-(maxx-minx)*0.1,maxx+(maxx-minx)*0.1)
        ax.set_ylim(miny-(maxy-miny)*0.1,maxy+(maxy-miny)*0.1)
        ax.invert_yaxis()
       
    else:
        minx, maxx = ax.get_xlim()
        maxy, miny = ax.get_ylim() # y axis inverted x lim is still lower, upper !!!
        showIdx = np.argwhere(np.logical_and(np.logical_and(x>minx, x<maxx), np.logical_and(y>miny, y<maxy))).flatten()
        x = x[showIdx]
        y = y[showIdx]
        c = c[showIdx]
        
    
    sc.set_offsets(np.c_[x,y])
    sc.set_facecolors(cmap((c-np.nanmin(c))/(np.nanmax(c)-np.nanmin(c))))    

ani = ani.FuncAnimation(fig, animate, frames=2, interval=200, repeat=True) 
#animate(0)

plt.show()
        
