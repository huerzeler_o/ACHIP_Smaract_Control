from epics import caget, caput
import numpy as np
import h5py
from time import sleep

fileNameRef = 'lattice/LHtoAT_20201105.lat'
#fileName = 'lattice/20200117_Wake_betax0.013betay0.039/altered_lattice.lat'
#fileName = 'lattice/SpectrometerTransport_betax0.015betay0.022/altered_lattice.lat'
#fileName = 'lattice/SmallFocusGoodTransport20200528/altered_lattice.lat'
fileName = 'lattice/202011Wake_betax0.035betay0.065/altered_lattice.lat'

scalingFactor = 1.03
'''ATTENTION S20 Invasive'''

quads = ['S20SY01-MQUA020', 'S20SY01-MQUA030', 'S20SY01-MQUA050', 'S20SY01-MQUA080',
         'S20SY02-MQUA070', 'S20SY02-MQUA100', 'S20SY02-MQUA140', 'S20SY02-MQUA180',
         'SATSY02-MQUA010', 'SATSY02-MQUA110', 'SATSY02-MQUA120', 'SATSY02-MQUA230', 
         'SATSY03-MQUA010', 'SATSY03-MQUA040', 'SATSY03-MQUA070', 'SATSY03-MQUA100', 'SATSY03-MQUA130',        
        'SATCL01-MQUA120', 'SATCL01-MQUA130', 'SATCL01-MQUA180', 'SATCL01-MQUA190',        
        'SATDI01-MQUA040', 'SATDI01-MQUA050', 'SATDI01-MQUA220', 'SATDI01-MQUA230',
        'SATDI01-MQUA250', 'SATDI01-MQUA260', 'SATDI01-MQUA280', 'SATDI01-MQUA300',
        'SATCB01-MQUA230', 'SATCB01-MQUA430', 'SATCL02-MQUA430', 'SATMA01-MQUA050',
        ]

def getKdict(fileName):
    d = {}
    lattice = open(fileName, 'r').read().split('\n')
    for q in quads:
        found = False
        for l in lattice:
            if (q.replace('-', '.') in l) and ('K1' in l):
                k = float(l.split('K1=')[1].split(',')[0])
                d[q]=k
#                print(q)
                found = True

        if not found: 
            print(q, ' not found, set 0')
            d[q]=0
    return d

def findDifferentQuads(fileNameRef, fileName):
    kDictRef = getKdict(fileNameRef)
    kDict = getKdict(fileName)  
    setQ = []
    print('quad  old K  new K')
    for q in quads:
        print(q, kDictRef[q], kDict[q])
        if abs(kDict[q] - kDictRef[q]) > 1e-3:
            
            setQ.append(q)
    return setQ

setQuadNames = findDifferentQuads(fileNameRef, fileName)   
## manual quad selection
'''
setQuadNames = [
        'S20SY01-MQUA020', 'S20SY01-MQUA030', 'S20SY01-MQUA050', 'S20SY01-MQUA080',
                
#        'SATSY02-MQUA010', 'SATSY02-MQUA110', 'SATSY02-MQUA120',  #dispersive
        'SATSY02-MQUA230', 
        'SATSY03-MQUA010', 'SATSY03-MQUA040', 'SATSY03-MQUA070', 'SATSY03-MQUA100', 'SATSY03-MQUA130', 
        
        'SATCL01-MQUA120', 'SATCL01-MQUA130', 'SATCL01-MQUA180', 'SATCL01-MQUA190', #dispersive
        'SATDI01-MQUA040', 'SATDI01-MQUA050', 'SATDI01-MQUA220', 'SATDI01-MQUA230'
        ]
'''

k2i = 1./0.232457039702

k2i_S20SY02 = 1./0.0254439517581

lattice = open(fileName, 'r').read().split('\n')



print('SETTING QUAD CURRENTS!')
#sleep(3)
for setQuad in setQuadNames:
    for l in lattice:
        if (setQuad.replace('-', '.') in l) and ('Q1' in l) and ('K1' in l):
            k = float(l.split('K1=')[1])
            i = k*k2i
            if 'S20SY02' in setQuad:
                i=k*k2i_S20SY02
                
#            if i > 10: i = 10
#            if i < -10: i = -10
            i*=scalingFactor
            print(setQuad, i)
            caput(setQuad+':I-SET', i)
