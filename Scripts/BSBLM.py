from bsread import source
from epics import caget, caput
from time import sleep, time
import numpy as np
import matplotlib.pyplot as plt

bunchIndex = '2'

#blm = 'SATSY03-DBLM085:B'+bunchIndex+'_LOSS'
blm = 'SATCL01-DBLM135:B'+bunchIndex+'_LOSS'
beamOK = 'SIN-CVME-TIFGUN-EVR0:BUNCH-'+bunchIndex+'-OK'
lossREC = 'SATSY03-DLAC080-DHXP:BLM1'
pulseIDREC = 'SATSY03-DLAC080-DHXP:pulseID'

blmRawPV = 'SATCL01-DBLM135:LOSS_SIGNAL_RAW'
blmBasePV = 'SATCL01-DBLM328:AL1-BASELINE-VAL' 


def filter_method(m):
    conditions = []
#    conditions.append(m.data.data[beamOK].value == 1)
    conditions.append(m.data.pulse_id % 100 == 99)
    
    return all(conditions)

def calcLoss(raw):
    base=float(caget(blmBasePV))
#    plt.plot(raw-base)
    return -np.sum(raw[505:528]-base)

with source(channels=[beamOK, blm, blmRawPV]) as stream:
    while True:
        
#        stream.connect()   
        message = stream.receive(filter=filter_method)
        #stream.disconnect()
        blmValue = message.data.data[blm].value
#        blmRawSignal = np.array(message.data.data[blmRawPV].value)
#        blmSelfCalc = calcLoss(blmRawSignal)/1000
        
        pulseId = message.data.pulse_id
                    
        #write blmValue to epics waveform
        try:
            caput(lossREC, blmValue)
#            caput(lossREC, blmSelfCalc)
            caput(pulseIDREC, pulseId)
        except:            
#            print(blm, 'incorrectly grabbed.')
            pass

        print(pulseId, blmValue)
        sleep(0.001)
        
