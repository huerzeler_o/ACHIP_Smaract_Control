# -*- coding: utf-8 -*-

import data_api as api
import h5py as h5
import numpy as np
import os

path = '/afs/psi.ch/intranet/SF/Diagnostics/ACHIP/Data/20210222/'
    
fileNames = [
             'ws1_20210222_044639/',
                     ]


for i, fileName in enumerate(fileNames):
#fileName= fileNames20200826[-1]

    f = h5.File(path+fileName+'RAWScanData.h5', 'r')
    pulseId = np.array(f['SensorValue/pulseId'].value)
#    start_pulse_id = int(np.min(pulseId))+99
#    stop_pulse_id = int(np.max(pulseId))+99
    
    fArchiver = np.loadtxt(path+fileName+'data.csv', skiprows=1, dtype=str, delimiter=';')
#    data = api.get_data(channels=channels, start=start_pulse_id, end=stop_pulse_id, range_type="pulseId", delta_range =1)

#    pulseID = np.arange(start_pulse_id, stop_pulse_id, 100, dtype=int)
    blm=[]
    j=0
    for i in range(len(fArchiver[:,0])):
        if pulseId[j]+99==float(fArchiver[i,3][2:-1]):
            print(pulseId[j])
            j=j+1
            try:
                blm.append(float(fArchiver[i,2][2:-1]))
            except:
                blm.append(float(fArchiver[i,2].split('max:')[-1][:-2]))
            if j==len(pulseId):
                break
            

    blm=np.array(blm)
    
    fd = h5.File(path+fileName+'ModScanData.h5', 'a')
    fd['SensorValue/pulseId'][...] = pulseId+99
    fd['SensorValue/SATCL01-DBLM135:B2_LOSS'][...] = blm
    f.close()
    fd.close()
#    api.get_data()