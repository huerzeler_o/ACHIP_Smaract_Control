# -*- coding: utf-8 -*-

from epics import caput

caput('SATSY03-CPPS-01-CH1:POWERONOFF-DESC', 'ACHIP IOC sf-achipi02'.encode())
caput('SATSY03-CPPS-01-CH2:POWERONOFF-DESC', 'THz Detector'.encode())
caput('SATSY03-CPPS-01-CH3:POWERONOFF-DESC', 'LED Lamp'.encode())
caput('SATSY03-CPPS-01-CH4:POWERONOFF-DESC', 'Alignment Laser'.encode())
caput('SATSY03-CPPS-01-CH5:POWERONOFF-DESC', 'YAG Camera'.encode())
caput('SATSY03-CPPS-01-CH6:POWERONOFF-DESC', 'Amplifier for THz Det.'.encode())

caput('SATSY03-CPPS-02-CH1:POWERONOFF-DESC', 'Smaract Controller'.encode())
caput('SATSY03-CPPS-02-CH2:POWERONOFF-DESC', 'ACHIP MPS sf-achipmps01'.encode())
caput('SATSY03-CPPS-02-CH3:POWERONOFF-DESC', 'USB-Network Adapter'.encode())
caput('SATSY03-CPPS-02-CH4:POWERONOFF-DESC', 'Zoom Lens Controller'.encode())
caput('SATSY03-CPPS-02-CH5:POWERONOFF-DESC', 'Thorlabs Controler Microscope Movers'.encode())
caput('SATSY03-CPPS-02-CH8:POWERONOFF-DESC', 'ACHIP Beckhoff Controller, Quad Movers'.encode())


