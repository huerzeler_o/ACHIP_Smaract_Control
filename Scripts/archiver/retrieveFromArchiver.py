# -*- coding: utf-8 -*-
import data_api as api
import h5py as h5
import numpy as np
import os

path = '/afs/psi.ch/intranet/SF/Diagnostics/ACHIP/Data/20200826/'
    
fileNames = [
             'ws_20200826_052518',
             'ws_z4758_20200826_054008',
             'ws_z9758_20200826_055125',
             'ws_z19758_20200826_060107',
             'ws_z39758_20200826_062837',
             'ws_z79758_20200826_063944',
                     ]

fileNames20200826 = [path+f+'/RAWScanData.h5' for f in fileNames]

bpmList = ['SATSY01-DBPM010', 'SATSY01-DBPM060', 'SATSY01-DBPM100', 'SATSY01-DBPM240', 'SATSY01-DBPM290',
           'SATSY02-DBPM020', 'SATSY02-DBPM210',
           'SATSY03-DBPM030', 'SATSY03-DBPM060', 'SATSY03-DBPM090', 'SATSY03-DBPM120', 
           'SATCL01-DBPM140', 
           'SATDI01-DBPM030', 'SATDI01-DBPM060', 'SATDI01-DBPM210', 'SATDI01-DBPM240', 'SATDI01-DBPM270', 'SATDI01-DBPM310',
           'SATCB01-DBPM220', 'SATCB01-DBPM420', 
           'SATCL02-DBPM420']

bpmList2 = ['S10BC02-DBPM140', 'S10BC02-DBPM320']

blmList = ['SATSY03-DBLM085', 'SATCL01-DBLM135', 'SATDI01-DBLM095', 'SATDI01-DBLM105', 'SATDI01-DBLM225']

channels = []
for b in bpmList:
    channels.append(b+':X2')
    channels.append(b+':Y2')
    channels.append(b+':Q2')
    
for b in bpmList2:
    channels.append(b+':X1')
    channels.append(b+':Y1')
    channels.append(b+':Q1')
    channels.append(b+':X2')
    channels.append(b+':Y2')
    channels.append(b+':Q2')

for b in blmList:
    channels.append(b+':B2_LOSS')
    


for i, fileName in enumerate(fileNames20200826):
#fileName= fileNames20200826[-1]

    f = h5.File(fileName, 'r')
    pulseId = np.array(f['SensorValue/pulseId'].value)
    start_pulse_id = int(np.min(pulseId))
    stop_pulse_id = int(np.max(pulseId))
        
    data = api.get_data(channels=channels, start=start_pulse_id, end=stop_pulse_id, range_type="pulseId", delta_range =100)

    if not os.path.exists(fileNames[i]):
        os.makedirs(fileNames[i])
    else:
        try:
            os.remove(fileNames[i]+'/archiver.h5')
        except:
            pass
    data.to_hdf(fileNames[i]+'/archiver.h5', 'root')
