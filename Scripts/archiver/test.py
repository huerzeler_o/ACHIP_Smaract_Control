import numpy as np
import data_api as api
import datetime
import json
import matplotlib.pyplot as plt

def mf(data, index_field):
    new_data = {}
    for channel in data:
        new_data[channel['channel']['name']] = channel['data']
    return new_data

now = datetime.datetime.now()
time_stop = now-datetime.timedelta(seconds=10)
time_start = time_stop - datetime.timedelta(seconds=100)

my_channels = []

channels = [   
        {"backend":"sf-databuffer",
         "channel":"SATSY03-DBLM085:B2_LOSS"}, 
        {"backend":"sf-databuffer",
         "channel":"SATSY03-DBLM085:B1_LOSS"}, 
]

for i in channels:
    ch = i['backend'] + '/' + i['channel']
    my_channels.append(ch)

data = api.get_data(channels=my_channels,
                    start=time_start,
                    end=time_stop,
                    mapping_function=mf)

fig = plt.figure(1)
ax = fig.add_subplot(111)

for i in channels:
    #print(data['SATSY03-DBLM085:B2_LOSS'])
    d = data[i["channel"]]
    pulseId = [d[j]["pulseId"] for j in range(len(d))]
    value = [d[j]["value"] for j in range(len(d))]
    print(max(pulseId)-min(pulseId), len(value))
    ax.plot(pulseId, value)

plt.show()
