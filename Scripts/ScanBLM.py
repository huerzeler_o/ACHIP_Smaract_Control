import sys
import os
import h5py
import matplotlib.pyplot as plt
import numpy as np
from epics import caget, caput
from bsread import source
import datetime
import time
from time import sleep, time
from shutil import copyfile

bunchIndex = '2'
moveOnly = False # False: stop and go, True: constant velocity with data retrieval after scan (to be tested).
activateBLMWS = False # automatic start stop of WS mode for scans
blmStart = ['SATCL01-DBLM328:AL2-WS-RUN-F_1.PROC', 'SATCL01-DBLM328:AL1-WS-RUN-F_1.PROC']
blmStop = ['SATCL01-DBLM328:AL2-WS-STOP-SEQ.PROC', 'SATCL01-DBLM328:AL1-WS-STOP-SEQ.PROC']

scanName = caget('SATSY03-DLAC080-DHXP:scanName') + '_' + str(datetime.datetime.now().strftime("%Y%m%d_%H%M%S"))
directory = '/afs/psi.ch/intranet/SF/Diagnostics/ACHIP/Data/' + scanName
fileName = directory + '/RAWScanData' + '.h5'

if not os.path.exists(directory):
    os.makedirs(directory)

scanPreparationFile = '/afs/psi.ch/intranet/SF/Diagnostics/ACHIP/ACHIP_Smaract_Control/Scripts/scanPreparation.h5'
newScanPreparationFile = directory + '/scanPreparation.h5'
copyfile(scanPreparationFile, newScanPreparationFile)

scanPointsPlot = '/afs/psi.ch/intranet/SF/Diagnostics/ACHIP/ACHIP_Smaract_Control/UI/temp_ScanPoints.png'
newScanPointsPlot = directory + '/ScanPoints.png'
copyfile(scanPointsPlot, newScanPointsPlot)
                                              # array of scan points
nSteps = caget('SATSY03-DLAC080-DHXP:numberOfSteps')       
coordinates = ['x','y','z','rx','ry','rz']                  
IOC = 'SATSY03-DLAC080-DHXP:'
cPV = ['absposX', 'absposY', 'absposZ', 'absposRX', 'absposRY', 'absposRZ']
pPV = ['posX', 'posY', 'posZ', 'posRX', 'posRY', 'posRZ']

beamOK = 'SIN-CVME-TIFGUN-EVR0:BUNCH-'+bunchIndex+'-OK'

### BS sensors
sensors = ['SATSY03-DBLM085:B'+bunchIndex+'_LOSS', 'SATSY03-DBLM085:LOSS_SIGNAL_RAW', 
           'SATCL01-DBLM135:B'+bunchIndex+'_LOSS', 'SATCL01-DBLM135:LOSS_SIGNAL_RAW',
           'SATSY03-DBPM060:Q'+bunchIndex, 'SATSY03-DBPM090:Q'+bunchIndex, 
           'SATSY03-DBPM060:X'+bunchIndex, 'SATSY03-DBPM060:Y'+bunchIndex,
           'SATSY03-DBPM090:X'+bunchIndex, 'SATSY03-DBPM090:Y'+bunchIndex
]
sensors = ['SATSY03-DBLM085:B'+bunchIndex+'_LOSS', 
           'SATCL01-DBLM135:B'+bunchIndex+'_LOSS', 
]

plotSensors = ['SATSY03-DBLM085:B'+bunchIndex+'_LOSS']

### Epics Channels, read once before Scan, e.g. quad
EpicsChannels = ['SATSY03-MQUA010:I-SET', 'SATSY03-MCRX010:I-SET', 'SATSY03-MCRY010:I-SET',
                 'SATSY03-MQUA040:I-SET', 'SATSY03-MCRX040:I-SET', 'SATSY03-MCRY040:I-SET',
                 'SATSY03-MQUA070:I-SET', 'SATSY03-MCRX070:I-SET', 'SATSY03-MCRY070:I-SET',
                 'SATCL01-DBLM328:AL1-WS-GAIN-TOTAL', 'SATCL01-DBLM328:AL2-WS-GAIN-TOTAL']

debug = False # set False for measurement, True takes every 10th BLM value. 
useMotor = True

currentPosition = np.zeros(6)
XposHEX = []
YposHEX = []
ZposHEX = []
RXposHEX = []
RYposHEX = []
RZposHEX = []
velHEX = []
accHEX = []
freqHEX = []
coordinateSign =[]
sensorValues = {}

for sen in sensors:
    sensorValues[sen]=[]
    
EpicsSensorValues = {}
for sen in EpicsChannels:
    EpicsSensorValues[sen]=[]

pulseIdRec = []
tsRec = []
array = []

# open the .h5 file and read every listed point and saves it into Points[] array
f = h5py.File(scanPreparationFile, 'r')
Points = []   
for i in range(len(f['Points'])):
    element = 'Points/P'+str(i)
    Points.append(f[element].value)
nScans = int(len(Points)/2)

delayTime = 0.1 # caget
nShots = caget('SATSY03-DLAC080-DHXP:numberShots')
caput('SATSY03-DLAC080-DHXP:scanProgress', 0)

nTotalShots = nScans*(nSteps+1)*nShots
nTotalShotsMeasured = 0
progress = 0.

def filter_method(m):
    if debug:
        return m.data.pulse_id % 100 == 0
    else:
        return m.data.pulse_id % 100 == 99
#        return m.data.data[beamOK].value == 1

# Reads out the actual data once the hexapod has stopped
def readData(stream):
    
    global nTotalShots
    global nTotalShotsMeasured
    global progress
    
    #try:
    nShotsMeasured = 0
    
    while nShotsMeasured < nShots: 
        stream.connect()
        message = stream.receive(filter=filter_method)
             
        pulseId = message.data.pulse_id
        pulseIdRec.append(pulseId)     
        tsRec.append(message.data.global_timestamp)
        for sen in sensors:
            if message.data.data[sen].value is not None:
                sensorValues[sen].append(message.data.data[sen].value)
            else:
                sensorValues[sen].append(np.nan)
        
        XposHEX.append(caget('SATSY03-DLAC080-DHXP:posX'))
        YposHEX.append(caget('SATSY03-DLAC080-DHXP:posY'))
        ZposHEX.append(caget('SATSY03-DLAC080-DHXP:posZ'))
        RXposHEX.append(caget('SATSY03-DLAC080-DHXP:posRX'))
        RYposHEX.append(caget('SATSY03-DLAC080-DHXP:posRY'))
        RZposHEX.append(caget('SATSY03-DLAC080-DHXP:posRZ'))

        velHEX.append(caget('SATSY03-DLAC080-DHXP:velHEX'))
        freqHEX.append(caget('SATSY03-DLAC080-DHXP:freqHEX'))
        accHEX.append(caget('SATSY03-DLAC080-DHXP:accHEX'))
        nShotsMeasured += 1
        nTotalShotsMeasured += 1
        progress = nTotalShotsMeasured/nTotalShots*100
        caput('SATSY03-DLAC080-DHXP:scanProgress', progress)
        print('shot: '+str(nShotsMeasured)+'/'+str(nShots))
        for sen in sensors:
            print(sen+str(sensorValues[sen][-1]))
        print('overall progress: '+str(progress)+' %')
        
    return True
      
#    except (RuntimeError, TypeError, NameError):
#        print('Failed data readout...')
#        return False



# saves the data arrays into a .h5 file
def writeFile():
    with h5py.File(fileName, 'a') as hdf:
        hdf.create_dataset('PositionHEX/x', data = np.array(XposHEX, dtype = float))
        hdf.create_dataset('PositionHEX/y', data = np.array(YposHEX, dtype = float))
        hdf.create_dataset('PositionHEX/z', data = np.array(ZposHEX, dtype = float))
        hdf.create_dataset('PositionHEX/rx', data = np.array(RXposHEX, dtype = float))
        hdf.create_dataset('PositionHEX/ry', data = np.array(RYposHEX, dtype = float))
        hdf.create_dataset('PositionHEX/rz', data = np.array(RZposHEX, dtype = float))
        
        if moveOnly:
            hdf.create_dataset('SensorValue/'+lossRecPV, data = np.array(sensorValues[lossRecPV], dtype = float))
        else:
            for sen in sensors:
                hdf.create_dataset('SensorValue/'+sen, data = np.array(sensorValues[sen], dtype = float))
        
        for sen in EpicsChannels:
            hdf.create_dataset('SensorValue/'+sen, data = np.array(EpicsSensorValues[sen], dtype = float))
        
        hdf.create_dataset('SensorValue/pulseId', data = np.array(pulseIdRec, dtype = float))
        hdf.create_dataset('timeStamp', data = np.array(tsRec, dtype = float))
      
        
        hdf.create_dataset('SettingsHEX/velocity', data = np.array(velHEX, dtype = float))
        hdf.create_dataset('SettingsHEX/acceleration', data = np.array(accHEX, dtype = float))
        hdf.create_dataset('SettingsHEX/frequency', data = np.array(freqHEX, dtype = float))
        hdf.create_dataset('SettingsHEX/coordinateSign', data = np.array(coordinateSign, dtype = float))
        

def positionReached(point):
    global currentPosition

    currentPosition[0] = caget('SATSY03-DLAC080-DHXP:posX')
    currentPosition[1] = caget('SATSY03-DLAC080-DHXP:posY')
    currentPosition[2] = caget('SATSY03-DLAC080-DHXP:posZ')
    currentPosition[3] = caget('SATSY03-DLAC080-DHXP:posRX')
    currentPosition[4] = caget('SATSY03-DLAC080-DHXP:posRY')
    currentPosition[5] = caget('SATSY03-DLAC080-DHXP:posRZ')

    currentPosition = np.array(currentPosition)

    # make a delta for x,y,z and another one with rx,ry,rz
    deltaPos = np.abs(np.array(point[0:3]) - currentPosition[0:3])
    deltaAng = np.abs(np.array(point[3:7]) - currentPosition[3:7])

    if np.logical_and(np.all(deltaPos <= 0.03), np.all(deltaAng <= 0.01)):
        print('position reached, Delta Position is: ' + str(deltaPos))
        
        return True
    else:
        if not moveOnly:
            print('position not reached')
            print('Goal is:             ' + str(point))
            print('Actual position:     ' + str(currentPosition))
            print('Delta Position is:   ' + str(deltaPos))
            print('Delta Angle is:      ' + str(deltaAng))
        return False

# charges the calculated values into the absolute position and move
def loadPoint(point, sleepTime=0.1): 
    if useMotor:
        for c, a in zip(coordinates, cPV):
            caput(IOC+a, point[coordinates.index(c)])
        caput('SATSY03-DLAC080-DHXP:hSet6d', [1,1,1,1,1,1])
        while not positionReached(point):
            sleep(sleepTime)
    else:
        pass

# function to scan from pointA (format: [x,y,z,rx,ry,rz]) to pointB
def scan(pointA, pointB, stream):
    for sen in EpicsChannels:
        val=caget(sen)
        EpicsSensorValues[sen].append(val)
    delta = (pointB - pointA)/nSteps
    for i in range(nSteps+1):
        print('---------------------------------------------------------------')
        print('step: '+str(i)+'/'+str(nSteps))
        
        if not caget('SATSY03-DLAC080-DHXP:abortScan'):
            loadPoint(pointA+i*delta)
            sleep(delayTime)
            readData(stream)

def scanContinuous(pointA, pointB):
    frequency = caget('SIN-TIMAST-TMA:Bunch-'+bunchIndex+'-Exp-Freq-RB')
#    frequency = 2
    delta = (pointB - pointA)/nSteps
    deltaT = nShots/frequency
    t0 = time()
    i=0
    while i<=nSteps: 
        loadPoint(pointA+i*delta, sleepTime=0.01)
        runTime = time()-t0
        sleepTime = (1+i)*deltaT-runTime
        sleep(max(0, sleepTime))
        i+=max(1, int(2-sleepTime/deltaT)) # if sleeptime is negative, advance i faster.
        
def plotData(outputFile):
    file = h5py.File(fileName, 'r')
    coordinates = ['x', 'y', 'z', 'rx', 'ry', 'rz']
    units = ['um','um','um', 'deg', 'deg', 'deg']
    
    fig = plt.figure(outputFile, figsize = (18,7), dpi = 100)
    ax0 = plt.subplot2grid((2, 5), (0, 0), colspan=2, rowspan=2)
    ax1 = plt.subplot2grid((2, 5), (0, 2))
    ax2 = plt.subplot2grid((2, 5), (0, 3))
    ax3 = plt.subplot2grid((2, 5), (0, 4))
    ax4 = plt.subplot2grid((2, 5), (1, 2))
    ax5 = plt.subplot2grid((2, 5), (1, 3))
    ax6 = plt.subplot2grid((2, 5), (1, 4))

    axes = [ax1, ax2, ax3, ax4, ax5, ax6]

    for c, ax, unit in zip(coordinates, axes, units):
        xData = np.array(file['PositionHEX/'+c].value)
        
        for s in plotSensors:
            data = np.array(file['SensorValue/'+s].value)
            if data.ndim==1:
                ax.plot(xData, data, label = s, ls = '', marker = '+', ms = 5)
        
        ax.set_xlabel(c+' ('+unit+')')
        ax.set_ylabel('Loss')
        ax.ticklabel_format(style='sci', scilimits = (-2,2), axis='both')

    x = np.array(file['PositionHEX/x'].value)
    y = np.array(file['PositionHEX/y'].value)
    z = np.array(file['PositionHEX/z'].value)
    p = np.array([x-x[0],y-y[0],z-z[0]])
    xData = np.sqrt(np.sum(p**2, axis=0))

    for s in plotSensors:
        data = np.array(file['SensorValue/'+s].value)
        if data.ndim==1:
            ax0.plot(xData, data, label = s, ls = '', marker = '+', ms = 5)

    ax0.set_xlabel('s (um)')
    ax0.set_ylabel('Loss')
    ax0.ticklabel_format(style='sci', scilimits = (-2,2), axis='both')

    handles, labels = axes[0].get_legend_handles_labels()
    fig.legend(handles, labels, loc = 'center left')

    fig.tight_layout()
    fig.savefig(directory + '/DataPlot.png')
   
if moveOnly:
    os.system('python /afs/psi.ch/intranet/SF/Diagnostics/ACHIP/ACHIP_Smaract_Control/UI/reset.py')
    i=0
    while i < len(Points):
        print('---------------------------------------------------------------')
        print('---------------------------------------------------------------')
        print('scan: '+str(int(i/2+1))+'/'+str(int(nScans)))
        
        if activateBLMWS:
            for blm in blmStart:
                caput(blm, 1)
                sleep(0.02)
        
        scanContinuous(Points[(i)], Points[(i+1)])
        
        if activateBLMWS:
            for blm in blmStop:
                caput(blm, 1)
                sleep(0.02)
        
        i = i + 2
else:
    i=0
    with source(channels=[*sensors, beamOK]) as stream:
        while i < len(Points):
            print('---------------------------------------------------------------')
            print('---------------------------------------------------------------')
            print('scan: '+str(int(i/2+1))+'/'+str(int(nScans)))
            
            if activateBLMWS:
                for blm in blmStart:
                    caput(blm, 1)
                    sleep(0.02)
            
            scan(Points[(i)], Points[(i+1)], stream)
            
            if activateBLMWS:
                for blm in blmStop:
                    caput(blm, 1)
                    sleep(0.02)
            
            i = i + 2
        

caput('SATSY03-DLAC080-DHXP:abortScan', 0)
caput('SATSY03-DLAC080-DHXP:scanProgress', 0)
if moveOnly:
    os.system('python /afs/psi.ch/intranet/SF/Diagnostics/ACHIP/ACHIP_Smaract_Control/UI/savePreview.py')
else:
    # save the file and keep the path
    writeFile()
    
    # plot the measured data....
    plotData(fileName)

print('Script ended successfully.')
sys.exit(0)
