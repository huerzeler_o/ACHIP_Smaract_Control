import sys
import os
import h5py
import matplotlib.pyplot as plt
import numpy as np
from epics import caget, caput
from bsread import source
import datetime
import time
from time import sleep
#from shutil import copyfile

######################################  

#Define scan parameters below

######################################

directory = '/afs/psi.ch/intranet/SF/Diagnostics/ACHIP/Data/InjectorWake/'
scanName = 'MCRX30_0.40_precise' + '_' + str(datetime.datetime.now().strftime("%Y%m%d_%H%M%S"))

start = 133.5
end = 133.7                        
points = [start, end]  

nSteps = 100 # number of steps from start to end. (number of Measurement positions: nSteps+1)
nShots = 1 # number of shots for each position

delayTime = 0.5 # wait time after position reached in each step
useMotor = True # True for measurement
debug = False # False for Measurement, takes beamOK=True



if not os.path.exists(directory):
    os.makedirs(directory)

fileName = directory + scanName + '.h5'

######################################  

#Define PVs to set and record below

######################################

setPosPV = 'SINDI02-DLAC055:MOTOR_Y1'
getPosPV = 'SINDI02-DLAC055:MOTOR_Y1.RBV'

beamOK = 'SIN-CVME-TIFGUN-EVR0:BEAMOK'
### BS sensors
sensors = ['SINDI02-DBLM025:B1_LOSS', 'SINDI02-DBLM085:B1_LOSS', 'SINDI02-DBLM085:LOSS_SIGNAL_RAW',
           'SINDI02-DBPM040:Q1', 'SINDI02-DBPM040:X1', 'SINDI02-DBPM040:Y1',
           'SINDI02-DBPM080:Q1', 'SINDI02-DBPM080:X1', 'SINDI02-DBPM080:Y1']

plotSensors = ['SINDI02-DBLM085:B1_LOSS', 'SINDI02-DBLM085:LOSS_SIGNAL_RAW']

### Epics Channels, read once before Scan, e.g. quad
EpicsChannels = ['SINDI02-MQUA050:I-SET',
                 'SINDI02-MCRX050:I-SET',
                 'SINDI02-MCRY050:I-SET',
                 'SINDI02-MQUA060:I-SET', 
                 'SINDI02-MCRX060:I-SET',
                 'SINDI02-MCRY060:I-SET',
                 'SINDI02-DBLM084:AL2-WS-GAIN-TOTAL']

posReadRec = []
posSetRec = []
sensorValues = {}
for sen in sensors:
    sensorValues[sen]=[]

EpicsSensorValues = {}
for sen in EpicsChannels:
    EpicsSensorValues[sen]=[]
    
pulseIdRec = []
tsRec = []

def filter_method(m):
    if debug:
        return True
    else:
        return m.data.data[beamOK].value == 1
# Reads out the actual data once the stage has stopped
def readData(stream):
    
    global nTotalShots
    global nTotalShotsMeasured
    global progress
    
    
    nShotsMeasured = 0
    
    while nShotsMeasured < nShots:    
        message = stream.receive(filter=filter_method)
      
        pulseId = message.data.pulse_id
        pulseIdRec.append(pulseId)     
        tsRec.append(message.data.data[beamOK].timestamp)
        for sen in sensors:
            sensorValues[sen].append(message.data.data[sen].value)
                    
        posReadRec.append(caget(getPosPV))
        posSetRec.append(caget(setPosPV))
        nShotsMeasured += 1
        nTotalShotsMeasured += 1
        progress = nTotalShotsMeasured/nTotalShots*100
        print('shot: '+str(nShotsMeasured)+'/'+str(nShots))
        print('overall progress: '+str(progress)+' %')
        
    return True
      
# saves the data arrays into a .h5 file
def writeFile():
    with h5py.File(fileName, 'a') as hdf:
        hdf.create_dataset('PositionRead', data = np.array(posReadRec, dtype = float))
        hdf.create_dataset('PositionSet', data = np.array(posSetRec, dtype = float))
        for sen in sensors:
            hdf.create_dataset('SensorValue/'+sen, data = np.array(sensorValues[sen], dtype = float))
        for sen in EpicsChannels:
            hdf.create_dataset('SensorValue/'+sen, data = np.array(EpicsSensorValues[sen], dtype = float))
        
        hdf.create_dataset('pulseId', data = np.array(pulseIdRec, dtype = float))
        hdf.create_dataset('timeStamp', data = np.array(tsRec, dtype = float))
      

def positionReached(goal):
    
    currentPosition = caget(getPosPV)
   
    # make a delta for x,y,z and another one with rx,ry,rz
    delta = np.abs(goal-currentPosition)
    
    if delta <= 0.001: 
         
        print('target reached, Delta Position is: ' + str(delta))
     
        
        return True
    else:
        print('   target not reached')
        print('   Goal is:             ' + str(goal))
        print('   Actual val:     ' + str(currentPosition))
        print('   Delta val is:   ' + str(delta))

        return False

# charges the calculated positions into the absolute position and move
def loadPoint(goal):
    if useMotor:
        caput(setPosPV, goal)
        while not positionReached(goal):
            time.sleep(.1)
        
# function to scan from pointA to pointB
def scan(pointA, pointB, stream):
    ### read Epics channels before scan
    for sen in EpicsChannels:
        val=caget(sen)
        EpicsSensorValues[sen].append(val)
    stepSize = (pointB - pointA)/nSteps
    for i in range(nSteps+1):
        print('---------------------------------------------------------------')
        print('step: '+str(i)+'/'+str(nSteps))
        loadPoint(pointA+i*stepSize)
        sleep(delayTime)
        readData(stream)
        
       


def plotData(outputFile):
    file = h5py.File(fileName, 'r')
    unit = 'mm'
    
    #plot loss profile
    fig1 = plt.figure(figsize = (12,9))
    ax1 = fig1.add_subplot(111)
    
    posData = np.array(file['PositionRead'].value)
    for s in plotSensors:
        data = np.array(file['SensorValue/'+s].value)
        if data.ndim==1:
            data = np.array(file['SensorValue/'+s].value)
            #data-= np.average(data[-10:])
            mean = np.sum(posData*data)/np.sum(data)
            rms = np.sqrt(np.sum((posData-mean)**2*data))/np.sqrt(np.sum(data))
            print(mean, rms)
            #ax1.set_title('rms: '+str(rms))
            ax1.plot(posData, data, label = s, ls = '-', marker = '+', ms = 10, color = 'r')
	  
    ax1.set_xlabel('Position ('+unit+')')
    ax1.set_ylabel('BLM Loss')
    ax1.ticklabel_format(style='sci', scilimits = (-2,2), axis='both')

    ax1.legend()

    fig1.tight_layout()
    fig1.savefig(directory + scanName + '_BLM.png')
    
    #plot waveforms
    if False:
        fig2 = plt.figure(figsize = (16,9), dpi = 200)
        ax2 = fig2.add_subplot(111)
        
        for s in plotSensors:
            data = np.array(file['SensorValue/'+s].value)
            if data.ndim==2:
                for i in range(len(posData)):
                    ax2.plot(data[i], label = str(posData[i])+' mm', ls = '-')
			
        ax2.set_xlabel('Sample')
        ax2.set_ylabel('Waveform')
        ax2.ticklabel_format(style='sci', scilimits = (-2,2), axis='both')
        
        ax2.legend()
    
        fig2.tight_layout()
        fig2.savefig(directory + scanName + '_Waveform.png')

        
nScans = int(len(points)/2)
nTotalShots = nScans*(nSteps+1)*nShots
nTotalShotsMeasured = 0
progress = 0.

i=0
with source(channels=[*sensors, beamOK]) as stream:
    while i < len(points):
        print('---------------------------------------------------------------')
        print('---------------------------------------------------------------')
        print('scan: '+str(int(i/2+1))+'/'+str(int(nScans)))
        scan(points[i], points[i+1], stream)
        i += 2

# save the file 
writeFile()

# plot data
plotData(fileName)
plt.show()

print('Script ended successfully.')
sys.exit(0)
