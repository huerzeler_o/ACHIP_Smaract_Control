import sys
import os
from epics import caget, caput
import time
import pifacedigitalio
import datetime
import logging

logging.basicConfig(format='%(asctime)s %(levelname)s %(name)s: %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', level=logging.DEBUG, filename='/home/pi/Documents/security_routine.log')

pifacedigital = pifacedigitalio.PiFaceDigital()
relail0 = pifacedigital.output_pins[0]
relail1 = pifacedigital.output_pins[1]
actualState = 4.

# initial set
logging.info('Start script.........')
relail0.turn_off()
relail1.turn_off()
time.sleep(1)

# alarm function
def setAlarm(reason, message = ''):
    global actualState
    if reason == 0.1 and actualState != reason:
        relail0.turn_on()
        relail1.turn_on()
        actualState = 0.1
        caput('SATSY03-DLAC080-DHXP:Alarm', 0)
        logging.info(message + '\x1b[1;32;40m' + ' -----> no alarm' + '\x1b[0m')
    if reason == 0.2 and actualState != reason:
        relail0.turn_on()
        relail1.turn_on()
        actualState = 0.2
        caput('SATSY03-DLAC080-DHXP:Alarm', 0)
        logging.info(message + '\x1b[1;32;40m' + ' -----> no alarm' + '\x1b[0m')
    if reason == 1 and actualState != reason:
        relail0.turn_off()
        relail1.turn_on()
        actualState = 1
        caput('SATSY03-DLAC080-DHXP:Alarm', 1)
        logging.info(message + '\x1b[1;31;40m' + ' -----> set alarm: l0' + '\x1b[0m')
    if reason == 2 and actualState != reason:
        relail0.turn_off()
        relail1.turn_off()
        actualState = 2
        caput('SATSY03-DLAC080-DHXP:Alarm', 2)
        logging.warning(message + '\x1b[1;31;40m' + ' -----> set alarm: l1' + '\x1b[0m')

# make sure the ethernet connection is up and running
while os.system('ping -c 1 -q sf-gw') != 0:
    logging.warning('ping sf-gw failed')
    setAlarm(2, 'No connection at startup.')
    time.sleep(1)

logging.info('Network found, Start Security Routine')
logging.info('Test PV: SF Mode: '+str(caget('SATSY03-DLAC080-DHXP:SwissFELmode')))
# start security routine
while True:
    try:
        connected = caget('SATSY03-DLAC080-DHXP:isConnected', timeout=0.5)
        referenced = caget('SATSY03-DLAC080-DHXP:isReferenced', timeout=0.5)
        SwissFELmode = caget('SATSY03-DLAC080-DHXP:SwissFELmode', timeout=0.5)
        parkPosition = caget('SATSY03-DLAC080-DHXP:ParkPosition', timeout=0.5)
        if connected and referenced:
            if SwissFELmode == True:
                if parkPosition == True:
                    setAlarm(0.1, 'SwissFEL mode and in park position.')
                else:
                    setAlarm(2, 'SwissFEL mode and not not in park position.') #L1 no beam
            elif SwissFELmode == False:
                if parkPosition == True:
                    setAlarm(0.2, 'ACHIP mode and in park position.')
                else:
                    setAlarm(1, 'ACHIP mode and not in park position.') #L0, 1hz beam
        else:
            setAlarm(2, 'Error: Conncection: '+str(connected)+', Reference: '+str(referenced))
    except:
        setAlarm(2)
        logging.error('Error: Cannot connect to the PVs.')
    time.sleep(.300)