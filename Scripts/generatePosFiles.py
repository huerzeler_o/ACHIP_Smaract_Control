# -*- coding: utf-8 -*-
import numpy as np
import h5py

samples = ['SampleHolder1', 'SampleHolder2', 'SampleHolder3', 'SampleHolder4', 'SampleHolder5']

sampleDU = 18e3
sin30 = np.sin(30/180*np.pi)
cos30 = np.cos(30/180*np.pi)

#pre defined values
s1 = {'linH': 30e3}
s2 = {'linH': 30e3}
#pivot at LH=0
s3 = {'linH':25e3, 'x': 0, 'y': 2.2e3, 'z': -16.5e3, 'pivotX': -8.913e3, 'pivotY': 47.719e3, 'pivotZ': -5.567e3}
s4 = {'linH': 7e3}
s5 = {'linH': -11e3}

#correct pivot of s3 for LH
s3['pivotX'] = s3['pivotX'] - s3['linH']*sin30
s3['pivotZ'] = s3['pivotZ'] + s3['linH']*cos30


s1['x'] = s3['x'] - (2*sampleDU - (s1['linH']-s3['linH']))*sin30
s1['y'] = s3['y']
s1['z'] = s3['z'] + (2*sampleDU - (s1['linH']-s3['linH']))*cos30
s1['pivotX'] = s3['pivotX'] + (2*sampleDU - (s1['linH']-s3['linH']))*sin30
s1['pivotY'] = s3['pivotY']
s1['pivotZ'] = s3['pivotZ'] - (2*sampleDU - (s1['linH']-s3['linH']))*cos30

s2['x'] = s3['x'] - (sampleDU - (s2['linH']-s3['linH']))*sin30
s2['y'] = s3['y']
s2['z'] = s3['z'] + (sampleDU - (s2['linH']-s3['linH']))*cos30
s2['pivotX'] = s3['pivotX'] + (sampleDU - (s2['linH']-s3['linH']))*sin30
s2['pivotY'] = s3['pivotY']
s2['pivotZ'] = s3['pivotZ'] - (sampleDU - (s2['linH']-s3['linH']))*cos30

s4['x'] = s3['x'] 
s4['y'] = s3['y'] 


s4['z'] = s3['z']
s4['pivotX'] = s3['pivotX'] 
s4['pivotY'] = s3['pivotY']
s4['pivotZ'] = s3['pivotZ']

s5['x'] = s3['x']
s5['y'] = s3['y']
s5['z'] = s3['z']
s5['pivotX'] = s3['pivotX']
s5['pivotY'] = s3['pivotY']
s5['pivotZ'] = s3['pivotZ']

for s in [s1, s2, s3, s4, s5]:
    s['rx'] = 0
    s['ry'] = 0
    s['rz'] = 0
for name, s in zip(samples, [s1, s2, s3, s4, s5]):
    print(name)
    print(s)
    f = h5py.File(name+'.h5')
    f.clear()
    for k in s:
        if k=='linH':
            f.create_dataset('PositionLIN/'+k, data=np.array([s[k]]))
        else:
            f.create_dataset('PositionHEX/'+k, data=np.array([s[k]]))
            
    f.close()


#f=h5py.File('parkPosition.h5', 'r')
#list(f['PositionHEX'].keys())
#Out[28]: ['pivotX', 'pivotY', 'pivotZ', 'rx', 'ry', 'rz', 'x', 'y', 'z']

#list(f['PositionLIN'].keys())
#Out[29]: ['linH', 'lin1z', 'lin2x', 'lin2z', 'lin3x', 'lin3y', 'lin3z']