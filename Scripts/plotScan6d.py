# -*- coding: utf-8 -*-
'''
this script plots scan data from the hexapod scan tool with respect to 7 coordinates. (x,y,z,rx,ry,rz and s)
s is the scan coordinate, assuming a straight movement of the hexapod
''' 
import numpy as np
import matplotlib.pyplot as plt
import h5py 

path = 'C:\\Users\\hermann_b\\Documents\\pyScripts\\ACHIP-sf-achipi03\\Data\\ScanData\\FirstScan_31.08.18\\'
fileName = 'P1-P2_31.08.18_16.28.h5'

file = h5py.File(path+fileName, 'r')

coordinates = ['x', 'y', 'z', 'rx', 'ry', 'rz']
units = ['um','um','um', 'deg', 'deg', 'deg']
sensors = ['cam0', 'cam1']

fig = plt.figure(fileName, figsize = (12,6), dpi = 200)
ax0 = plt.subplot2grid((2, 5), (0, 0), colspan=2, rowspan=2)
ax1 = plt.subplot2grid((2, 5), (0, 2))
ax2 = plt.subplot2grid((2, 5), (0, 3))
ax3 = plt.subplot2grid((2, 5), (0, 4))
ax4 = plt.subplot2grid((2, 5), (1, 2))
ax5 = plt.subplot2grid((2, 5), (1, 3))
ax6 = plt.subplot2grid((2, 5), (1, 4))

axes = [ax1, ax2, ax3, ax4, ax5, ax6]

for c, ax, unit in zip(coordinates, axes, units):
    xData = np.array(file['PositionHEX/'+c].value)
    
    for s in sensors:
        yData = np.array(file['SensorValue/'+s].value)
        ax.plot(xData, yData, label = s, ls = '', marker = 'o', ms = 1)
    
    ax.set_xlabel(c+' ('+unit+')')
    ax.set_ylabel('Loss')
    ax.ticklabel_format(style='sci', scilimits = (-2,2), axis='both')

x = np.array(file['PositionHEX/x'].value)
y = np.array(file['PositionHEX/y'].value)
z = np.array(file['PositionHEX/z'].value)
p = np.array([x-x[0],y-y[0],z-z[0]])
xData = np.sqrt(np.sum(p**2, axis=0))

for s in sensors:
    yData = np.array(file['SensorValue/'+s].value)
    ax0.plot(xData, yData, label = s, ls = '', marker = 'o', ms = 1)

ax0.set_xlabel('s (um)')
ax0.set_ylabel('Loss')
ax0.ticklabel_format(style='sci', scilimits = (-2,2), axis='both')

handles, labels = axes[0].get_legend_handles_labels()
fig.legend(handles, labels, loc = 'center left')

fig.tight_layout()


def plotData(path):
    file = h5py.File(path, 'r')
    coordinates = ['x', 'y', 'z', 'rx', 'ry', 'rz']
    units = ['um','um','um', 'deg', 'deg', 'deg']
    sensors = ['cam0', 'cam1']

    fig = plt.figure(fileName, figsize = (12,6), dpi = 200)
    ax0 = plt.subplot2grid((2, 5), (0, 0), colspan=2, rowspan=2)
    ax1 = plt.subplot2grid((2, 5), (0, 2))
    ax2 = plt.subplot2grid((2, 5), (0, 3))
    ax3 = plt.subplot2grid((2, 5), (0, 4))
    ax4 = plt.subplot2grid((2, 5), (1, 2))
    ax5 = plt.subplot2grid((2, 5), (1, 3))
    ax6 = plt.subplot2grid((2, 5), (1, 4))

    axes = [ax1, ax2, ax3, ax4, ax5, ax6]

    for c, ax, unit in zip(coordinates, axes, units):
        xData = np.array(file['PositionHEX/'+c].value)
        
        for s in sensors:
            yData = np.array(file['SensorValue/'+s].value)
            ax.plot(xData, yData, label = s, ls = '', marker = 'o', ms = 1)
        
        ax.set_xlabel(c+' ('+unit+')')
        ax.set_ylabel('Loss')
        ax.ticklabel_format(style='sci', scilimits = (-2,2), axis='both')

    x = np.array(file['PositionHEX/x'].value)
    y = np.array(file['PositionHEX/y'].value)
    z = np.array(file['PositionHEX/z'].value)
    p = np.array([x-x[0],y-y[0],z-z[0]])
    xData = np.sqrt(np.sum(p**2, axis=0))

    for s in sensors:
        yData = np.array(file['SensorValue/'+s].value)
        ax0.plot(xData, yData, label = s, ls = '', marker = 'o', ms = 1)

    ax0.set_xlabel('s (um)')
    ax0.set_ylabel('Loss')
    ax0.ticklabel_format(style='sci', scilimits = (-2,2), axis='both')

    handles, labels = axes[0].get_legend_handles_labels()
    fig.legend(handles, labels, loc = 'center left')

    fig.tight_layout