import h5py as h
import numpy as np
from pyscan import *
from pyscan.utils import DictionaryDataProcessor

# Defines positions to move the motor to.
positions = [1, 2, 3, 4]
positioner = VectorPositioner(positions)

# Read "PYSCAN:TEST:OBS1" value at each position.
readables = [bs_property("SATCL01-DBLM135:B2_LOSS")]

# Move MOTOR1 over defined positions.
writables = [
#        epics_pv("PYSCAN:TEST:MOTOR1:SET", "PYSCAN:TEST:MOTOR1:GET")
]

# At each read of "PYSCAN:TEST:OBS1", check if "PYSCAN:TEST:VALID1" == 10 and "PYSCAN:TEST:VALID2" == 12
conditions = [
#    epics_condition("PYSCAN:TEST:VALID1", 10),
#    epics_condition("PYSCAN:TEST:VALID2", 12)
]

# Before the scan starts, set "PYSCAN:TEST:PRE1:SET" to 1.
initialization = [
#        action_set_epics_pv("PYSCAN:TEST:PRE1:SET", 1, "PYSCAN:TEST:PRE1:GET")
]

# After the scan completes, restore the original value of "PYSCAN:TEST:MOTOR1:SET".
finalization = [
#        action_restore(writables)
]

# At each position, do 4 readings of the readables with 10Hz (0.1 seconds between readings).
settings = scan_settings(measurement_interval=0.1, n_measurements=1)
data_processor = DictionaryDataProcessor(readables)

# Execute the scan and get the result.
result = scan(positioner=positioner,
              readables=readables,
              writables=writables,
              conditions=conditions,
              initialization=initialization,
              finalization=finalization,
              settings=settings,
              data_processor=data_processor)