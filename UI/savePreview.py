# -*- coding: utf-8 -*-
import numpy as np
from epics import caget
import datetime
import os
import h5py
import sys
import matplotlib.pyplot as plt

scanName = caget('SATSY03-DLAC080-DHXP:scanName') + '_' + str(datetime.datetime.now().strftime("%Y%m%d_%H%M%S"))
directory = '/afs/psi.ch/intranet/SF/Diagnostics/ACHIP/Data/' + scanName
fileName = directory + '/LiveData' + '.h5'

if not os.path.exists(directory):
    os.makedirs(directory)

### Read Compress Records
BLM = caget('SATSY03-DLAC080-DHXP:BLM1_comp')
pulseID = caget('SATSY03-DLAC080-DHXP:pulseID_comp')

XposHEX = caget('SATSY03-DLAC080-DHXP:posX_comp')
YposHEX = caget('SATSY03-DLAC080-DHXP:posY_comp')
ZposHEX = caget('SATSY03-DLAC080-DHXP:posZ_comp')
RXposHEX = caget('SATSY03-DLAC080-DHXP:posRX_comp')
RYposHEX = caget('SATSY03-DLAC080-DHXP:posRY_comp')
RZposHEX = caget('SATSY03-DLAC080-DHXP:posRZ_comp')

posLHU = caget('SATSY03-DLAC080-DHXP:posLHU_comp')
posLIN1Z = caget('SATSY03-DLAC080-DHXP:posLIN1Z_comp')
posLIN2X = caget('SATSY03-DLAC080-DHXP:posLIN2X_comp')
posLIN2Z = caget('SATSY03-DLAC080-DHXP:posLIN2Z_comp')
posLIN3X = caget('SATSY03-DLAC080-DHXP:posLIN3X_comp')
posLIN3Y = caget('SATSY03-DLAC080-DHXP:posLIN3Y_comp')
posLIN3Z = caget('SATSY03-DLAC080-DHXP:posLIN3Z_comp')
posLINTHZ = caget('SATSY03-DLAC080-DHXP:posLINTHZ_comp')

#scopeSum = caget('SATSY03-DLAC080-DOSC:SUMHIST')
#scopePos = caget('SATSY03-DLAC080-DOSC:POSHIST')

with h5py.File(fileName, 'a') as hdf:
    hdf.create_dataset('SATSY03-DLAC080-DHXP:BLM1_comp', data = np.array(BLM, dtype = float))
    
    hdf.create_dataset('PositionHEX/x', data = np.array(XposHEX, dtype = float))
    hdf.create_dataset('PositionHEX/y', data = np.array(YposHEX, dtype = float))
    hdf.create_dataset('PositionHEX/z', data = np.array(ZposHEX, dtype = float))
    hdf.create_dataset('PositionHEX/rx', data = np.array(RXposHEX, dtype = float))
    hdf.create_dataset('PositionHEX/ry', data = np.array(RYposHEX, dtype = float))
    hdf.create_dataset('PositionHEX/rz', data = np.array(RZposHEX, dtype = float))
    
    hdf.create_dataset('PositionLIN/LHU', data = np.array(posLHU, dtype = float))
    hdf.create_dataset('PositionLIN/LIN1Z', data = np.array(posLIN1Z, dtype = float))
    hdf.create_dataset('PositionLIN/LIN2X', data = np.array(posLIN2X, dtype = float))
    hdf.create_dataset('PositionLIN/LIN2Z', data = np.array(posLIN2Z, dtype = float))
    hdf.create_dataset('PositionLIN/LIN3X', data = np.array(posLIN3X, dtype = float))
    hdf.create_dataset('PositionLIN/LIN3Y', data = np.array(posLIN3Y, dtype = float))
    hdf.create_dataset('PositionLIN/LIN3Z', data = np.array(posLIN3Z, dtype = float))
    hdf.create_dataset('PositionLIN/LINTHZ', data = np.array(posLINTHZ, dtype = float))
    
#    hdf.create_dataset('Scope/SUMHIST', data = np.array(scopeSum, dtype = float))
#    hdf.create_dataset('Scope/POSHIST', data = np.array(scopePos, dtype = float))

def plotData(outputFile):
    file = h5py.File(fileName, 'r')
    coordinates = ['x', 'y', 'z', 'rx', 'ry', 'rz']
    units = ['um','um','um', 'deg', 'deg', 'deg']
    
    fig = plt.figure(outputFile, figsize = (18,7), dpi = 200)
    ax0 = plt.subplot2grid((2, 5), (0, 0), colspan=2, rowspan=2)
    ax1 = plt.subplot2grid((2, 5), (0, 2))
    ax2 = plt.subplot2grid((2, 5), (0, 3))
    ax3 = plt.subplot2grid((2, 5), (0, 4))
    ax4 = plt.subplot2grid((2, 5), (1, 2))
    ax5 = plt.subplot2grid((2, 5), (1, 3))
    ax6 = plt.subplot2grid((2, 5), (1, 4))

    axes = [ax1, ax2, ax3, ax4, ax5, ax6]
    s = 'SATSY03-DLAC080-DHXP:BLM1_comp'
    data = np.array(file[s].value)
    for c, ax, unit in zip(coordinates, axes, units):
        xData = np.array(file['PositionHEX/'+c].value)
        ax.plot(xData, data, label = s, ls = '', marker = '+', ms = 5, color = 'r')
        ax.set_xlabel(c+' ('+unit+')')
        ax.set_ylabel('Loss')
        ax.ticklabel_format(style='sci', scilimits = (-2,2), axis='both')

    x = np.array(file['PositionHEX/x'].value)
    y = np.array(file['PositionHEX/y'].value)
    z = np.array(file['PositionHEX/z'].value)
    p = np.array([x-x[0],y-y[0],z-z[0]])
    xData = np.sqrt(np.sum(p**2, axis=0))

    ax0.plot(xData, data, label = s, ls = '', marker = '+', ms = 5, color = 'r')

    ax0.set_xlabel('s (um)')
    ax0.set_ylabel('Loss')
    ax0.ticklabel_format(style='sci', scilimits = (-2,2), axis='both')

    handles, labels = axes[0].get_legend_handles_labels()
    fig.legend(handles, labels, loc = 'center left')

    fig.tight_layout()
    fig.savefig(directory + '/DataPlot.png')

plotData(fileName)

print('Script ended successfully.')
sys.exit(0)
