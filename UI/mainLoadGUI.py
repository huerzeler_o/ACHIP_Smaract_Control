import sys
from PyQt4 import QtCore,QtGui
from PyQt4.uic import loadUiType
from PyQt4 import uic, Qt

from os import listdir
from os.path import isfile, join, getmtime
import h5py
import matplotlib.pyplot as plt
from epics import caget, caput, PV
from time import sleep
import numpy as np

Ui_MainWindow, QMainWindow = loadUiType('/afs/psi.ch/intranet/SF/Diagnostics/ACHIP/ACHIP_Smaract_Control/UI/loadGUI.ui')

class Main(QMainWindow, Ui_MainWindow):         # class to start the application
    def __init__(self, ):                       # definition of the constructor
        super(Main, self).__init__()
        self.onlyfiles = []
        self.normalAxis = 'z'                   # describes the normal axis to the plane in which the geometric polygon is created. x: laser, y: horizontal application, z: electron beam
        self.coordinate1 = 0
        self.coordinate2 = 1
        self.scanPointsPP = []
        self.zeroPoint = ''
        self.path = '/afs/psi.ch/intranet/SF/Diagnostics/ACHIP/ACHIP_Smaract_Control/Saved_Positions/'
        self.setupUi(self)                      # call the UI's setup function
        self.loadPositions()

        # Tab 1 - Settings
        self.btnOk.clicked.connect(self.okClickManager)
        self.btnCancel2.clicked.connect(self.close)

        # Tab 2 - Save/Load Positions
        self.btnLoad.clicked.connect(self.loadClickManager)
        self.btnSave.clicked.connect(self.saveClickManager)
        self.btnCancel1.clicked.connect(self.close)

        # Tab 3 - Scan Polygon
        self.btnOk4.clicked.connect(self.generatePolygon)
        self.btnCenter.clicked.connect(self.loadZeroPosition)
        self.btnCancel4.clicked.connect(self.close)

        # Tab 4 - Scan Point to Point
        self.btnStart.clicked.connect(self.loadStartPosition)
        self.btnEnd.clicked.connect(self.loadEndPosition)
        self.btnOk3.clicked.connect(self.prepareScan)
        self.btnCancel3.clicked.connect(self.close)

        # Tab 5 - Scan Direction
        self.btnSetStart.clicked.connect(self.loadStartPositionDir)
        self.btnCancel5.clicked.connect(self.close)

        self.btnOk5.clicked.connect(self.scanDirection)



# Tab 1 - Settings

    def okClickManager(self):

        caput('SATSY03-DLAC080-DHXP:SwissFELmode', 0)
        caput('SATSY03-DLAC080-DHXP:freqHEX', self.Freq.value())
        caput('SATSY03-DLAC080-DHXP:accHEX', self.Acc.value())
        caput('SATSY03-DLAC080-DHXP:velHEX', self.Vel.value())
        caput('SATSY03-DLAC080-DHXP:pivotX', self.pivotX.value())
        caput('SATSY03-DLAC080-DHXP:pivotY', self.pivotY.value())
        caput('SATSY03-DLAC080-DHXP:pivotZ', self.pivotZ.value())
        caput('SATSY03-DLAC080-DHXP:setPivot', 1)

        self.close()

# Tab 2 - Load/Save positions

    def loadPositions(self):
        self.onlyfiles = [f for f in listdir(self.path) if isfile(join(self.path, f))]
        self.onlyfiles.sort(key=lambda x: -getmtime(join(self.path, x)))
        # sav/load tab
        self.tableWidget.setRowCount(len(self.onlyfiles))
        for i in range(len(self.onlyfiles)):
            self.tableWidget.setItem(i, 0, QtGui.QTableWidgetItem(self.onlyfiles[i]))
        
        # scan tab
        self.tableWidget2.setRowCount(len(self.onlyfiles))
        for i in range(len(self.onlyfiles)):
            self.tableWidget2.setItem(i, 0, QtGui.QTableWidgetItem(self.onlyfiles[i]))
        
        # scan geometry tab
        self.tableWidget3.setRowCount(len(self.onlyfiles))
        for i in range(len(self.onlyfiles)):
            self.tableWidget3.setItem(i, 0, QtGui.QTableWidgetItem(self.onlyfiles[i]))
        
        # scan direction
        self.tableWidget4.setRowCount(len(self.onlyfiles))
        for i in range(len(self.onlyfiles)):
            self.tableWidget4.setItem(i, 0, QtGui.QTableWidgetItem(self.onlyfiles[i]))

        # load the setting from the epics channels
        velocity = caget('SATSY03-DLAC080-DHXP:velHEX')
        self.Vel.setValue(velocity)
        acceleration = caget('SATSY03-DLAC080-DHXP:accHEX')
        self.Acc.setValue(acceleration)
        frequency = caget('SATSY03-DLAC080-DHXP:freqHEX')
        self.Freq.setValue(frequency)
        pivotX = caget('SATSY03-DLAC080-DHXP:pivotX')
        self.pivotX.setValue(pivotX)
        pivotY = caget('SATSY03-DLAC080-DHXP:pivotY')
        self.pivotY.setValue(pivotY)
        pivotZ = caget('SATSY03-DLAC080-DHXP:pivotZ')
        self.pivotZ.setValue(pivotZ)

    def loadClickManager(self):
        indexes = self.tableWidget.selectionModel().selectedRows()
        index = indexes[-1].row()
        localPath = self.path + str(self.onlyfiles[index])
        f = h5py.File(localPath, 'r')
        output = 'Loaded: '
        try:
            # load hexapod position
            if 'PositionHEX' in list(f.keys()):
                if 'x' in list(f['PositionHEX'].keys()):
                    self.loadData(f, '/PositionHEX/x', 0, 'SATSY03-DLAC080-DHXP:absposX')
                if 'y' in list(f['PositionHEX'].keys()):
                    self.loadData(f, '/PositionHEX/y', 0, 'SATSY03-DLAC080-DHXP:absposY')
                if 'z' in list(f['PositionHEX'].keys()):
                    self.loadData(f, '/PositionHEX/z', 0, 'SATSY03-DLAC080-DHXP:absposZ')
                if 'rx' in list(f['PositionHEX'].keys()):
                    self.loadData(f, '/PositionHEX/rx', 0, 'SATSY03-DLAC080-DHXP:absposRX')
                if 'ry' in list(f['PositionHEX'].keys()):
                    self.loadData(f, '/PositionHEX/ry', 0, 'SATSY03-DLAC080-DHXP:absposRY')
                if 'rz' in list(f['PositionHEX'].keys()):
                    self.loadData(f, '/PositionHEX/rz', 0, 'SATSY03-DLAC080-DHXP:absposRZ')
                if 'pivotX' in list(f['PositionHEX'].keys()):
                    self.loadData(f, '/PositionHEX/pivotX', 0, 'SATSY03-DLAC080-DHXP:pivotX')
                if 'pivotY' in list(f['PositionHEX'].keys()):
                    self.loadData(f, '/PositionHEX/pivotY', 0, 'SATSY03-DLAC080-DHXP:pivotY')
                if 'pivotZ' in list(f['PositionHEX'].keys()):
                    self.loadData(f, '/PositionHEX/pivotZ', 0, 'SATSY03-DLAC080-DHXP:pivotZ')
                output += 'PositionsHEX'

            # load settings
            if 'SettingsHEX' in list(f.keys()):
                #if 'velocity' in list(f['SettingsHEX'].keys()):
                #    self.loadData(f, 'SettingsHEX/velocity', 0, 'SATSY03-DLAC080-DHXP:velHEX')
                if 'acceleration' in list(f['SettingsHEX'].keys()):
                    self.loadData(f, 'SettingsHEX/acceleration', 0, 'SATSY03-DLAC080-DHXP:accHEX')
                if 'frequency' in list(f['SettingsHEX'].keys()):
                    self.loadData(f, 'SettingsHEX/frequency', 0, 'SATSY03-DLAC080-DHXP:freqHEX')
                output += ' Settings'

            if 'PositionLIN' in list(f.keys()):
                if 'linH' in list(f['PositionLIN'].keys()):
                    self.loadData(f, '/PositionLIN/linH', 0, 'SATSY03-DLAC080-DHXP:absposLHU')
                if 'lin1z' in list(f['PositionLIN'].keys()):
                    self.loadData(f, '/PositionLIN/lin1z', 0, 'SATSY03-DLAC080-DHXP:absposLIN1Z')
                if 'lin2x' in list(f['PositionLIN'].keys()):
                    self.loadData(f, '/PositionLIN/lin2x', 0, 'SATSY03-DLAC080-DHXP:absposLIN2X')
                if 'lin2z' in list(f['PositionLIN'].keys()):
                    self.loadData(f, '/PositionLIN/lin2z', 0, 'SATSY03-DLAC080-DHXP:absposLIN2Z')
                if 'lin3x' in list(f['PositionLIN'].keys()):
                    self.loadData(f, '/PositionLIN/lin3x', 0, 'SATSY03-DLAC080-DHXP:absposLIN3X')
                if 'lin3y' in list(f['PositionLIN'].keys()):
                    self.loadData(f, '/PositionLIN/lin3y', 0, 'SATSY03-DLAC080-DHXP:absposLIN3Y')
                if 'lin3z' in list(f['PositionLIN'].keys()):
                    self.loadData(f, '/PositionLIN/lin3z', 0, 'SATSY03-DLAC080-DHXP:absposLIN3Z')
                if 'linTHZ' in list(f['PositionLIN'].keys()):
                    self.loadData(f, '/PositionLIN/linTHZ', 0, 'SATSY03-DLAC080-DHXP:absposLINTHZ')
                output += ' PositionLIN'

                if self.setVal.isChecked() == True:
                    caput('SATSY03-DLAC080-DHXP:hSet6d', [1,1,1,1,1,1])
                    caput('SATSY03-DLAC080-DHXP:setposLHU', 1)
                    caput('SATSY03-DLAC080-DHXP:setposLIN1Z', 1)
                    caput('SATSY03-DLAC080-DHXP:setposLIN2X', 1)
                    caput('SATSY03-DLAC080-DHXP:setposLIN2Z', 1)
                    caput('SATSY03-DLAC080-DHXP:setposLIN3X', 1)
                    caput('SATSY03-DLAC080-DHXP:setposLIN3Y', 1)
                    caput('SATSY03-DLAC080-DHXP:setposLIN3Z', 1)
                    caput('SATSY03-DLAC080-DHXP:setposLINTHZ', 1)
                    caput('SATSY03-DLAC080-DHXP:setPivot', 1)
                    output += ', values set.'

            print(output)
        except:
            print('Failed loading data. ' + output)
        self.close()

    def saveClickManager(self):
        try:
            xPos = [float(caget('SATSY03-DLAC080-DHXP:posX'))]
            yPos = [float(caget('SATSY03-DLAC080-DHXP:posY'))]
            zPos = [float(caget('SATSY03-DLAC080-DHXP:posZ'))]
            rxPos = [float(caget('SATSY03-DLAC080-DHXP:posRX'))]
            ryPos = [float(caget('SATSY03-DLAC080-DHXP:posRY'))]
            rzPos = [float(caget('SATSY03-DLAC080-DHXP:posRZ'))]
            linH =  [float(caget('SATSY03-DLAC080-DHXP:posLHU'))]
            lin1z = [float(caget('SATSY03-DLAC080-DHXP:posLIN1Z'))]
            lin2x = [float(caget('SATSY03-DLAC080-DHXP:posLIN2X'))]
            lin2z = [float(caget('SATSY03-DLAC080-DHXP:posLIN2Z'))]
            lin3x = [float(caget('SATSY03-DLAC080-DHXP:posLIN3X'))]
            lin3y = [float(caget('SATSY03-DLAC080-DHXP:posLIN3Y'))]
            lin3z = [float(caget('SATSY03-DLAC080-DHXP:posLIN3Z'))]
            linTHZ = [float(caget('SATSY03-DLAC080-DHXP:posLINTHZ'))]
            pivotX = [float(caget('SATSY03-DLAC080-DHXP:pivotX'))]
            pivotY = [float(caget('SATSY03-DLAC080-DHXP:pivotY'))]
            pivotZ = [float(caget('SATSY03-DLAC080-DHXP:pivotZ'))]
            velHEX = [caget('SATSY03-DLAC080-DHXP:velHEX')]
            freqHEX = [caget('SATSY03-DLAC080-DHXP:freqHEX')]
            accHEX = [caget('SATSY03-DLAC080-DHXP:accHEX')]
        except:
            print('Failed to grab data from the PVs')

        try:
            # get the name and save file
            name = self.fileName.text()
            localPath = self.path + name + '.h5'
            with h5py.File(localPath, 'w') as hdf:
                hdf.create_dataset('PositionHEX/x', data=xPos)
                hdf.create_dataset('PositionHEX/y', data=yPos)
                hdf.create_dataset('PositionHEX/z', data=zPos)
                hdf.create_dataset('PositionHEX/rx', data=rxPos)
                hdf.create_dataset('PositionHEX/ry', data=ryPos)
                hdf.create_dataset('PositionHEX/rz', data=rzPos)
                hdf.create_dataset('PositionHEX/pivotX', data=pivotX)
                hdf.create_dataset('PositionHEX/pivotY', data=pivotY)
                hdf.create_dataset('PositionHEX/pivotZ', data=pivotZ)
                hdf.create_dataset('SettingsHEX/velocity', data=velHEX)
                hdf.create_dataset('SettingsHEX/acceleration', data=accHEX)
                hdf.create_dataset('SettingsHEX/frequency', data=freqHEX)
                hdf.create_dataset('PositionLIN/linH', data=linH)
                hdf.create_dataset('PositionLIN/lin1z', data=lin1z)
                hdf.create_dataset('PositionLIN/lin2x', data=lin2x)
                hdf.create_dataset('PositionLIN/lin2z', data=lin2z)
                hdf.create_dataset('PositionLIN/lin3x', data=lin3x)
                hdf.create_dataset('PositionLIN/lin3y', data=lin3y)
                hdf.create_dataset('PositionLIN/lin3z', data=lin3z)
                hdf.create_dataset('PositionLIN/linTHZ', data=linTHZ)
            print('Position saved.')
        except:
            print('Failed saving the position.')
        self.close()
    
    def loadData(self, f, path, elementNr, PV):
        data = f[path]
        data = np.array(data)
        caput(PV, data[elementNr])

# Tab 3 - Scan Polygon
    def generatePolygon(self):
        scanPointsPolygon = []
        # read out the GUI text boxes and convert them into the desired format
        nWires = int(self.nWires.text())
        radius = float(self.radius.text())
        scanLength = float(self.scanLength.text())
        offAngle = float(self.offAngle.text())

        # read out the zero position into the center array to forward it to the genPoly function
        zeroPoint = h5py.File(self.zeroPoint, 'r')
        x = self.loadDataArray(zeroPoint, '/PositionHEX/x', 0)
        y = self.loadDataArray(zeroPoint, '/PositionHEX/y', 0)
        z = self.loadDataArray(zeroPoint, '/PositionHEX/z', 0)
        rx = self.loadDataArray(zeroPoint, '/PositionHEX/rx', 0)
        ry = self.loadDataArray(zeroPoint, '/PositionHEX/ry', 0)
        rz = self.loadDataArray(zeroPoint, '/PositionHEX/rz', 0)
        center = np.array([x,y,z,rx,ry,rz])
        
        # get the normal axis
        self.normalAxis = self.btnNormalAxis.currentText()
        print('Normal axis set to: ' + self.normalAxis)

        # use the functions provided from the generatePolygon.py file to generate the Points
        scanPointsPolygon = self.genPoly(nWires,radius,scanLength,offAngle,center)
        print(scanPointsPolygon)
        self.plotScanPoints(scanPointsPolygon, self.coordinate1, self.coordinate2)

        # write a h5 document with all the points
        self.writeScanPreparation(scanPointsPolygon)

        self.close()

    def writeScanPreparation(self, array):
        # writes a h5 scan preparation file with all the points to scan.
        with h5py.File('/afs/psi.ch/intranet/SF/Diagnostics/ACHIP/ACHIP_Smaract_Control/Scripts/scanPreparation.h5', 'w') as hdf:
            for i in range(len(array)):
                hdf.create_dataset('Points/P' + str(i), data=array[i])
    
    def loadZeroPosition(self):
        # read out the selected row and save its path
        indexes = self.tableWidget3.selectionModel().selectedRows()
        index = indexes[-1].row()
        localPath = self.path + str(self.onlyfiles[index])
        self.zeroPoint = localPath
        print('loaded zero position: ' + self.zeroPoint)
        self.btnCenter.setFlat(True)

    def loadDataArray(self, f, path, elementNr):
        data = f[path]
        data = np.array(data)
        return data[elementNr]

    def genPoly(self,nWires,radius,scanLength,offAngle,center):
        '''
        this function generates a polygon path for wire scan tomography. 
        Input
        center: np.array([x,y,z,rx,ry,rz]) in um,deg
        nWires: int 
        radius: flaot um
        scanLength: flaot um
        offAngle: offset angle of the wire star, 0 means wire 0 is horizontal (aligned with pos. x axis). in deg

        Output
        A list of length 2*nWires is returned. Points in the same format as center are returned in the following order:
        start wire 0, end wire 0, start wire 1, end wire 1, ... 
        '''
        scanPointsPolygon = []
        if self.normalAxis == 'x':
            self.coordinate1 = 1
            self.coordinate2 = 2

        elif self.normalAxis == 'y':
            self.coordinate1 = 0
            self.coordinate2 = 2

        if self.normalAxis == 'z':
            self.coordinate1 = 0
            self.coordinate2 = 1

        for i in range(nWires):
            beta = 2.*np.pi/nWires*i + offAngle/180*np.pi # angle of i-th wire in rad 
            for j in range(2): # for start and end point
                offset = np.zeros(6)
                offset[self.coordinate1] = radius*np.cos(beta) + (-1)**(j+1)*scanLength/2.*np.sin(-beta)
                offset[self.coordinate2] = radius*np.sin(beta) + (-1)**(j+1)*scanLength/2.*np.cos(-beta)
                scanPointsPolygon.append(center+offset)
        return scanPointsPolygon


        

    def plotScanPoints(self,scanPointsPolygon, idX, idY):
        '''
        this function plots a polygon path for wire scan tomography. 
        Input
        (output from genPoly) + The idX changes the mode of the genPoly, plotScanPoints functions (0 for SwissFEL operation, 2 for laser testing)
        A list of scan Points.
        start wire 0, end wire 0, start wire 1, end wire 1, ... 
        '''
        fig = plt.figure('Visualize Scan Path')
        ax = fig.add_subplot(111)
        for i,p in enumerate(scanPointsPolygon):
            ax.plot(p[idX], p[idY], marker='+', color='r')
            ax.annotate(i, (p[idX], p[idY]))
        
        xlabel = 'x'*(idX==0)+'y'*(idX==1)+'z'*(idX==2)+'rx'*(idX==3)+'ry'*(idX==4)+'rz'*(idX==5)+'arb.'*(idX==-1)
        ylabel = 'x'*(idY==0)+'y'*(idY==1)+'z'*(idY==2)+'rx'*(idY==3)+'ry'*(idY==4)+'rz'*(idY==5)+'arb.'*(idY==-1)
        
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        
        ax.set_aspect('equal')
        fig.tight_layout()
        plt.savefig('/afs/psi.ch/intranet/SF/Diagnostics/ACHIP/ACHIP_Smaract_Control/UI/temp_ScanPoints.png')

# Tab 4 - Point To Point
    def loadPosition(self,localPath):
        # reads out the position h5 file and returns it
        Point = h5py.File(localPath, 'r')
        x = self.loadDataArray(Point, '/PositionHEX/x', 0)
        y = self.loadDataArray(Point, '/PositionHEX/y', 0)
        z = self.loadDataArray(Point, '/PositionHEX/z', 0)
        rx = self.loadDataArray(Point, '/PositionHEX/rx', 0)
        ry = self.loadDataArray(Point, '/PositionHEX/ry', 0)
        rz = self.loadDataArray(Point, '/PositionHEX/rz', 0)
        return np.array([x,y,z,rx,ry,rz])

    def loadStartPosition(self):
        # localizes the choosen start position and writes it into the local self.scanPointsPP
        indexes = self.tableWidget2.selectionModel().selectedRows()
        if len(indexes)!=0:
            index = indexes[-1].row()
            localPath = self.path + str(self.onlyfiles[index])
            self.scanPointsPP.append(self.loadPosition(localPath))
            self.btnStart.setFlat(True)
            self.btnEnd.setFlat(False)
    
    def loadEndPosition(self):
        if self.btnStart.isFlat():
            indexes = self.tableWidget2.selectionModel().selectedRows()
            if len(indexes)!=0:
                index = indexes[-1].row()
                localPath = self.path + str(self.onlyfiles[index])
                self.scanPointsPP.append(self.loadPosition(localPath))
                self.btnStart.setFlat(False)
                self.btnEnd.setFlat(True)
        
    def prepareScan(self, array):
        # writes a h5 scan preparation file with all the points to scan.
               
        self.plotScanPoints(self.scanPointsPP, -1, -1)
        self.writeScanPreparation(self.scanPointsPP)
        print(self.scanPointsPP)
        self.close()

# Tab 5 - Scan Direction

    def scanDirection(self):
        actualPosition = np.zeros(6)
        scanLength = float(self.scanLength2.text())
        scanDirection = self.btnDirection.currentText()
        scanPoints = []
        if self.setActualPos.isChecked():
            actualPosition[0] = caget('SATSY03-DLAC080-DHXP:posX')
            actualPosition[1] = caget('SATSY03-DLAC080-DHXP:posY')
            actualPosition[2] = caget('SATSY03-DLAC080-DHXP:posZ')
            actualPosition[3] = caget('SATSY03-DLAC080-DHXP:posRX')
            actualPosition[4] = caget('SATSY03-DLAC080-DHXP:posRY')
            actualPosition[5] = caget('SATSY03-DLAC080-DHXP:posRZ')
            scanPoints = self.P(scanDirection, scanLength, actualPosition)
            self.writeScanPreparation(scanPoints)
        else:
            scanPoints = self.P(scanDirection, scanLength, self.loadStartPositionDir())
            self.writeScanPreparation(scanPoints)
        
        self.plotScanPoints(scanPoints, self.coordinate1, -1)
        self.close()

    def P(self, coordinateStr, length, start):
        '''
        this function generates a path for scan along a hexapod coordinate (x, y, z, rx, ry, rz). 
        Input
        coordinate: string ('x', 'y', 'z', 'rx', 'ry', 'rz')
        length: float um, deg
        start: np.array([x,y,z,rx,ry,rz]) in um,deg
        
        Output
        A list of length 2 is returned. Points in the same format as center are returned in the following order:
        [start, end] 
        '''
        coordinateInt = 0
        if coordinateStr == 'x': self.coordinate1 = 0
        if coordinateStr == 'y': self.coordinate1 = 1
        if coordinateStr == 'z': self.coordinate1 = 2
        if coordinateStr == 'rx': self.coordinate1 = 3
        if coordinateStr == 'ry': self.coordinate1 = 4
        if coordinateStr == 'rz': self.coordinate1 = 5
        
        endpoint = start.copy()
        endpoint[self.coordinate1] += length
        
        return [start.copy(), endpoint.copy()]

    def loadStartPositionDir(self):
        # localizes the choosen start position and returns it 
        indexes = self.tableWidget4.selectionModel().selectedRows()
        
        if len(indexes)!=0:
            index = indexes[-1].row()
            localPath = self.path + str(self.onlyfiles[index])
            return self.loadPosition(localPath)
        
        
if __name__ == "__main__":                      # main routine
    app = QtGui.QApplication(sys.argv)
    app.setStyle('cleanlooks')                  # needed on some systems

    main = Main()
    main.show()

    sys.exit(app.exec_())
