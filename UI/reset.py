# -*- coding: utf-8 -*-
import sys
from epics import caget, caput
import numpy as np

size = len(caget('SATSY03-DLAC080-DHXP:posX_comp'))
nanArray = np.zeros(size)
nanArray[:] = np.nan

caput('SATSY03-DLAC080-DHXP:BLM1_comp', nanArray)
caput('SATSY03-DLAC080-DHXP:pulseID_comp', nanArray)

caput('SATSY03-DLAC080-DHXP:posX_comp', nanArray)
caput('SATSY03-DLAC080-DHXP:posY_comp', nanArray)
caput('SATSY03-DLAC080-DHXP:posZ_comp', nanArray)
caput('SATSY03-DLAC080-DHXP:posRX_comp', nanArray)
caput('SATSY03-DLAC080-DHXP:posRY_comp', nanArray)
caput('SATSY03-DLAC080-DHXP:posRZ_comp', nanArray)

caput('SATSY03-DLAC080-DHXP:posLHU_comp', nanArray)
caput('SATSY03-DLAC080-DHXP:posLIN1Z_comp', nanArray)
caput('SATSY03-DLAC080-DHXP:posLIN2X_comp', nanArray)
caput('SATSY03-DLAC080-DHXP:posLIN2Z_comp', nanArray)
caput('SATSY03-DLAC080-DHXP:posLIN3X_comp', nanArray)
caput('SATSY03-DLAC080-DHXP:posLIN3Y_comp', nanArray)
caput('SATSY03-DLAC080-DHXP:posLIN3Z_comp', nanArray)
caput('SATSY03-DLAC080-DHXP:posLINTHZ_comp', nanArray)

#try:
#    caput('SATSY03-DLAC080-DOSC:SUMHIST', nanArray.astype(int), timeout = 0.5)
#    caput('SATSY03-DLAC080-DOSC:POSHIST', nanArray.astype(int), timeout = 0.5)
#except:
#    pass

sys.exit()
