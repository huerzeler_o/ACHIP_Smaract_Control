# -*- coding: utf-8 -*-
import time
from pcaspy import Driver, SimpleServer
from interfaces import ECM, hexapod, smaractLinear
import numpy as np
import threading
import queue
import logging
import traceback

# adapt this to make it runnable somewhere else than a raspberry pi
logging.basicConfig(format='%(asctime)s %(levelname)s %(name)s:%(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', level=logging.INFO, filename='/home/pi/Documents/ioc.log')
logger = logging.getLogger("ioc")

prefix = 'SATSY03-DLAC080-DHXP:' #PV prefix

pvdb = {
        'stopHex': {'type': 'int',
                    'value': 0},
        'sendRawHex': {'type': 'str',
                    'value': 'init'},
        'returnMsgHex': {'type': 'char',
                      'count': 100000},
        'sendButtonHex': {'type': 'int',
                        'value': 0},
        'connect': {'type': 'int',
                    'value': 0},
        'isConnected': {'type': 'int',
                        'value': 0},
        'isReferenced': {'type': 'int',
                        'value': 0},
        'reference': {'type': 'int',
                        'value': 0},
        'freqHEX': {'type': 'int',
                        'value': 5000},
        'velHEX': {'type': 'float',
                        'value': 1.0,
                        'prec': 5},
        'accHEX': {'type': 'float',
                        'value': 1},
        # actual position PVs
        'posX':{'type': 'float',
                'value': 0,
                'prec': 3},
        'posY':{'type': 'float',
                'value': 0,
                'prec': 3},
        'posZ':{'type': 'float',
                'value': 0,
                'prec': 3},
        'posRX':{'type': 'float',
                'value': 0,
                'prec': 5},
        'posRY':{'type': 'float',
                'value': 0,
                'prec': 5},
        'posRZ':{'type': 'float',
                'value': 0,
                'prec': 5},
        # the 6D array sent to the hexapod
        'hSet6d': {'type': 'float',
                'count': 6,
                'value': [0.,0.,0.,0.,0.,0.]},
        'moveToParkPos': {'type': 'int',
                'value': 0},
        'moveToScreenPos': {'type': 'int',
                'value': 0},
        # absolut position
        'absposX':{'type': 'float',
                'value': 0,
                'prec': 3},
        'absposY':{'type': 'float',
                'value': 0,
                'prec': 3},
        'absposZ':{'type': 'float',
                'value': 0,
                'prec': 3},
        'absposRX':{'type': 'float',
                'value': 0,
                'prec': 5},
        'absposRY':{'type': 'float',
                'value': 0,
                'prec': 5},
        'absposRZ':{'type': 'float',
                'value': 0,
                'prec': 5},
        # step size of each direction
        'stepposX':{'type': 'float',
                'value': 100,
                'prec': 3},
        'stepposY':{'type': 'float',
                'value': 100,
                'prec': 3},
        'stepposZ':{'type': 'float',
                'value': 100,
                'prec': 3},
        'stepposRX':{'type': 'float',
                'value': 1,
                'prec': 5},
        'stepposRY':{'type': 'float',
                'value': 1,
                'prec': 5},
        'stepposRZ':{'type': 'float',
                'value': 1,
                'prec': 5},
        # move step buttons
        'movestepposX':{'type': 'int',
                'value': 0},  
        'movestepposY':{'type': 'int',
                'value': 0}, 
        'movestepposZ':{'type': 'int',
                'value': 0}, 
        'movestepposRX':{'type': 'int',
                'value': 0}, 
        'movestepposRY':{'type': 'int',
                'value': 0}, 
        'movestepposRZ':{'type': 'int',
                'value': 0}, 
        # target position
        'tarposX':{'type': 'float',
                'value': 0,
                'prec': 3},
        'tarposY':{'type': 'float',
                'value': 0,
                'prec': 3},
        'tarposZ':{'type': 'float',
                'value': 0,
                'prec': 3},
        'tarposRX':{'type': 'float',
                'value': 0,
                'prec': 5},
        'tarposRY':{'type': 'float',
                'value': 0,
                'prec': 5},
        'tarposRZ':{'type': 'float',
                'value': 0,
                'prec': 5},
        'scanName': {'type': 'str',
                    'value': 'init'},
        'numberOfSteps': {'type': 'int',
                    'value': 5},
        'abortScan': {'type': 'int',
                    'value': 0},
        'HexapodMoving': {'type': 'int',
                    'value': 0},
        # MPS PV's
        'SwissFELmode': {'type': 'int',
                    'value': 1},
        'ParkPosition': {'type': 'int',
                    'value': 0},
        'ScreenPosition': {'type': 'int',
                    'value': 0},
        'Alarm': {'type': 'int',
                    'value': 0},
        'MPSTime': {'type': 'int',
                    'value': 0}, 
        'MPSStatusMessage': {'type': 'char',
                      'count': 100000},
        'ACHIPCharge1h': {'type': 'float',
                    'value': 0,
                    'prec': 8},
        'ACHIPCharge1hPrediction': {'type': 'float',
                    'value': 0,
                    'prec': 8},
      # Pivot
        'pivotX':{'type': 'float',
                'value': 0}, 
        'pivotY':{'type': 'float',
                'value': 0}, 
        'pivotZ':{'type': 'float',
                'value': 0},
        'setPivot':{'type': 'int',
                'value': 0}, 
        # Linear stages PV's
        'sendRawLIN': {'type': 'str',
                    'value': 'init'},
        'returnMsgLIN': {'type': 'char',
                      'count': 100000},
        'sendButtonLIN': {'type': 'int',
                        'value': 0},

        'posLHU':{'type': 'float',
                'value': 0,
                'prec': 3},
        'posLIN1Z':{'type': 'float',
                'value': 0,
                'prec': 3},
        'posLIN2X':{'type': 'float',
                'value': 0,
                'prec': 3},
        'posLIN2Z':{'type': 'float',
                'value': 0,
                'prec': 3},
        'posLIN3X':{'type': 'float',
                'value': 0,
                'prec': 3},
        'posLIN3Y':{'type': 'float',
                'value': 0,
                'prec': 3},
        'posLIN3Z':{'type': 'float',
                'value': 0,
                'prec': 3},
        'posLINTHZ':{'type': 'float',
                'value': 0,
                'prec': 3},
        
        'absposLHU':{'type': 'float',
                'value': 0,
                'prec': 3},
        'absposLIN1Z':{'type': 'float',
                'value': 0,
                'prec': 3},
        'absposLIN2X':{'type': 'float',
                'value': 0,
                'prec': 3},
        'absposLIN2Z':{'type': 'float',
                'value': 0,
                'prec': 3},
        'absposLIN3X':{'type': 'float',
                'value': 0,
                'prec': 3},
        'absposLIN3Y':{'type': 'float',
                'value': 0,
                'prec': 3},
        'absposLIN3Z':{'type': 'float',
                'value': 0,
                'prec': 3},
        'absposLINTHZ':{'type': 'float',
                'value': 0,
                'prec': 3},

        'setposLHU': {'type': 'int',
                'value': 0},
        'setposLIN1Z': {'type': 'int',
                'value': 0},
        'setposLIN2X': {'type': 'int',
                'value': 0},
        'setposLIN2Z': {'type': 'int',
                'value': 0},
        'setposLIN3X': {'type': 'int',
                'value': 0},
        'setposLIN3Y': {'type': 'int',
                'value': 0},
        'setposLIN3Z': {'type': 'int',
                'value': 0},
        'setposLINTHZ': {'type': 'int',
                'value': 0},        
        
     # step size of each direction
        'stepposLHU':{'type': 'float',
                'value': 100,
                'prec': 3},
        'stepposLIN1Z':{'type': 'float',
                'value': 100,
                'prec': 3},
        'stepposLIN2X':{'type': 'float',
                'value': 100,
                'prec': 3},
        'stepposLIN2Z':{'type': 'float',
                'value': 100,
                'prec': 3},
        'stepposLIN3X':{'type': 'float',
                'value': 100,
                'prec': 3},
        'stepposLIN3Y':{'type': 'float',
                'value': 100,
                'prec': 3},
        'stepposLIN3Z':{'type': 'float',
                'value': 100,
                'prec': 3},
        'stepposLINTHZ':{'type': 'float',
                'value': 100,
                'prec': 3},
                        
    # move step buttons
        'movestepposLHU':{'type': 'int',
                'value': 0},  
        'movestepposLIN1Z':{'type': 'int',
                'value': 0}, 
        'movestepposLIN2X':{'type': 'int',
                'value': 0}, 
        'movestepposLIN2Z':{'type': 'int',
                'value': 0}, 
        'movestepposLIN3X':{'type': 'int',
                'value': 0}, 
        'movestepposLIN3Y':{'type': 'int',
                'value': 0},
        'movestepposLIN3Z':{'type': 'int',
                'value': 0}, 
        'movestepposLINTHZ':{'type': 'int',
                'value': 0}, 

        # target position
        'tarposLHU':{'type': 'float',
                'value': 0,
                'prec': 3},
        'tarposLIN1Z':{'type': 'float',
                'value': 0,
                'prec': 3},
        'tarposLIN2X':{'type': 'float',
                'value': 0,
                'prec': 3},
        'tarposLIN2Z':{'type': 'float',
                'value': 0,
                'prec': 3},
        'tarposLIN3X':{'type': 'float',
                'value': 0,
                'prec': 3},
        'tarposLIN3Y':{'type': 'float',
                'value': 0,
                'prec': 3},
        'tarposLIN3Z':{'type': 'float',
                'value': 0,
                'prec': 3},
        'tarposLINTHZ':{'type': 'float',
                'value': 0,
                'prec': 3},
        
        'stopLIN': {'type': 'int',
                    'value': 0},
        
        'scanProgress': {'type': 'int',
                    'value': 0},
        'numberShots': {'type': 'int',
                    'value': 1},
        'setHomeValues':{'type': 'int',
                    'value': 1},
        }

class iocDriver(Driver):

    def __init__(self):
        super(iocDriver, self).__init__()
        self.ECM = ECM()
        self.hexpod = hexapod('hexapod', self.ECM)
        self.smaractLinear = smaractLinear('smaractLinear', self.ECM)

        # linear stages
        self.lH = smaractLinear('lH', self.ECM)
        self.l1 = smaractLinear('l1', self.ECM)
        self.l2 = smaractLinear('l2', self.ECM)
        self.l3 = smaractLinear('l3', self.ECM)
        self.lTHZ = smaractLinear('lTHZ', self.ECM)

        # set up a thread
        self.command_queue = queue.Queue()
        self.commander = threading.Thread(target=self.commander_thread)
        self.commander.setDaemon(True)
        self.commander.start()
        # position variables
        self.absolutePosition = np.array([0., 0., 0., 0., 0., 0.])
        self.targetPosition = np.array([0., 0., 0., 0., 0., 0.])
        self.pos = np.array([0., 0., 0., 0., 0., 0.], dtype = float)

        self.absolutePositionLIN = np.array([0., 0., 0., 0., 0., 0., 0.])
        self.targetPositionLIN = np.array([0., 0., 0., 0., 0., 0., 0.])
        self.supposedPosition = 0.
        self.savePosition = [0,0,0,0,0,0]
        self.pivot = np.array([0,0,0])
        self.factor = 10e5
        self.parkPosition = np.array([27000.,-8000.,0.,0.,22.,0.,0.,0.,0.,0.]) # x,y,z,rx,ry,rz,lhu,pivotX,pivotY,pivotZ
        self.screenPosition = np.array([0., -1150., -16500., 0., 0., 0., 25000.]) # x,y,z,rx,ry,rz,lhu
        self.firstPositionLoadedHEX = False
        self.firstPositionLoadedLIN = False

    def write(self, reason, val):
        status = True
        
        # move to park position only allow in SF Mode connected and referenced
        if reason == 'moveToParkPos':
            if self.getParam('SwissFELmode') and self.getParam('isConnected') and self.getParam('isReferenced'):
            
                self.setParam('tarposLIN3X', 0.)
                self.command_queue.put((self.l3.setPos, 'x', 0.))
                
                self.setParam('tarposLIN3Z', 0.)
                self.command_queue.put((self.l3.setPos, 'z', 0.))
    
                self.setParam('tarposLHU', 0.)
                self.command_queue.put((self.lH.setPos, 'u', 0.))
                
                
                self.setParam('pivotX', 0.)
                self.setParam('pivotY', 0.)
                self.setParam('pivotZ', 0.)
                self.command_queue.put((self.hexpod.setPivot, np.zeros(3)))
                
                self.absolutePosition = self.parkPosition.copy()[:6]
                self.setParam('tarposX', self.absolutePosition[0])
                self.setParam('tarposY', self.absolutePosition[1])
                self.setParam('tarposZ', self.absolutePosition[2])
                self.setParam('tarposRX', self.absolutePosition[3])
                self.setParam('tarposRY', self.absolutePosition[4])
                self.setParam('tarposRZ', self.absolutePosition[5])
                self.command_queue.put((self.move, self.absolutePosition))
        
        # mps interaction
        elif reason == 'SwissFELmode':
            self.setParam(reason, val)
        elif reason == 'ParkPosition':
            self.setParam(reason, val)
        elif reason == 'ScreenPosition':
            self.setParam(reason, val)
        elif reason == 'Alarm':
            self.setParam(reason, val)
        elif reason == 'MPSTime':
            self.setParam(reason, val)
        elif reason == 'MPSStatusMessage':
            self.setParam(reason, val)
        elif reason == 'ACHIPCharge1h':
            self.setParam(reason, val)
        elif reason == 'ACHIPCharge1hPrediction':
            self.setParam(reason, val)
                    
        # reject some write access if SwissFEL mode is True
        elif (not self.getParam('isConnected') or self.getParam('SwissFELmode')) and reason not in ['stopHex', 'connect']:
                logging.info('Change operation Mode!')
                return False
        
        # reject some write access if devices are not referenced
        elif not self.getParam('isReferenced') and reason not in ['reference', 'SwissFELmode', 'connect', 'sendRawHex', 'sendButtonHex', 'sendRawLIN', 'sendButtonLIN', 'isReferenced', 'returnMsgLIN', 'returnMsgHex']:
                logging.info('Reference the devices!')
                return False

        elif reason == 'reference':
            if val == True:
                self.command_queue.put(self.reference)
        elif reason == 'stopHex':
            if val == True:
                self.command_queue.put(self.hexpod.stopAll)

        elif reason == 'connect':
            if val == True:
                self.command_queue.put(self.connect)
                self.command_queue.put(self.isReferenced)
            else: 
                self.command_queue.put(self.disconnect)

        # absolute position PV's
        elif reason == 'absposX':
            self.writePositionArray(reason, val, 0)
        elif reason == 'absposY':
            self.writePositionArray(reason, val, 1)
        elif reason == 'absposZ':
            self.writePositionArray(reason, val, 2)
        elif reason == 'absposRX':
            self.writePositionArray(reason, val, 3)
        elif reason == 'absposRY':
            self.writePositionArray(reason, val, 4)
        elif reason == 'absposRZ':
            self.writePositionArray(reason, val, 5)

        # send position to hexapod
        elif reason == 'hSet6d':
            self.setParam(reason, self.absolutePosition)
            self.setParam('tarposX', self.absolutePosition[0])
            self.setParam('tarposY', self.absolutePosition[1])
            self.setParam('tarposZ', self.absolutePosition[2])
            self.setParam('tarposRX', self.absolutePosition[3])
            self.setParam('tarposRY', self.absolutePosition[4])
            self.setParam('tarposRZ', self.absolutePosition[5])
            self.command_queue.put((self.move, self.absolutePosition))


        elif reason == 'moveToScreenPos':
            
            self.setParam('absposLHU', self.screenPosition[6])
#            self.command_queue.put((self.lH.setPos, 'u', self.screenPosition[6]))
            
            self.setParam('tarposLHU', self.getParam('absposLHU'))
            self.command_queue.put((self.lH.setPos, 'u', self.getParam('absposLHU')/self.factor))
                                   
            self.absolutePosition = self.screenPosition.copy()[:6]
            self.setParam('tarposX', self.absolutePosition[0])
            self.setParam('tarposY', self.absolutePosition[1])
            self.setParam('tarposZ', self.absolutePosition[2])
            self.setParam('tarposRX', self.absolutePosition[3])
            self.setParam('tarposRY', self.absolutePosition[4])
            self.setParam('tarposRZ', self.absolutePosition[5])
            self.command_queue.put((self.move, self.absolutePosition))

        # step size PV's
        elif reason == 'stepposX':
            self.setParam(reason, val)
        elif reason == 'stepposY':
            self.setParam(reason, val)
        elif reason == 'stepposZ':
            self.setParam(reason, val)
        elif reason == 'stepposRX':
            self.setParam(reason, val)
        elif reason == 'stepposRY':
            self.setParam(reason, val)
        elif reason == 'stepposRZ':
            self.setParam(reason, val)
        
        # step size buttons
        elif reason == 'movestepposX':
            self.command_queue.put((self.calculatePosition, val, 'tarposX', 'stepposX', 0))
        elif reason == 'movestepposY':
            self.command_queue.put((self.calculatePosition, val, 'tarposY', 'stepposY', 1))
        elif reason == 'movestepposZ':
            self.command_queue.put((self.calculatePosition, val, 'tarposZ', 'stepposZ', 2))
        elif reason == 'movestepposRX':
            self.command_queue.put((self.calculatePosition, val, 'tarposRX', 'stepposRX', 3))
        elif reason == 'movestepposRY':
            self.command_queue.put((self.calculatePosition, val, 'tarposRY', 'stepposRY', 4))
        elif reason == 'movestepposRZ':
            self.command_queue.put((self.calculatePosition, val, 'tarposRZ', 'stepposRZ', 5))

        # PV's for the manual command window    
        elif reason == 'sendRawHex':
            self.setParam(reason, val)
        elif reason == 'sendButtonHex':
            if val == True:
                cmd = self.getParam('sendRawHex')
                self.command_queue.put((self.sendRawCommandHex, cmd))
        elif reason == 'returnMsgHex':
            self.setParam(reason, val)

        # settings hexapod
        elif reason == 'velHEX':
            meter = val/1000
            self.command_queue.put((self.hexpod.setVel, meter))
            logger.info('set velocity: ' + str(meter) + 'meter/s')
        elif reason == 'freqHEX':
            self.command_queue.put((self.hexpod.setFrq, val))
            logger.info('set frequency: ' + str(val))
        elif reason == 'accHEX':
            meter = val/1000
            self.command_queue.put((self.hexpod.setAcc, meter))
            logger.info('set acceleration: ' + str(meter) + 'meter/s2')
        elif reason == 'scanName':
            self.setParam(reason, val)
        elif reason == 'numberOfSteps':
            self.setParam(reason, val)
        elif reason == 'abortScan':
            self.setParam(reason, val)
        elif reason == 'HexapodMoving':
            self.setParam(reason, val)
    
        
        #elif reason == 'changeMode':
            #self.setParam(reason,val)

        # pivot PV's
        elif reason == 'pivotX':
            self.setParam(reason, val)
        elif reason == 'pivotY':
            self.setParam(reason, val)
        elif reason == 'pivotZ':
            self.setParam(reason, val)
        elif reason == 'setPivot':
            if val == True:
                pivot = np.array([self.getParam('pivotX'), self.getParam('pivotY'), self.getParam('pivotZ')])
                self.command_queue.put((self.hexpod.setPivot, pivot))
                self.setParam(reason, 0)
        
        # linear stage PV's
        elif reason == 'sendRawLIN':
            self.setParam(reason, val)
        elif reason == 'sendButtonLIN':
            if val == True:
                cmd = self.getParam('sendRawLIN')
                self.command_queue.put((self.sendRawCommandLin, cmd))   
        elif reason == 'returnMsgLIN':
            self.setParam(reason, val)
        
        elif reason == 'absposLHU':
            self.setParam(reason, val)
        elif reason == 'absposLIN1Z':
            self.setParam(reason, val)
        elif reason == 'absposLIN2X':
            self.setParam(reason, val)
        elif reason == 'absposLIN2Z':
            self.setParam(reason, val)
        elif reason == 'absposLIN3X':
            self.setParam(reason, val)
        elif reason == 'absposLIN3Y':
            self.setParam(reason, val)
        elif reason == 'absposLIN3Z':
            self.setParam(reason, val)
        elif reason == 'absposLINTHZ':
            self.setParam(reason, val)

        elif reason == 'setposLHU':
            self.setParam('tarposLHU', self.getParam('absposLHU'))
            self.command_queue.put((self.lH.setPos, 'u', self.getParam('absposLHU')/self.factor))
        elif reason == 'setposLIN1Z':
            self.setParam('tarposLIN1Z', self.getParam('absposLIN1Z'))
            self.command_queue.put((self.l1.setPos, 'z', self.getParam('absposLIN1Z')/self.factor))
        elif reason == 'setposLIN2X':
            self.setParam('tarposLIN2X', self.getParam('absposLIN2X'))
            self.command_queue.put((self.l2.setPos, 'x', self.getParam('absposLIN2X')/self.factor))
        elif reason == 'setposLIN2Z':
            self.setParam('tarposLIN2Z', self.getParam('absposLIN2Z'))
            self.command_queue.put((self.l2.setPos, 'z', self.getParam('absposLIN2Z')/self.factor))
        elif reason == 'setposLIN3X':
            self.setParam('tarposLIN3X', self.getParam('absposLIN3X'))
            self.command_queue.put((self.l3.setPos, 'x', self.getParam('absposLIN3X')/self.factor))
        elif reason == 'setposLIN3Y':
            self.setParam('tarposLIN3Y', self.getParam('absposLIN3Y'))
            self.command_queue.put((self.l3.setPos, 'y', self.getParam('absposLIN3Y')/self.factor))
            logging.info('Stage is not activated...')
        elif reason == 'setposLIN3Z':
            self.setParam('tarposLIN3Z', self.getParam('absposLIN3Z'))
            self.command_queue.put((self.l3.setPos, 'z', self.getParam('absposLIN3Z')/self.factor))
        elif reason == 'setposLINTHZ':
            self.setParam('tarposLINTHZ', self.getParam('absposLINTHZ'))
            self.command_queue.put((self.lTHZ.setPos, 'x', self.getParam('absposLINTHZ')/self.factor))

        # step size PV's
        elif reason == 'stepposLHU':
            self.setParam(reason, val)
        elif reason == 'stepposLIN1Z':
            self.setParam(reason, val)
        elif reason == 'stepposLIN2X':
            self.setParam(reason, val)
        elif reason == 'stepposLIN2Z':
            self.setParam(reason, val)
        elif reason == 'stepposLIN3X':
            self.setParam(reason, val)
        elif reason == 'stepposLIN3Y':
            self.setParam(reason, val)
        elif reason == 'stepposLIN3Z':
            self.setParam(reason, val)
        elif reason == 'stepposLINTHZ':
            self.setParam(reason, val)

        # step size PV's
        elif reason == 'tarposLHU':
            self.setParam(reason, val)
        elif reason == 'tarposLIN1Z':
            self.setParam(reason, val)
        elif reason == 'tarposLIN2X':
            self.setParam(reason, val)
        elif reason == 'tarposLIN2Z':
            self.setParam(reason, val)
        elif reason == 'tarposLIN3X':
            self.setParam(reason, val)
        elif reason == 'tarposLIN3Y':
            self.setParam(reason, val)
        elif reason == 'tarposLIN3Z':
            self.setParam(reason, val)
        elif reason == 'tarposLINTHZ':
            self.setParam(reason, val)

        # step size buttons
        elif reason == 'movestepposLHU':
            if val == False:
                self.supposedPosition = self.getParam('tarposLHU') - self.getParam('stepposLHU')
                self.setParam('tarposLHU', self.supposedPosition)
            if val == True:
                self.supposedPosition = self.getParam('tarposLHU') + self.getParam('stepposLHU')
                self.setParam('tarposLHU', self.supposedPosition)
            self.command_queue.put((self.lH.setPos, 'u', self.getParam('tarposLHU')/self.factor))
        elif reason == 'movestepposLIN1Z':
            if val == False:
                self.supposedPosition = self.getParam('tarposLIN1Z') - self.getParam('stepposLIN1Z')
                self.setParam('tarposLIN1Z', self.supposedPosition)
            if val == True:
                self.supposedPosition = self.getParam('tarposLIN1Z') + self.getParam('stepposLIN1Z')
                self.setParam('tarposLIN1Z', self.supposedPosition)
            self.command_queue.put((self.l1.setPos, 'z', self.getParam('tarposLIN1Z')/self.factor))
        elif reason == 'movestepposLIN2X':
            if val == False:
                self.supposedPosition = self.getParam('tarposLIN2X') - self.getParam('stepposLIN2X')
                self.setParam('tarposLIN2X', self.supposedPosition)
            if val == True:
                self.supposedPosition = self.getParam('tarposLIN2X') + self.getParam('stepposLIN2X')
                self.setParam('tarposLIN2X', self.supposedPosition)
            self.command_queue.put((self.l2.setPos, 'x', self.getParam('tarposLIN2X')/self.factor))
        elif reason == 'movestepposLIN2Z':
            if val == False:
                self.supposedPosition = self.getParam('tarposLIN2Z') - self.getParam('stepposLIN2Z')
                self.setParam('tarposLIN2Z', self.supposedPosition)
            if val == True:
                self.supposedPosition = self.getParam('tarposLIN2Z') + self.getParam('stepposLIN2Z')
                self.setParam('tarposLIN2Z', self.supposedPosition)
            self.command_queue.put((self.l2.setPos, 'z', self.getParam('tarposLIN2Z')/self.factor))
        elif reason == 'movestepposLIN3X':
            if val == False:
                self.supposedPosition = self.getParam('tarposLIN3X') - self.getParam('stepposLIN3X')
                self.setParam('tarposLIN3X', self.supposedPosition)
            if val == True:
                self.supposedPosition = self.getParam('tarposLIN3X') + self.getParam('stepposLIN3X')
                self.setParam('tarposLIN3X', self.supposedPosition)
            self.command_queue.put((self.l3.setPos, 'x', self.getParam('tarposLIN3X')/self.factor))
        elif reason == 'movestepposLIN3Y':
            if val == False:
                self.supposedPosition = self.getParam('tarposLIN3Y') - self.getParam('stepposLIN3Y')
                self.setParam('tarposLIN3Y', self.supposedPosition)
            if val == True:
                self.supposedPosition = self.getParam('tarposLIN3Y') + self.getParam('stepposLIN3Y')
                self.setParam('tarposLIN3Y', self.supposedPosition)
            self.command_queue.put((self.l3.setPos, 'y', self.getParam('tarposLIN3Y')/self.factor))
        elif reason == 'movestepposLIN3Z':
            if val == False:
                self.supposedPosition = self.getParam('tarposLIN3Z') - self.getParam('stepposLIN3Z')
                self.setParam('tarposLIN3Z', self.supposedPosition)
            if val == True:
                self.supposedPosition = self.getParam('tarposLIN3Z') + self.getParam('stepposLIN3Z')
                self.setParam('tarposLIN3Z', self.supposedPosition)
            self.command_queue.put((self.l3.setPos, 'z', self.getParam('tarposLIN3Z')/self.factor))
        elif reason == 'movestepposLINTHZ':
            if val == False:
                self.supposedPosition = self.getParam('tarposLINTHZ') - self.getParam('stepposLINTHZ')
                self.setParam('tarposLINTHZ', self.supposedPosition)
            if val == True:
                self.supposedPosition = self.getParam('tarposLINTHZ') + self.getParam('stepposLINTHZ')
                self.setParam('tarposLINTHZ', self.supposedPosition)
            self.command_queue.put((self.lTHZ.setPos, 'x', self.getParam('tarposLINTHZ')/self.factor))

        
        elif reason == 'stopLIN':
            if val == True:
                self.command_queue.put((self.lH.stop, 'u'))
                self.command_queue.put((self.l1.stop, 'z'))
                self.command_queue.put((self.l2.stop, 'x'))
                self.command_queue.put((self.l2.stop, 'z'))
                self.command_queue.put((self.l3.stop, 'x'))
                self.command_queue.put((self.l3.stop, 'y'))
                self.command_queue.put((self.l3.stop, 'z'))
                self.command_queue.put((self.lTHZ.stop, 'x'))
        
        elif reason == 'scanProgress':
            if val == True:
                self.setParam(reason, val)
        elif reason == 'numberShots':
            if val == True:
                self.setParam(reason, val)
        
        elif reason == 'setHomeValues':
            if val == True:
                self.writePositionArray('absposX', 27000, 0)
                self.writePositionArray('absposY', -8000, 1)
                self.writePositionArray('absposZ', 0, 2)
                self.writePositionArray('absposRX', 0, 3)
                self.writePositionArray('absposRY', 22, 4)
                self.writePositionArray('absposRZ', 0, 5)
            
        else:
            status = False
        if status:
            self.setParam(reason, val)
            self.updatePVs()
        return status

    # send command to controller
    def commander_thread(self):
        while True:
            try:
                command = self.command_queue.get(timeout=0.1)
            except queue.Empty:
                # if no command, poll the positions
                try:
                    if self.getActualPosition():
                        self.updatePVs()
                except:
                    logger.exception('Failed position readout.')
                    traceback.print_exc()
                continue
            # execute commands in the queue
            if isinstance(command, tuple) and len(command) == 2:
                func, arg = command
                try:
                    func(arg)
                    self.updatePVs()
                except:
                    logger.exception('Error in calling function "%s(%s)"' % (func, arg))

            elif isinstance(command, tuple) and len(command) == 3:
                func, arg, arg2 = command
                try:
                    func(arg, arg2)
                    self.updatePVs()
                except:
                    logger.exception('Error in calling function "%s(%s, %s)"' % (func, arg, arg2))
            
            elif isinstance(command, tuple) and len(command) == 5:
                func, arg, arg2, arg3, arg4 = command
                try:
                    func(arg, arg2, arg3, arg4)
                    self.updatePVs()
                except:
                    logger.exception('Error in calling function "%s(%s, %s, %s, %s)"' % (func, arg, arg2, arg3, arg4))
                
            else:
                func = command
                try:
                    func()
                    self.updatePVs()
                except:
                    logger.exception('Error in calling function "%s"' % func)

    def connect(self):
        if not self.ECM.connect():
            return 
        if not self.hexpod.connect():
            return
        if not self.lH.connect():
            return
        if not self.l1.connect():
            return
        if not self.l2.connect():
            return
        if not self.l3.connect():
            return
        self.setParam('isConnected', 1)
        
    def reference(self):
        if not self.l1.home('z', ' f'):
            return
        while self.l1.movState('z') == 7:
            logging.info('stage lin1 Z is moving')
            time.sleep(1)
            
        if not self.l2.home('x', ' b'):
            return
        while self.l2.movState('x') == 7:
            logging.info('stage lin2 X is moving.')
            time.sleep(1)

        if not self.l2.home('z', ' f'):
            return
        while self.l2.movState('z') == 7:
            logging.info('stage lin2 Z is moving')
            time.sleep(1)

        if not self.l3.home('x', ' b'):
            return
        while self.l3.movState('x') == 7:
            logging.info('stage lin3 X is moving')
            time.sleep(1)

        if not self.l3.home('y', ' f'):
            return
        while self.l3.movState('y') == 7:
            logging.info('stage lin3 Y is moving')
            time.sleep(1)

        if not self.l3.home('z', ' f'):
            return
        while self.l3.movState('z') == 7:
            logging.info('stage lin3 Z is moving')
            time.sleep(1)

        if not self.lTHZ.home('x', ' f'):
            return
        while self.lTHZ.movState('x') == 7:
            logging.info('stage linTHZ X is moving')
            time.sleep(1)

        if not self.hexpod.home():
            logging.warning('failed referencing hexapod')
            return
        while not self.hexpod.isNotMoving():
            logging.info('hexapod is referencing')
            time.sleep(1)
        
        if not self.lH.home('u', ' f'):
            return
        while self.lH.movState('u') == 7:
            logging.info('stage lH U is moving')
        
        logging.info('\n' + '*********** Referenced ***********'+ '\n')
        self.setParam('isReferenced', 1)
        self.updatePVs()

    def isReferenced(self):
        ref = True
        if not self.l1.isHome('z'):
            logging.info('lin1z not referenced')
            ref = False
            #return
        if not self.l2.isHome('x'):
            logging.info('lin2x not referenced')
            ref = False
            #return
        if not self.l2.isHome('z'):
            logging.info('lin2z not referenced')
            ref = False
            #return
        if not self.l3.isHome('x'):
            logging.info('lin3x not referenced')
            ref = False
            #return
        if not self.l3.isHome('y'):
            logging.info('lin3y not referenced')
            ref = False
            #return
        if not self.l3.isHome('z'):
            logging.info('lin3z not referenced')
            ref = False
            #return        
        if not self.lTHZ.isHome('x'):
            logging.info('lin3THZ not referenced')
#            ref = False
            #return   
        
        if not self.hexpod.isHome():
            logging.info('Hexapod is not referenced')
            ref = False
            return
        if not self.lH.isHome('u'):
            logging.info('lH is not referenced.')
            ref = False
            #return
        if ref == True:
            self.setParam('isReferenced', 1)

    def disconnect(self):
        self.ECM.disconnect()
        self.hexpod.disconnect()
        self.smaractLinear.disconnect()
        self.setParam('isConnected', 0)

    # poll current positions
    def getActualPosition(self):
        if not self.getParam('isConnected'):
            self.command_queue.put(self.connect)
            self.command_queue.put(self.isReferenced)

        if not self.getParam('isReferenced'):
            return False 

        try:
            output = self.hexpod.get6d()
            self.pos = output
            
            self.setParam('posX', self.pos[0])
            self.setParam('posY', self.pos[1])
            self.setParam('posZ', self.pos[2])
            self.setParam('posRX', self.pos[3])
            self.setParam('posRY', self.pos[4])
            self.setParam('posRZ', self.pos[5])

            if self.hexpod.isNotMoving() == True:
                self.setParam('HexapodMoving', 0)
            else:
                self.setParam('HexapodMoving', 1)

            if not self.firstPositionLoadedHEX:
                self.writePositionArray('absposX', round(self.pos[0], 6),0)
                self.writePositionArray('absposY', round(self.pos[1], 6),1)
                self.writePositionArray('absposZ', round(self.pos[2], 6),2)
                self.writePositionArray('absposRX', round(self.pos[3], 6),3)
                self.writePositionArray('absposRY', round(self.pos[4], 6),4)
                self.writePositionArray('absposRZ', round(self.pos[5], 6),5)
                self.setParam('tarposX', self.absolutePosition[0])
                self.setParam('tarposY', self.absolutePosition[1])
                self.setParam('tarposZ', self.absolutePosition[2])
                self.setParam('tarposRX', self.absolutePosition[3])
                self.setParam('tarposRY', self.absolutePosition[4])
                self.setParam('tarposRZ', self.absolutePosition[5])
                print(self.absolutePosition)
                self.firstPositionLoadedHEX = True
        except:
            logger.exception('Error in get hexapod positions: ' + str(self.pos))
            traceback.print_exc()
            return False

        # linear stages
        try:
            self.filterData('posLHU', self.lH.getPos('u'))
            self.filterData('posLIN1Z', self.l1.getPos('z'))
            self.filterData('posLIN2X', self.l2.getPos('x'))
            self.filterData('posLIN2Z', self.l2.getPos('z'))
            self.filterData('posLIN3X', self.l3.getPos('x'))
            self.filterData('posLIN3Y', self.l3.getPos('y'))
            self.filterData('posLIN3Z', self.l3.getPos('z'))
            self.filterData('posLINTHZ', self.lTHZ.getPos('x'))

            if not self.firstPositionLoadedLIN:
                self.setParam('absposLHU', self.lH.getPos('u'))
                self.setParam('absposLIN1Z', self.l1.getPos('z'))
                self.setParam('absposLIN2X', self.l2.getPos('x'))
                self.setParam('absposLIN2Z', self.l2.getPos('z'))
                self.setParam('absposLIN3X', self.l3.getPos('x'))
                self.setParam('absposLIN3Y', self.l3.getPos('y'))
                self.setParam('absposLIN3Z', self.l3.getPos('z'))
                self.setParam('absposLINTHZ', self.lTHZ.getPos('x'))

                self.setParam('tarposLHU', self.lH.getPos('u'))
                self.setParam('tarposLIN1Z', self.l1.getPos('z'))
                self.setParam('tarposLIN2X', self.l2.getPos('x'))
                self.setParam('tarposLIN2Z', self.l2.getPos('z'))
                self.setParam('tarposLIN3X', self.l3.getPos('x'))
                self.setParam('tarposLIN3Y', self.l3.getPos('y'))
                self.setParam('tarposLIN3Z', self.l3.getPos('z'))
                self.setParam('tarposLINTHZ', self.lTHZ.getPos('x'))
                self.firstPositionLoadedLIN = True

        except:
            logger.exception('Error in get linear stages positions')
            return False

        # check if hexapod is in park position
        try:
            deltaPos = np.abs(self.parkPosition[0:3] - self.pos[[0,1,2]])
            deltaAng = np.abs(self.parkPosition[3:6] - self.pos[[3,4,5]])
            deltalH = np.abs(self.parkPosition[6] - self.getParam('posLHU'))
            pivot = np.array([float(self.getParam('pivotX')),float(self.getParam('pivotY')),float(self.getParam('pivotZ'))])
            deltaPivot = np.abs(self.parkPosition[7:10] - pivot[0:3])
            if np.logical_and(np.all(deltaPos <= 2), np.all(deltaAng <= 0.1)) and np.all(deltaPivot <= 0.1) and deltalH <= 2:
                self.setParam('ParkPosition', 1)
            else:
                self.setParam('ParkPosition', 0)
        except:
            logger.exception('Error in comparing park position.')
            return False

        # check if hexapod is in screen position
        try:
            deltaPos = np.abs(self.screenPosition[0:3] - self.pos[[0,1,2]])
            deltaAng = np.abs(self.screenPosition[3:6] - self.pos[[3,4,5]])
            deltalH = np.abs(self.screenPosition[6] - self.getParam('posLHU'))
           
            if np.logical_and(np.all(deltaPos <= 2), np.all(deltaAng <= 0.1)) and deltalH <= 2:
                self.setParam('ScreenPosition', 1)
            else:
                self.setParam('ScreenPosition', 0)
        except:
            logger.exception('Error in comparing screen position.')
            return False

        return True

    # define some useful functions
    def strToChr(self, msg): # convert string to character array
        return [ord(s) for s in msg]

    def chrToStr(self, msg): # convert character array to string
        return ''.join([chr(s) for s in msg])

    # corresponds to the absolutePosition[]
    def writePositionArray(self, reason, val, number):
        self.setParam(reason, val)
        self.absolutePosition[number] = val
        self.setParam('hSet6d', self.absolutePosition)

    # calculates and executes the position when a stepwise motion is done
    def calculatePosition(self, val, targetPosition, stepPosition, number):
        self.targetPosition[0] = self.getParam('tarposX')
        self.targetPosition[1] = self.getParam('tarposY')
        self.targetPosition[2] = self.getParam('tarposZ')
        self.targetPosition[3] = self.getParam('tarposRX')
        self.targetPosition[4] = self.getParam('tarposRY')
        self.targetPosition[5] = self.getParam('tarposRZ')
        if val == False:
            position = self.getParam(targetPosition) - self.getParam(stepPosition)
            self.setParam(targetPosition, position)
        if val == True:
            position = self.getParam(targetPosition) + self.getParam(stepPosition)
            self.setParam(targetPosition, position)
        self.targetPosition[number] = position
        self.move(self.targetPosition)
        self.updatePVs

    def sendRawCommandHex(self, cmd):
        retMsg = self.hexpod.send(cmd)
        self.setParam('returnMsgHex', self.strToChr(retMsg))
    def sendRawCommandLin(self, cmd):
        retMsg = self.ECM.send(0, cmd)
        self.setParam('returnMsgLIN', self.strToChr(retMsg))

    def filterData(self, channel, value):
        temp = value
        if temp != None:
            self.setParam(channel, temp)
        else:
            logging.warning('Didnt read oud channel: ' + str(channel) + ', value: ' + str(value))

    def move(self, array):
        self.hexpod.set6d(array)
        logger.info('Move to position: ' + str(array))

if __name__ == '__main__': #create PVs based on prefix and pvdb definition
    server = SimpleServer()
    server.createPV(prefix, pvdb)
    driver = iocDriver()
    while True:
        server.process(0.1) # process CA transactions
