# -*- coding: utf-8 -*-
from time import sleep
import numpy as np
import socket
import traceback
import logging

class ECM():
    """ ECM (embedded control module) - Smaract Motor Controller """
    def __init__(self):
        #self.TCP_IP = '129.129.217.74'
        self.TCP_IP = 'ECM-00000029.psi.ch'  # hostname
        self.TCP_PORT = 2000
        self.ret = '' # last return message
        self.isConnected = False
              
    def connect(self):
        try:
            self.soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.soc.connect((self.TCP_IP, self.TCP_PORT))
            self.isConnected = True
            return True
        except:
            logging.exception('Check ECM connection.')
            self.isConnected = False
            return False

    def disconnect(self):
        self.soc.close()
        self.isConnected = False
        return True
        
    def sendRaw(self, cmd):
        if self.isConnected:
            try:
                self.soc.send(bytes((cmd+'\n').encode()))
                retMsg = self.soc.recv(4096).decode('UTF-8')
                # error handling
                if retMsg[0] == '!' and retMsg != '!0\r\n':
                    retMsg = '!Error code: ' + retMsg
                    self.ret = retMsg
                    logging.warning(retMsg)
                    logging.warning('auto reconnet started')
                    self.disconnect()
                    sleep(0.01)
                    self.connect()
            except:
                self.ret = 'ECM Communication Error'
                retMsg = self.ret
        else:
            self.ret = 'Check ECM connection.'
            retMsg = self.ret
        return retMsg
    
    def send(self, unit, cmd):
        returnMessage = self.setUnit(unit)
        if returnMessage == '!0\r\n': 
            return self.sendRaw(cmd)
        else:
            return returnMessage
        
    def setUnit(self, unit):
        cmd = '%unit '+str(unit)
        return self.sendRaw(cmd)

class hexapod():

    '''A class for hexapod control with ASCII Commands'''
    def __init__(self, name, ECM):
        self.mainName = name
        self.ECM = ECM
        self.unit = '1'
        self.stdFrq = 5000
        self.signConversion = np.array([-1,1,-1,-1,1,-1])
        self.factor = 10e5
    
    # change name to activate
    def connect(self):
        if self.ECM.sendRaw('%unit-activated? '+self.unit) == '0\r\n':
            self.ECM.sendRaw('%activate-unit '+self.unit)
        return True
    
    def disconnect(self):
        if self.ECM.sendRaw('%unit-activated? '+self.unit) == '1\r\n':
            self.ECM.sendRaw('%deactivate-unit '+self.unit)
        return True

    def send(self, cmd):
        return self.ECM.send(self.unit, cmd)
        
    def get6d(self):
        # pos contains coordinates in x, y, z, rx, ry, rz
        # posECM in z, x, y, rz, rx, ry (internal coordinates)
        pos = np.zeros(6)
        posECM = self.send('pos?')
        if posECM[0]!='!':
            string = posECM.split(' ')[:6]
            posECMArray = np.array(string, dtype = float)
            pos[2] = posECMArray[0] * self.factor
            pos[0] = posECMArray[1] * self.factor
            pos[1] = posECMArray[2] * self.factor
            pos[5] = posECMArray[3]
            pos[3] = posECMArray[4]
            pos[4] = posECMArray[5]
            pos *= self.signConversion
            return pos
        else:
            logging.error('failed hexapod position read out, output: ' + str(posECM))
            self.send('%deactivate-unit 1')
            self.send('%activate-unit 1')


    def set6d(self, pos):
        # pos contains coordinates in x, y, z, rx, ry, rz
        # posECM in z, x, y, rz, rx, ry (internal coordinates)
        target = pos.copy()
        target[0:3] /= self.factor
        target *= self.signConversion
        posECM = np.zeros(6)
        posECM[0] = target[2]    # position 1 corresponds to the z value
        posECM[1] = target[0]    # position 2 corresponds to the x value
        posECM[2] = target[1]    # position 3 corresponds to the y value
        posECM[3] = target[5]    # position 4 corresponds to the rz value (roll)
        posECM[4] = target[3]    # position 5 corresponds to the rx value (pitch)
        posECM[5] = target[4]    # position 6 corresponds to the ry value (yaw)
        
        if self.isPosReachable(posECM):
            cmd = 'mov '
            for c in posECM:
                cmd += str(c)+' '
            return self.send(cmd)
    

        
    def isPosReachable(self, posECM):
        #posECM in z, x, y, rz, rx, ry (internal coordinates)
        cmd = 'rea? '
        for c in posECM:
            cmd+= str(c)+' '
        if self.send(cmd) == '1\r\n':
            return True
        else:
            logging.info('Position not reachable. command: ' + str(cmd) + ', pos: ' + str(posECM))
            return False

    def stopAll(self):
        cmd = 'stop'
        logging.info('Stopped.')
        return self.send(cmd)

    def setFrq(self, f):
        cmd = 'frq ' + str(f)
        return self.send(cmd)

    def setAcc(self, a):
        cmd = 'acc ' + str(a)
        return self.send(cmd)
    
    def setVel(self, a):
        cmd = 'vel ' + str(a)
        return self.send(cmd)

    def isHome(self):
        cmd = 'ref?'
        if self.send(cmd) == '1\r\n':
            return True
        logging.info('Not referenced.')
        return False

    def home(self):
        cmd = 'ref'
        if self.send(cmd) == '!0\r\n':
            logging.info('referencing hexapod.')
            return True
        else:
            return False

    def setPivot(self, piv):
        # piv contains all 3 pivot coordinates in x, y, z
        # pivECM contains all 3 pivot coordinates in z, x, y (internal coordinates)
        cmd = 'piv '
        piv = (piv/self.factor)*self.signConversion[0:3]
        pivECM = np.zeros(3)
        pivECM[0] = piv[2]    # position 1 corresponds to the z value
        pivECM[1] = piv[0]    # position 2 corresponds to the x value
        pivECM[2] = piv[1]    # position 3 corresponds to the y value
        for c in pivECM:
            cmd += str(c)+' '
        return self.send(cmd)   

    def isNotMoving(self):
        cmd = 'mst?'
        ret = self.send(cmd)
        if ret == '0\r\n' or ret == '1\r\n':
            return True
        else:
            return False

    def getPivot(self):
        # piv contains all 3 pivot coordinates
        cmd = 'piv?'  
        piv = self.send(cmd)  
        if piv[0]!='!':
            return piv.split(' ')[:3]
        else:
            return [None, None, None]
    
    
class smaractLinear():
    '''A class for Smaract linear control with ASCII Commands'''
    def __init__(self, name, ECM):
        self.mainName = name
        self.ECM = ECM
        self.unit = '0'
        self.stdFrq = 5000
        if name == 'l1':
            self.axes = ['z']
            self.chForECM = {'z': 8}
            self.unitConversion = {'z': 1e-6}
            self.signConversion = {'z': -1}
        if name == 'l2':
            self.axes = ['x', 'z']
            self.chForECM = {'x': 6, 'z': 7}
            self.unitConversion = {'x': 1e-6, 'z': 1e-6}
            self.signConversion = {'x': 1, 'z': -1}
        if name == 'l3':
            self.axes = ['x', 'y', 'z']
            self.chForECM = {'x': 1, 'y': 2, 'z': 0}
            self.unitConversion = {'x': 1e-6, 'y': 1e-6, 'z': 1e-6}
            self.signConversion = {'x': -1, 'y': 1, 'z':1}
        if name == 'lH':
            self.axes = ['u']
            self.chForECM = {'u': 3}
            self.unitConversion = {'u': 1e-6}
            self.signConversion = {'u': -1}
        if name == 'lTHZ':
            self.axes = ['x']
            self.chForECM = {'x': 4}
            self.unitConversion = {'x': 1e-6}
            self.signConversion = {'x': 1}
    
    def connect(self):
        if self.ECM.sendRaw('%unit-activated? '+self.unit) == '0\r\n':
            self.ECM.sendRaw('%activate-unit '+self.unit)
        return True
    
    def disconnect(self):
        if self.ECM.sendRaw('%unit-activated? '+self.unit) == '1\r\n':
            self.ECM.sendRaw('%deactivate-unit '+self.unit)    
        return True

    def isHome(self, axis):
        cmd = 'ref? ' + str(self.chForECM[axis])
        if self.send(cmd) == '1\r\n':
            return True
        return False

    def send(self, cmd):
        return self.ECM.send(self.unit, cmd)

    def home(self, axis, dir):
        if axis == 'z':
            cmd = 'ref ' + str(self.chForECM[axis]) + dir + ' 0'
        else:
            cmd = 'ref ' + str(self.chForECM[axis]) + dir + ' 1'
        if self.send(cmd) == '!0\r\n':
            return True
        else:
            return False

    def getPos(self, ch):
        cmd = 'pos? ' + str(self.chForECM[ch])
        pos = self.send(cmd)
        if pos[0] != '!':
            pos = float(pos) / self.unitConversion[ch] * self.signConversion[ch]
            return pos
        else:
            logging.error('Failed read position of: Linear stage ' + str(self.chForECM[ch]) + ', value: ' + str(pos))
            self.send('%deactivate-unit 0')
            self.send('%activate-unit 0')
           
        
    def setPos(self, ch, pos):
        pos = pos * self.signConversion[ch]
        cmd = 'mpa ' + str(self.chForECM[ch]) + ' ' + str(pos)
        return self.send(cmd)

    def stop(self, ch):
        cmd = 'stop ' + str(self.chForECM[ch])
        output = self.send(cmd)
        if output == '!0\r\n':
            logging.info('Stage channel '+ str(ch) + ': stopped')
            return True
        else:
            logging.error('Error in stopping stage with channel: ' + str(ch) + ' output: ' + output)
            return False

    
    def setFrq(self, ch, f):
        cmd = 'frq ' + str(self.chForECM[ch]) + ' ' + str(f)
        return self.send(cmd)
        
    def movState(self, ch):
        cmd = 'sta? ' + str(self.chForECM[ch])
        ref = float(self.send(cmd))
        logging.info('mov state: '+ str(self.chForECM[ch]) + ' ' + str(ref))
        return ref


         


