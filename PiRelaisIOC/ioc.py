# -*- coding: utf-8 -*-
from pcaspy import Driver, SimpleServer
import threading
import Queue as queue
from time import sleep
import pifacedigitalio

# ioc to control shutters via relais on pi, piface board

prefix = 'SATSY03-DLAC080-DSHU:' #PV prefix

pvdb = {}

pvdb['S1OPEN'] = {'type': 'float','value': 0}
pvdb['S1CLOSE'] = {'type': 'float','value': 0}
pvdb['S2OPEN'] = {'type': 'float','value': 0}
pvdb['S2CLOSE'] = {'type': 'float','value': 0}


class iocDriver(Driver):

    def __init__(self):
        super(iocDriver, self).__init__()
        
        # set up a thread
        self.command_queue = queue.Queue()
        self.commander = threading.Thread(target=self.commander_thread)
        self.commander.setDaemon(True)
        self.commander.start()
        self.pifacedigital = pifacedigitalio.PiFaceDigital()
        self.R1 = self.pifacedigital.output_pins[0]
        self.R2 = self.pifacedigital.output_pins[1]
        
    def openR(self, R):
        R.turn_off()
 
    def closeR(self, R):
        R.turn_on()
        
    def write(self, reason, val):
        status = True
        
        if 'S1' in reason:
            if 'OPEN' in reason:
                self.command_queue.put((self.openR, self.R1))
            if 'CLOSE' in reason:
                self.command_queue.put((self.closeR, self.R1))        
        
        elif 'S2' in reason:
            if 'OPEN' in reason:
                self.command_queue.put((self.openR, self.R2))
            if 'CLOSE' in reason:
                self.command_queue.put((self.closeR, self.R2))        
        
        else:
            status=False
       
               
        if status:
            self.setParam(reason, val)
            self.updatePVs()
        return status

    # send command to controller
    def commander_thread(self):
        while True:
            try:
                command = self.command_queue.get(timeout=0.05)
            except queue.Empty:
                continue
            # execute commands in the queue
            if isinstance(command, tuple) and len(command) == 2:
                func, arg = command
                try:
                    func(arg)
                    self.updatePVs()
                except:
                    print('Error in calling function "%s(%s)"' % (func, arg))
                    
            else:
                func = command
                try:
                    func()
                    self.updatePVs()
                except:
                    print('Error in calling function "%s"' % func)


if __name__ == '__main__': #create PVs based on prefix and pvdb definition
    server = SimpleServer()
    server.createPV(prefix, pvdb)
    driver = iocDriver()
    while True:
        server.process(0.05) # process CA transactions
