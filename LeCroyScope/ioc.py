# -*- coding: utf-8 -*-
from pcaspy import Driver, SimpleServer
#from epics import caput, caget
import numpy as np
import threading
import Queue as queue
from time import sleep

prefix = 'SATSY03-DLAC080-DOSC:' #PV prefix


waveformLength=2048
histLength=3000
pvdb = {}
    
pvdb['SUM'] = {'type': 'float','value': 0, 'prec':5}
pvdb['POS'] = {'type': 'float','value': 0, 'prec':5}
pvdb['SUMHIST'] = {'type': 'int', 'count': histLength}
pvdb['POSHIST'] = {'type': 'int', 'count': histLength}

pvdb['WAVEFORM'] = {'type': 'int', 'count': waveformLength}
pvdb['WAVEFORMIDX'] = {'type': 'int', 'count': waveformLength}

class iocDriver(Driver):

    def __init__(self):
        super(iocDriver, self).__init__()
        
        self.sumhist = np.zeros(histLength, dtype=int)
        self.poshist = np.zeros(histLength, dtype=int)
        self.sumhist[:] = np.nan
        self.poshist[:] = np.nan
        # set up a thread
        self.command_queue = queue.Queue()
        self.commander = threading.Thread(target=self.commander_thread)
        self.commander.setDaemon(True)
        self.commander.start()
        self.setParam('WAVEFORMIDX', np.arange(waveformLength))
        self.updatePVs()
        
    def write(self, reason, val):
        status = True
        if reason == 'SUM':
            self.sumhist = self.getParam('SUMHIST')
            self.sumhist = np.roll(self.sumhist, 1)
            self.sumhist[0] = int(val)
            self.setParam('SUMHIST', self.sumhist)
        if reason == 'POS':
            self.poshist = self.getParam('POSHIST')
            self.poshist = np.roll(self.poshist, 1)
            self.poshist[0] = int(val)
            self.setParam('POSHIST', self.poshist)
            
        if status:
            self.setParam(reason, val)
            self.updatePVs()
        return status

    # send command to controller
    def commander_thread(self):
        while True:
            try:
                command = self.command_queue.get(timeout=0.05)
            except queue.Empty:
                continue
            # execute commands in the queue
            if isinstance(command, tuple) and len(command) == 2:
                func, arg = command
                try:
                    func(arg)
                    self.updatePVs()
                except:
                    print('Error in calling function "%s(%s)"' % (func, arg))
                    
            if isinstance(command, tuple) and len(command) == 3:
                func, arg1, arg2 = command
                try:
                    func(arg1, arg2)
                    self.updatePVs()
                except:
                    print('Error in calling function "%s(%s)"' % (func, arg1, arg2))
            
            if isinstance(command, tuple) and len(command) == 4:
                func, arg1, arg2, arg3 = command
                try:
                    func(arg1, arg2, arg3)
                    self.updatePVs()
                except:
                    print('Error in calling function "%s(%s)"' % (func, arg1, arg2, arg3))
            else:
                func = command
                try:
                    func()
                    self.updatePVs()
                except:
                    print('Error in calling function "%s"' % func)


if __name__ == '__main__': #create PVs based on prefix and pvdb definition
    server = SimpleServer()
    server.createPV(prefix, pvdb)
    driver = iocDriver()
    while True:
        server.process(0.1) # process CA transactions
