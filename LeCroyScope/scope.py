import visa
import pylab as pl
import numpy as np
from time import time

class LeCroy():    
    def __init__(self, ):         
        self._rm = visa.ResourceManager()
        self.IPstr = 'LCRY0715N47498'
        self.dso = self._rm.open_resource('TCPIP0::'+self.IPstr+'::INSTR')
        self.dso.write("chdr off")
        self.vdiv = self.dso.query("c1:vdiv?")
        self.ofst = self.dso.query("c1:ofst?")
        self.tdiv = self.dso.query("tdiv?")
        self.dso.timeout = 1000 #default value is 2000(2s)
    
    def getWaveForm(self, waveformLength = 2048):
        self.vdiv = self.dso.query("c1:vdiv?")
        self.ofst = self.dso.query("c1:ofst?")
        self.tdiv = self.dso.query("tdiv?")
        self.dso.write("c1:wf? dat1")
        recv = list(self.dso.read_raw())[16:]
        volt_value = np.array(recv, dtype=np.int8)
        volt_value = volt_value/25*float(self.vdiv)-float(self.ofst)
        volt_value_reshape = volt_value[:waveformLength*int(len(volt_value)/waveformLength)].reshape((waveformLength, -1))
        volt_value_comp = np.average(volt_value_reshape, axis=1)
        #round to muV
        volt_value_comp = np.around(volt_value_comp*10**6).astype(int)
        return volt_value_comp
    
    def __del__(self): 
        self.dso.close()
    
if __name__=='__main__':
    scope = LeCroy()
    pl.plot(scope.getWaveForm())
    pl.show()