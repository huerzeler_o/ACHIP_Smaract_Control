# -*- coding: utf-8 -*-

from scope import LeCroy
from epics import caget, caput
import numpy as np
from time import sleep

scope = LeCroy()

if __name__ == '__main__': #create PVs based on prefix and pvdb definition
    while True:
        #read scope and stage        
        data = scope.getWaveForm()#*1e6
        dataSum = np.sum(data)        
        pos = caget('SATSY03-DLAC080-DHXP:posLINTHZ')
        
        #forward data to epics
        caput('SATSY03-DLAC080-DOSC:WAVEFORM', data)
        caput('SATSY03-DLAC080-DOSC:SUM', dataSum)
        caput('SATSY03-DLAC080-DOSC:POS', pos)
        
        #print(dataSum)
        sleep(0.1)